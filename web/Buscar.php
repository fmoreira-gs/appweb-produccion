<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');
	
	//SETEAR CARACTERES A UTF-8
	//ini_set('mssql.charset', 'UTF-8');
	
	//HISTORIAL
	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);

	//OBTENER PASO DE DATO (GET)
	$opcion = $_GET['opt'];
	$buscarPor = $_GET['buscarPor'];
	$criterio = $_GET['criterio'];
	
	$origen = $_GET['origen'];

	if(isset($opcion)){
		if(isset($buscarPor)){
			//INICIAR CONSULTA
		    if ($origen==1){
		        $clientes = $x->BuscarCliente($buscarPor,$criterio);
		    }else{
		        $clientes = $x->clienteSinCoordenada($buscarPor,$criterio);
		    }
			if(count($clientes)>0){
				switch($buscarPor){
					case 'nombre':
						echo('<center>');
						echo('Se muestran (' . count($clientes) . ') registros, para obtener resultados &oacute;ptimos especifique mejor el criterio.');
						echo('<br />');
						echo('<table class="VistaDatos" width="100%">');
						echo('<tr>');
						echo('<th class="LineaB" width="8%">Codigo</th>');
						echo('<th class="LineaB">Nombre</th>');
						//echo('<th class="LineaB">Historial</th>');
						echo('</tr>');
						$css = 0; $class = '';
						foreach($clientes as $resultados){
							if($css==1){
								$class = 'class="impar"';
								$css = 0;
							}else{
								$class = '';
								$css = 1;
							}
							echo('<tr ' . $class . '>');
							echo('<td class="LineaR">');
							if ($origen == 1){
							    echo('<a onClick="DatosClientes(\'' . $resultados[Codigo] . '\')">' . $resultados[Codigo] . '</a>');
							}else{
							    echo('<a onClick="w_posicion(\'' . $resultados[Codigo] . '\',0)">' . $resultados[Codigo] . '</a>');
							}
							echo('</td>');
							echo('<td>');
							echo('<div class="MasInfo" onClick="MasInfo(\'' . $resultados[Codigo] . '\')">');
							echo($resultados[Nombre]);
							echo('</div>');
							echo('<div onClick="MasInfo(\'' . $resultados[Codigo] . '\')" class="MasInfo_detalle" id="' . $resultados[Codigo] . '" style="display: none">');
							echo('<hr width="50%" />');
							echo('<b>Direcci&oacute;n: </b>' . $resultados[Direccion]);
							echo('<br />');
							echo('<b>Tel&eacute;fonos: </b>' . $resultados[Telefonos]);
							echo('<br />');
							echo('<b>Referencia: </b>' . $resultados[Referencia]);
							echo('</div>');
							echo('</td>');
							if ($origen == 1){
							    echo '<td class="noBorderRight"><a onClick="Historial(\'' . $resultados[Codigo] . '\')"><img src="images/historial.png" class="iconoCelda" alt="Historial" title="Historial"></a></td>';
							}
							echo('</tr>');
						}
						echo('</table>');
						echo('</center>');
						break;
					case 'codigo':
						echo('<center>');
						echo('Se muestran (' . count($clientes) . ') registros, para obtener resultados &oacute;ptimos especifique mejor el criterio.');
						echo('<br />');
						echo('<table class="VistaDatos" width="100%">');
						echo('<tr>');
						echo('<th class="LineaB" width="8%">Codigo</th>');
						echo('<th class="LineaB">Nombre</th>');
						echo('</tr>');
						$css = 0; $class = '';
						foreach($clientes as $resultados){
							if($css==1){
								$class = 'class="impar"';
								$css = 0;
							}else{
								$class = '';
								$css = 1;
							}
							echo('<tr ' . $class . '>');
							echo('<td class="LineaR">');
							if ($origen == 1){
							    echo('<a onClick="DatosClientes(\'' . $resultados[Codigo] . '\')">' . $resultados[Codigo] . '</a>');
							}else{
							    echo('<a onClick="w_posicion(\'' . $resultados[Codigo] . '\',0)">' . $resultados[Codigo] . '</a>');
							}
							echo('</td>');
							echo('<td>');
							echo('<div class="MasInfo" onClick="MasInfo(\'' . $resultados[Codigo] . '\')">');
							echo($resultados[Nombre]);
							echo('</div>');
							echo('<div onClick="MasInfo(\'' . $resultados[Codigo] . '\')" class="MasInfo_detalle" id="' . $resultados[Codigo] . '" style="display: none">');
							echo('<hr width="50%" />');
							echo('<b>Direcci&oacute;n: </b>' . $resultados[Direccion]);
							echo('<br />');
							echo('<b>Tel&eacute;fonos: </b>' . $resultados[Telefonos]);
							echo('<br />');
							echo('<b>Referencia: </b>' . $resultados[Referencia]);
							echo('</div>');
							echo('</td>');
							if ($origen == 1){
							    echo '<td class="noBorderRight"><a onClick="Historial(\'' . $resultados[Codigo] . '\')"><img src="images/historial.png" class="iconoCelda" alt="Historial" title="Historial"></a></td>';
							}
							echo('</tr>');
						}
						echo('</table>');
						echo('</center>');
						break;
					case 'RUC':
							echo('<center>');
							echo('Se muestran (' . count($clientes) . ') registros, para obtener resultados &oacute;ptimos especifique mejor el criterio.');
							echo('<br />');
							echo('<table class="VistaDatos" width="100%">');
							echo('<tr>');
							echo('<th class="LineaB" width="8%">Codigo</th>');
							echo('<th class="LineaB">Nombre</th>');
							echo('</tr>');
							$css = 0; $class = '';
							foreach($clientes as $resultados){
								if($css==1){
									$class = 'class="impar"';
									$css = 0;
								}else{
									$class = '';
									$css = 1;
								}
								echo('<tr ' . $class . '>');
								echo('<td class="LineaR">');
								if ($origen == 1){
								    echo('<a onClick="DatosClientes(\'' . $resultados[Codigo] . '\')">' . $resultados[Codigo] . '</a>');
								}else{
								    echo('<a onClick="w_posicion(\'' . $resultados[Codigo] . '\',0)">' . $resultados[Codigo] . '</a>');
								}
								echo('</td>');
								echo('<td>');
								echo('<div class="MasInfo" onClick="MasInfo(\'' . $resultados[Codigo] . '\')">');
								echo($resultados[Nombre]);
								echo('</div>');
								echo('<div onClick="MasInfo(\'' . $resultados[Codigo] . '\')" class="MasInfo_detalle" id="' . $resultados[Codigo] . '" style="display: none">');
								echo('<hr width="50%" />');
								echo('<b>Direcci&oacute;n: </b>' . $resultados[Direccion]);
								echo('<br />');
								echo('<b>Tel&eacute;fonos: </b>' . $resultados[Telefonos]);
								echo('<br />');
								echo('<b>Referencia: </b>' . $resultados[Referencia]);
								echo('</div>');
								echo('</td>');
								if ($origen == 1){
								    echo '<td class="noBorderRight"><a onClick="Historial(\'' . $resultados[Codigo] . '\')"><img src="images/historial.png" class="iconoCelda" alt="Historial" title="Historial"></a></td>';
								}
								echo('</tr>');
							}
							echo('</table>');
							echo('</center>');
							break;
				}
			}else{
				echo('<center><br/>No se encontraron resultados para ' . strtoupper($criterio) . '</center>');
			}
		}
	}	
?>