<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//CARGA DE SESIONES
	session_start();
	
	//CARGAR TECNICOS
	$sqltecnicos = $x->Tecnicos();
	
	//OBTENER PASO DE DATO (GET)
	$TecnicoR = $_GET['tecnico'];
	if(!isset($TecnicoR)&&$_SESSION['log_IDTECNICO']!='-1'){
		$TecnicoR = $_SESSION['log_IDTECNICO'];
	}
?>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title><?php echo(Config::$mvc_titulo); ?></title>
        <!--  ESTILOS  -->
        <link rel="stylesheet" type="text/css" href="css/estilo.css" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    </head>
    <body>
        <?php	
        	echo('<div id="Titulo">ASISTENCIAS TECNICAS</div>');
        	echo('<br/><br />');
        	echo('<center>');
        	echo('T&eacute;nico: ');
        	echo('<select name="tecnico" id="tecnico" class="Combo Extendido" onchange="Asistencias(0)">');
        	echo('<option>');
        	echo('----');
        	echo('</option>');
        	foreach($sqltecnicos as $tecnico){
        		echo('<option value="'. $tecnico[ID_TECNICO] . '"');
        		if(isset($TecnicoR) && ($TecnicoR==$tecnico[ID_TECNICO]) ){
        			echo ('selected');
        		}
        		echo '>';
        		echo($tecnico[NOMBRE]);
        		echo('</option>');
        	}
        	echo('</select>');
        	echo('</center>');
        	
        	//CONTENIDO - ASISTENCIAS TECNICAS
        	//$datos = $x->Asistencias();
        	$datos = $x->ObtenerDatos('ORDENESTRABAJOASIGNADAS','*','WHERE IDENCARGADO = \'' . $TecnicoR . '\'','');
        	echo(count($datos));
        ?>
    </body>
</html>