<html>
<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title><?php //echo(Config::$mvc_titulo); ?></title>
		<!--  ESTILOS  -->
		<link rel="stylesheet" type="text/css" href="css/estilos.css" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
</head>
<body><div id="facturapendiente">
<?php
		
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	$local = $_SESSION['log_LOCAL'];
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->VerificarSCAIIN('index.php', $local);
	
	ini_set('mssql.charset', 'UTF-8');
	
	$numfactura = urldecode($_POST['numfactura']);
	$numguia = urldecode($_POST['guia']);
	$tipo = urldecode($_POST['tipo']);
	
	if($numfactura == null){ echo '<br><br>No se ha encontrado el dato'; }else{			
		$numclientes= $cone->CuentaClientes($numfactura,$local);
		}
		foreach ($numclientes as $value) {
			$codcli = $value[Cliente]; 
			$caja = $value[IDcaja];
			
			if ($value[Caja]==0){
				$var = 'SaldoFactura';
			}else{
				$var = 'saldovalor';
			}
			
			$facturas = $cone->FacturasPendientes($numfactura, $codcli, $caja, $var, $local);
			echo 'Nombre del Cliente:<font color="Blue"> ' . $facturas[0][NombreCliente] . '</font><br>';
			echo 'Total Factura:<font color="Blue"> $' . $facturas[0][TotalFactura] . '</font> - ';
			echo 'Saldo:<font color="Blue"> $' . $facturas[0][SaldoFactura] . '</font>';
			
			echo '<br><table rules="all" border=1><tr>';
			echo '<td width="10%"><b>Ct</td>';
			echo '<td width="20%"><b>Fact.</td>';
			echo '<td width="20%"><b>Saldo</td>';
			echo '<td width="20%"><b>Fecha</td>';
			echo '<td width="30%"><b>Caja</td>';
			echo '</tr></table>';
			echo '<table rules="all" border=1>';
			foreach ($facturas as $factura){
				if($factura[Cuota] == 0){
					$validar = $cone->ValidaFactura($factura[Cuota],$factura[Numero],$numguia,$factura[SaldoFactura], $facturas[0][IDcaja],$local);
				}else{
				    $validar = $cone->ValidaFactura($factura[Cuota],$factura[Numero],$numguia,$factura[saldovalor], $facturas[0][IDcaja],$local);
				}
				
				if($factura[Cuota]==0){
					echo '<tr><td width="10%"><input type="text" class="cajatext" value="Cont."</td>';
					if($factura[SaldoFactura] > 0 && $validar == 0 && $validar != 'Mayor'){
						echo '<td width="20%"><a href="#" onclick=InsertarFactura(' . $factura[Numero] . ',' . $factura[SaldoFactura] . ',' . $numguia . ',"' . $tipo . '",' . $factura[Cuota] . ',' . $factura[IDcaja] . ')>' . $factura[Numero] . '</a></td>';
						echo '<td width="20%">' . $factura[SaldoFactura] . '</td>';
					}elseif($validar < $factura[SaldoFactura]){
						$saldo = $factura[SaldoFactura] - $validar;
						echo '<td width="20%"><a href="#" onclick=InsertarFactura(' . $factura[Numero] . ',' . $saldo . ',' . $numguia . ',"' . $tipo . '",' . $factura[Cuota] . ',' . $factura[IDcaja] . ')>' . $factura[Numero] . '</a></td>';
						echo '<td width="20%">' . $saldo . '</td>';
					}else{
						echo '<td width="20%">' . $factura[Numero] . '</td>';
						echo '<td width="20%">' . $factura[SaldoFactura] . '</td>';
					}
					
				}else{
					echo '<tr><td width="10%">' . $factura[Cuota] . '</td>';
					if($factura[saldovalor] > 0 && $validar == 0 ){
						echo '<td width="20%"><a href="#" onclick=InsertarFactura(' . $factura[Numero] . ',' . $factura[saldovalor] . ',' . $numguia . ',"' . $tipo . '",' . $factura[Cuota] . ',' . $factura[IDcaja] . ') >' . $factura[Numero] . '</a></td>';
						echo '<td width="20%">' . $factura[saldovalor] . '</td>';
					}elseif($validar < $factura[saldovalor]){
							$saldo = $factura[saldovalor] - $validar;
							echo '<td width="20%"><a href="#" onclick=InsertarFactura(' . $factura[Numero] . ',' . $saldo . ',' . $numguia . ',"' . $tipo . '",' . $factura[Cuota] . ',' . $factura[IDcaja] . ') >' . $factura[Numero] . '</a></td>';
							echo '<td width="20%">' . $saldo . '</td>';
						}else{	
							$saldo = $saldo = $factura[saldovalor] - $validar;
							echo '<td width="20%">' . $factura[Numero] . '</td>';
							echo '<td width="20%">' . $saldo . '</td>';
					}
					
				}
				echo '<td width="20%">' . date("d/m/Y",strtotime($factura[Fecha])) . '</td>';
				echo '<td width="30%">' . $factura[Caja] . '</td>';
				echo '</tr>';
			}
			echo '</table><br>';
		}
		//}else{echo 'No se ha encontrado el dato';
	?>
<center><br><a  href="#" class="vinculocancelar icon_cancelar" onclick="GuiaCobro(<?php echo $numguia; ?>)" style="font-family: italic;" > Cancelar</a></center>
</div></body>
</html>