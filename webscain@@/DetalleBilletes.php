<?php 
		
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	$local = $_SESSION['log_LOCAL'];

	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->VerificarSCAIIN('index.php',$local);

	$empresa = Config::$Empresa;
	$caja = Config::$Caja;
    $guia = $_GET['guia'];
    $tipo= $_GET['tipo'];
    
	$billetes = $cone->billetes($guia,$local);
	$totaldetalle = $cone->totaldetalle($guia,$tipo,$local);
	$totaldetalle = number_format($totaldetalle[0][total], 2, '.', '');
	$cheque = $cone->cheques($guia,$local);
?>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
		<!--  ESTILOS  -->
		<link rel="stylesheet" type="text/css" href="css/estilos.css" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	
	<body><a class='titulo_m'><div class='menu_acordeon'>
		<div id="detallebilletes">
			<?php 
			 $guias= $cone->GuiasCobro($guia,$local);
				echo "<div class='titulo'>";
					echo "<p class='tutulo'>Detalle Billetes	-	<label class='totalmenu'>Suman: $ $totaldetalle</label> </p>";
				echo "</div>";
				echo "<div class='info' id='info_bille'>";
			?>
			<table rules="all" border=1>
				<tr style="background-color:#00a0d0;">
					<td>Den. Billetes</td>
					<td>Cantidad</td>
					<td>Total</td>
				</tr>
				<tr>
					<td><input type="text" class="textefectivo" id="uno" value=1 readonly></td>
					<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==1){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="unoval" onchange=calcular(this,'<?php echo($guia); ?>',"billetes"); <?php if($val==1){echo 'readonly';} ?>></td>
					<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totaluno" readonly></td>
					<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'billetes','','',1)></a></td></tr>";	}}?>
				</tr>
				<tr>
					<td><input type="text" class="textefectivo" id="cinco" value=5 readonly></td>
					<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==5){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1; }} ?>" id="cincoval" onchange=calcular(this,'<?php echo($guia); ?>',"billetes"); <?php if($val==1){echo 'readonly';} ?>></td>
					<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcinco" readonly></td>
					<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'billetes','','',5)></a></td></tr>";	}}?>
				</tr>
				<tr>
					<td><input type="text" class="textefectivo" id="diez" value=10 readonly></td>
					<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==10){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1; }} ?>" id="diezval" onchange=calcular(this,'<?php echo($guia); ?>',"billetes"); <?php if($val==1){echo 'readonly';} ?>></td>
					<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totaldiez" readonly></td>
					<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'billetes','','',10)></a></td></tr>";	}}?>
				</tr>
				<tr>
					<td><input type="text" class="textefectivo" id="veinte" value=20 readonly></td>
					<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==20){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="veinteval" onchange=calcular(this,'<?php echo($guia); ?>',"billetes"); <?php if($val==1){echo 'readonly';} ?>></td>
					<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalveinte" readonly></td>
					<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){  if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'billetes','','',20)></a></td></tr>";	}}?>
				</tr>
				<tr>
					<td><input type="text" class="textefectivo" id="cincuenta" value=50 readonly></td>
					<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==50){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="cincuentaval" onchange=calcular(this,'<?php echo($guia); ?>',"billetes"); <?php if($val==1){echo 'readonly';} ?>></td>
					<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcincuenta" readonly></td>
					<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'billetes','','',50)></a></td></tr>";	}}?>
				</tr>
				<tr>
					<td><input type="text" class="textefectivo" id="cien" value=100 readonly></td>
					<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==100){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="cienval" onchange=calcular(this,'<?php echo($guia); ?>',"billetes"); <?php if($val==1){echo 'readonly';} ?>></td>
					<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcien" readonly></td>
					<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){  if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'billetes','','',100)></a></td></tr>";	}}?>
				</tr>
			</table>
		<?php echo '</div></div></a>'; ?>
		<!-- <center><br><a href="#" onclick="GuiaCobro()" class="vinculo icon_cancelar" style="font-family: italic;" > Atras</a></center>  -->
	</div></body>
</html>
