<?php
	//Archivos
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
	session_start();
	$usuario=$_SESSION['log_USUARIO'];
	
	$codigocli = $_GET['codigo'];
	
	$sqlHorarios = $x->Horarios();
	$datohorario = $x->HorariosXcliente($codigocli);
	
	$diaSemana = Array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
	
	//echo '<form method=\'POST\'name=\'form\' action=\'GrabarHorariosClientes.php\' id="frmHorarios">';
	echo '<table class="VistaDatos" style="width:100%">';
	$cont=1;
	foreach ($diaSemana as $dia){
			echo '<tr>';
			$ingreso='Ingreso';
			$salida='Salida';
			echo "<td width='20%'>$dia</td><td width='70%'>";
			$ap1 = "$dia$ingreso";
			$ap = date('H:i:s', strtotime($datohorario[0][$ap1]));
			$cl1 = "$dia$salida";
			$cl = date('H:i:s', strtotime($datohorario[0][$cl1]));

			$horariocli= "$ap - $cl";
			echo '<select name="horario'. $dia . '" id="horario'. $dia . '" class="Combo Extendido" style="width:100%; height:80%">';
			   	foreach($sqlHorarios as $horario){
			   		$apertura= date('H:i:s', strtotime($horario[HorarioInicio]));
			   		$cierre = date('H:i:s', strtotime($horario[HorarioFin]));
			   		$horariodia = "$apertura - $cierre";
				   	echo '<option value="'. $horariodia .'"';
				   	if(isset($horariodia) && ($horariodia==$horariocli) ){
				   		echo (' selected');
				   	}
				   	echo '>';
				   		echo $horariodia;
				   	echo '</option>';
			   	}
		   	echo '</select>';
		   	echo '</td>';
		   	if($datohorario[0][$dia]== 1){
		   		echo('<td width=\'10%\'>');
		   		echo('<a class="Radio Radio_ON" onClick="CheckHorario(\'check' . $dia . '\',\'H' . $dia . '\')" id="H' . $dia . '"></a>');
		   		echo('<input type="checkbox" style="display: none" checked="checked" name="check'. $dia . '[]" id="check'. $dia . '" value=' . $cont . '/>');
		   		echo('</td>');
		   		echo('</tr>');
		   	}else{
		   		echo('<td width=\'10%\'>');
		   		echo('<a class="Radio Radio_OFF" onClick="CheckHorario(\'check' . $dia . '\',\'H' . $dia . '\')" id="H' . $dia . '"></a>');
		   		echo('<input type="checkbox" style="display: none" name="check'. $dia . '[]" id="check'. $dia . '" value=' . $cont . '/>');
		   		echo('</td>');
		   		echo('</tr>');
		   	}
		}
		echo '<tr><td>Nota:</td><td colspan="2"><input type=\'textarea\' name=\'notahorario\' id=\'notahorario\' class="Edicion" value=\'' . $datohorario[0][Nota] . '\' /> </td></tr>';
		echo '<tr><td>Test P.:</td><td colspan="2"><input type=\'time\' min="00:00:00" max="05:00:00" name=\'test\' id=\'test\' class="Edicion" value=\'' . date('H:i:s',strtotime($datohorario[0][testPeriodico])) . '\' /> </td></tr>';
		echo '<tr><td>Autoarmado:</td><td colspan="2"><input type=\'time\' name=\'armado\' id=\'armado\' class="Edicion" value=\'' . date('H:i:s',strtotime($datohorario[0][autoArmado])) . '\' /> </td></tr>';
		if($datohorario[0][Activo]==1){
			echo('<tr>');
			echo('<td>Control Horario:</td>');
			echo('<td colspan="2">');
			echo('<a class="Radio Radio_ON" onClick="CheckHorario(\'control\',\'HC\')" id="HC"></a>');
			echo('<input type="checkbox" style="display: none" checked="checked" name="control[]" id="control" value=\'' . $datohorario[0][Activo] . '\' />');
			echo('</td>');
		}else{
			echo('<tr>');
			echo('<td>Control Horario:</td>');
			echo('<td colspan="2">');
			echo('<a class="Radio Radio_OFF" onClick="CheckHorario(\'control\',\'HC\')" id="HC"></a>');
			echo('<input type="checkbox" style="display: none" name="control[]" id="control" value=\'' . $datohorario[0][Activo] . '\' />');			
			echo('</td>');
		}		
		echo '</table>';
		echo('<br/><center>');
		//echo '<input class="Boton_Guardar icon_guardar" type="submit" value="Ahora" />';
		//echo '</form>';
		//echo('<input type="text" id="txtHCodigo" name="txtHCodigo" value="' . $codigocli . '"/>');
		echo('<input type="hidden" class="Edicion" id="txtcodigo" value="' . $codigocli . '">');
		echo('<br/>');
		if($_SESSION['log_rolUsr'] != 0){
			echo('<a class="Boton_Guardar icon_guardar" onClick="Horarios_GuardarDatos()">&nbsp;Guardar cambios..</a>');
		}
		echo('</center>');
		echo('<br />');
?>