<?php
//Archivos
require_once __DIR__ . '/../app/Config.php';
require_once __DIR__ . '/../app/Model.php';

//VERIFICAR SI ESTA AUTENTICADO
$ver = new Login();
$ver->Verificar('index.php');

$codigo = $_GET['codigo'];

$lat = $_GET['latitud'];
$lon = $_GET['longitud'];

?>

<html>
    <head>
   		<title>Posicionamiento</title>
   		<link rel="stylesheet" type="text/css" href="css/estilo.css" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjZhArEJGrMLLfujWfelLUPyBnkvS5gQw"></script>
		<script type="text/javascript" src="js/scripts.js"></script> 
    </head>
    <body ><div id="G_Contenedor">
        <input type="button" class="Boton_Coordenadas" value="Buscar Coordenada Actual" onclick="obtenerPosicion()"><br />
        <form>
            <table>
                <tr>
                    <td>
                   		Latitud:&nbsp;
                    </td>
                    <td>
                    <?php 
                        echo '<br><input type="text" name="latitud" id="latitud" value=\''. $lat .'\' readonly></input>';
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>
    	                Longitud:&nbsp;
                    </td>
                    <td>
                     <?php 
                        echo '<input type="text" name="longitud" id="longitud" value=\''. $lon .'\' readonly></input>';
        	         ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
               		    <div id="mapholder"></div>
                    </td>
                </tr>
            </table>
        </form>
        <?php 
            if($_SESSION['log_rolUsr'] != 0){
                echo '<input type="button"  class="Boton_Coordenadas" value="Actualizar posicion en el Cliente" onclick="posicionamiento(\''. $codigo .'\',\'' . $origen . '\')">';
            }
            if($origen != 1){
                echo '<center><br><input type="button" id="btnCancelar" class="Boton_Coordenadas" value="CANCELAR" onclick="CerrarGestiones()"></center>';
            }
        ?>
        <font color="red">
	        <span id="aviso"></span>
        </font>

    </div></body>
</html>