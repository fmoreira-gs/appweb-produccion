<?php
	session_start();
	$usuario=$_SESSION['log_USUARIO'];
	
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	//ini_set('mssql.charset', 'UTF-8');
	
	$idCita= $_GET['idCita'];
	$idUsuario= $_GET['idUsr'];
	$detalleCita = $art -> citasdetalle($idCita);
	
?>
<html>
	<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title><?php echo(Config::$mvc_titulo); ?></title>
	<!--  ESTILOS  -->
	<link rel="stylesheet" type="text/css" href="css/estilos.css" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	</head>
	<body>
        <?php
            $citas = $art -> datosCita($idCita);
        	echo '<div class="ventana_titulo">GESTIONES';
        	echo '<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones(\'' . $idUsuario. '\')"><img src="images/cerrar_ventana.png" style="width:100%"/></a></div>';
        	echo '</div>';

        	echo '<div class="info">&nbsp;<b>CLIENTE: </b>' . $citas[0][MDECLI_IDCLIE] . ' - ' . substr($citas[0][CLIENTE],0,35) . ' <b>OT: ' . $citas[0][MDECIT_IDCITA] . '</div><hr width="25%">';
        
        	if (count($detalleCita)>0){
        		echo('<div style="overflow:scroll;height:100%;width:100%">');
        		echo('<table class="Gestiones">');
        			echo('<tr>');
        			echo('<th>FECHA</th>');
        			echo('<th>NOTA</th>');
        			echo('<th>USUARIO</th>');
        			echo('</tr>');
        			$css = 0; $class = '';
        			foreach ($detalleCita as $registro){
        				if($css==1){
        					$class = 'class="impar"';
        					$css = 0;
        				}else{
        					$class = '';
        					$css = 1;
        				}
        				echo '<tr ' . $class . '>';
        				echo '<td id="GestionFecha">' . date('d-m-Y',strtotime($registro[MDEGDC_FECHAG])) . '<br>' . date('H:i:s',strtotime($registro[MDEGDC_FECHAG])) . '</td>';
        				echo '<td class="GestionesBordesLaterales">' . $registro[MDEGDC_GESTIO] . '</td>';
        				echo '<td>' . $registro[MDEGDC_GENPOR] . '</td>';
        				echo '</tr>';
        			}
        		echo('</table>');
        	}else{
        		echo('<div style="overflow:scroll;height:100%em;width:100%">');
        		echo('<center>No existen registros.</center>');
        		echo('</div>');
        	}
        	echo('<div id="AgregarGestion">');
        	echo '<input type="text" class="NuevaGestion" name="txtGestion" id="txtGestion">';
        	echo '<input type="button" class="BotonEnviarGestion" onClick="RegistrarGestion(\'' . $idCita . '\',\'C\',\'' . $idUsuario .'\')" value="Enviar Gestion" />';
        	echo('</div>');
        	echo '<a onClick="MarcarEjecutado()" style="text-decoration: none"><img id="Marca" src="images/ejecutado_0.png" style="height:8%em;width:8%"><span id="MarcaTexto">MARCAR COMO EJECUTADO</span></a><hr width="50%">';
        	echo('<div id="PosponerGestion">');
        	$hoy = date('d')+1 . '-' . date('m'). '-' . date('Y');
        	$hoy = date('Y-m-d',strtotime($hoy));
        	echo 'Fecha:<input type="date" id="fechaPosponer" class="txtPosponer" min="' . $hoy . '" value="' . date('Y-m-d', strtotime($hoy)) . '" style="text-decoration: none">';
        	echo 'H.Ini:<input type="time" id="horaIni" class="txtPosponer" min="08:00" max="18:00" value="08:00" style="text-decoration: none" required="required"> ';
        	echo ' H.Fin:<input type="time" id="horaFin" class="txtPosponer" min="08:00" max="18:00" value="08:30" style="text-decoration: none" required="required">';
        	echo '<hr width="50%"><center><input type="button" class="BotonPosponerGestion" onClick="posponerCita(\''. $idCita .'\',\''. $idUsuario .'\',\'P\')" value="Posponer" /></center>';
        	echo('</div>');
        ?> 	
	</body>
</html>