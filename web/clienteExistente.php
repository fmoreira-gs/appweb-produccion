<?php 
    session_start();
    $usuario=$_SESSION['log_USUARIO'];	

	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
    $json =  json_decode($_POST['json']);
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title><?php echo(Config::$mvc_titulo); ?></title>
    <!--  ESTILOS  -->
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="stylesheet" type="text/css" href="css/estilo.css" />
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script type="text/javascript" src="js/scripts.php"></script>
</head>
<body>
<?php
    echo '<div class="ventana_titulo">COINCIDENCIAS ENCONTRADAS';
    echo '<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones()"><img src="images/cerrar_ventana.png" style="width:100%" /></a></div>';
    echo '</div>';
    $existentes = $art->clientesArtemisa($json->nombre,$json->apellido,$json->ruc);
    if(count($existentes)>0){
        echo '<div class="H_ContenedorPlus">
        <table class="table table-sm table-striped border=1">
        <thead class="thead-light">
            <tr>
                <th width="5%"></th>
                <th width="35%">NOM. COMERCIAL</th>
                <th width="20%">NOMBRE</th>
                <th width="20%">APELLIDO</th>
                <th width="20%">CED/RUC</th>
            </tr>
        </thead>
        <tbody>';
        $cont = 1;
        foreach ($existentes as $existe) {
            echo '
                <input type="hidden" id="txtIdCliente' . $cont . '" value="' . $existe['MDECLI_IDCLIE'] . '">
                <tr>
                <td width="5%"><button type="button" class="btn btn-sm btn-success" id="btnNuevo' . $cont . '" onclick=grabarCitaArtemisa(0,' . $cont . ')><i class="fa fa-plus"></i></button></td>
                <td width="35%"><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-0" type="text" value="' . $existe['MDECLI_NOMAMO'] . '" id="txtContacto' . $cont . '" readonly></td>
                <td width="20%"><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-0" type="text" value="' . $existe['MDECLI_NOMBRE'] . '" id="txtNombre' . $cont . '" readonly></td>
                <td width="20%"><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-0" type="text" value="' . $existe['MDECLI_APELLI'] . '" id="txtApellido' . $cont . '" readonly></td>
                <td width="20%"><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-0" type="text" value="' . $existe['MDECLI_CEDRUC'] . '" id="txtRuc' . $cont . '" readonly></td>
                </tr>
            ';
            $cont++;
        }
    }else{
        echo '<h3>NO Existen Coincidencias..!!<h3>';
    }
    echo '
    </tbody>
    </table></div>
    <center><input type="button" class="btn btn-primary btn-sm" value="Crear Cliente Nuevo" id="btnNuevo" onClick=grabarCitaArtemisa(1,' . $cont . ') /></center>
    <input type="hidden" id="txtFechCita" value="' . $json->fechaCita . '">
    <input type="hidden" id="txtRequerimiento" value="' . $json->requerimiento . '">
    <input type="hidden" id="txtDireccion" value="' . $json->direccion . '">
    <input type="hidden" id="txtTelefono" value="' . $json->telefono . '">
    <input type="hidden" id="txtAgencia" value="' . $json->agencia . '">
    <input type="hidden" id="txtNomCliente" value="' . $json->nombre . '">
    <input type="hidden" id="txtApeCliente" value="' . $json->apellido . '">
    <input type="hidden" id="txtUsuario" value="' . $usuario . '">
  
    ';
    
?>
</body>
</html>