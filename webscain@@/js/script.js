//src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js;
src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js";
src="../js/jquery-3.4.1.min.js";
src="../js/select2.js";

var destinoAJAX;

function loadXMLDoc() {	  
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
	xmlhttp.onreadystatechange=function()
    {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById(destinoAJAX).innerHTML=xmlhttp.responseText;
		}
	}
}

//FUNCION PARA RESTRINGIR EL INGRESO A UN CAMPO DE SOLO NUMEROS
function solonumeros(input){
	var valor = input.value.replace(/\./g,'');
	if(isNaN(valor)){
	  	alert('Solo se permiten numeros  en \xe9ste Campo!!');
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

//FUNCION QUE HACE QUE EL NUMERO INGRESADO SE FORMATEE CON 2 DECIMALES Y SOLO PERMITE NUMEROS
//SE LA DESHABILITA EN LA GUIA DE COBRO PORQUE NO PERMITE INGREAR CORRECTAMENTE LAS CANTIDADES POR EJEM. 100 � 100.00

function format(input){
  	var num = input.value.replace(/\./g,'');
  	if(!isNaN(num)){
  		if(num.length>2)
  		{
  			num=num.substring(0,num.length-2)+'.'+num.substring(num.length-2);
  		}
  			input.value = num;
  	}
  	else{ alert('Solo se permiten numeros  en \xe9ste Campo!!');
  		input.value = input.value.replace(/[^\d\.]*/g,'');
  	}
}

//FUNCION PARA REALIZAR TODAS LAS GESTIONES CON LOS CHEQUES
function cheques(input, saldofactura,contador,numero_orden){
	Nombre = 'valorcheque'+ contador + '';
	banco = 'bancocheque' + contador + '';
	cheque = 'numcheque' + contador + '';
	cuenta = 'cuentacheque' + contador + '';
	postfechado = 'chequepos' + contador + '';
	caja = 'cajacheque' + contador + '';
	cuota = 'cuotacheque' + contador + '';
	fechapos = 'fechapos' + contador + '';
	factura = 'facturacheque' + contador + '';
	
	var Banco = document.getElementById(banco).options.selectedIndex;
	var txtBanco= document.getElementById(banco).options[Banco].text;
	var txtCheque = document.getElementById(cheque).value;
	var txtCuenta = document.getElementById(cuenta).value;
	var chkpost = document.getElementById(postfechado).checked;
	var saldoch = document.getElementById(Nombre).value;
	var numeroguia = document.getElementById("numguia").value;
	var txtCaja = document.getElementById(caja).value;
	var txtCuota = document.getElementById(cuota).value;
	var txtFechapos = document.getElementById(fechapos).value;
	var txtFactura = document.getElementById(factura).value;
	var tipo='cheque';
	
	if(chkpost==false){
		chkpost='0';
		txtFechapos='';
	}
	if(chkpost==true){
		chkpost='1';
	}
	if(chkpost=='1' && txtFechapos==''){
		alert('Debe ingresar una fecha para el cheque POSFECHADO');
	}
	else{
		if(txtBanco!='---' && txtCheque!='' && txtCuenta!=''){
			if(saldoch > saldofactura){
				alert('El valor ingresado $' + saldoch + ' es mayor al saldo y/o cuota de la factura \n\r El valor pendiente solo es: $' + saldofactura + '');
				var numero = document.getElementById("numguia").value;
				destinoAJAX = "contenido";	
				loadXMLDoc();
				xmlhttp.open("POST","GuiaCobro.php", true);
				xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
				xmlhttp.send("numguia=" + encodeURIComponent(numero));
				xmlhttp.send();
				GuiaCobro(numeroguia)
			}else{
				//alert('caja: ' + txtCaja + '\n guia: ' + numeroguia + '\n cheque: ' + txtCheque + '\n valor: ' + saldoch + '\n postfechado: ' + chkpost + '\n cuota: ' + txtCuota + '\n banco: ' + txtBanco + '\n cuenta: ' + txtCuenta + '\n fechapos: ' + txtFechapos + '\n factura: ' + txtFactura + '\n tipo: ' + tipo + '\n numero: ' + numero_orden);
				destinoAJAX = "contenido";	
				$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
				loadXMLDoc();	
				xmlhttp.open("GET","EditarRegistro.php?caja=" + txtCaja + "&guia=" + numeroguia + "&cheque=" + txtCheque + "&valor="+ saldoch + "&postfechado=" + chkpost + "&cuota=" + txtCuota + "&banco=" + txtBanco + "&cuenta=" + txtCuenta + "&fechapos=" + txtFechapos + "&factura=" + txtFactura + "&tipo=" + tipo + "&numero=" + numero_orden, true);
				xmlhttp.send();
				GuiaCobro(numeroguia)
			}
		}else{
			if(txtBanco=='---'){
				alert('Debe Seleccionar el Banco!!');
			}
			if(txtCheque=='' && txtBanco!='---'){
				alert('No ha Ingresado N\xfamero de Cheque !!');
			}
			if(txtCuenta=='' && txtBanco!='---' && txtCheque!=''){
				alert('Debe ingresar el N\xfamero de cuenta!!');
			}
		}
	}
}

//FUNCION PARA REALIZAR TODAS LAS GESTIONES CON EL EFECTIVO
function efectivo(input, saldofactura, contador, numero_orden){
	saldo= 'valorefectivo' + contador +'';
	caja = 'cajaefectivo' + contador + '';
	cuota = 'cuotaefectivo' + contador + '';
	factura = 'facturaefectivo' + contador + '';

	var txtCaja = document.getElementById(caja).value;
	var txtCuota = document.getElementById(cuota).value;
	var txtFactura = document.getElementById(factura).value;
	var saldoef = document.getElementById(saldo).value;
	var numeroguia = document.getElementById("numguia").value;
	var tipo='efectivo';
	
	if(saldoef > saldofactura){
		alert('El valor ingresado $' + saldoef + ' es mayor al saldo de la factura \n\r El valor pendiente solo es: $' + saldofactura + '');
		var numero = document.getElementById("numguia").value;
		destinoAJAX = "contenido";	
		loadXMLDoc();
		xmlhttp.open("POST","GuiaCobro.php", true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("numguia=" + encodeURIComponent(numero));
		xmlhttp.send();
		GuiaCobro(numeroguia)

	}else{
		destinoAJAX = "contenido";	
		$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
		loadXMLDoc();	
		xmlhttp.open("GET","EditarRegistro.php?caja=" + txtCaja + "&guia=" + numeroguia + "&factura=" + txtFactura + "&tipo=" + tipo + "&cuota=" + txtCuota + "&valor=" + saldoef + "&numero=" + numero_orden, true);
		xmlhttp.send();
		GuiaCobro(numeroguia)
	}
}

//FUNCION PARA REALIZAR TODAS LAS GESTIONES CON LAS transferencias
function transferencias(input, saldofactura, contador, numero_orden){
	valor = 'valortr' + contador + '';
	banco = 'bancotr' + contador + '';
	transf = 'numtr' + contador + '';
	fecha = 'fechatr' + contador + '';
	caja = 'cajatr' + contador + '';
	cuota = 'cuotatr' + contador + '';
	factura = 'facturatr' + contador + '';

	var Banco = document.getElementById(banco).options.selectedIndex;
	var txtBanco= document.getElementById(banco).options[Banco].text;
	var txtTransf = document.getElementById(transf).value;
	var txtFecha = document.getElementById(fecha).value;
	var numeroGuia = document.getElementById("numguia").value;
	var txtCaja = document.getElementById(caja).value;
	var txtCuota = document.getElementById(cuota).value;
	var saldotr = document.getElementById(valor).value;
	var txtFactura = document.getElementById(factura).value;
	var tipo='transferencia';
	if((txtBanco!='---' || txtBanco==null ) && txtTransf!='' && txtFecha!=''){
		if(saldotr > saldofactura){
			alert('El valor ingresado $' + saldotr + ' es mayor al saldo y/o cuota de la factura \n\r El valor pendiente solo es: $' + saldofactura + '');
			var numero = document.getElementById("numguia").value;
			destinoAJAX = "contenido";	
			loadXMLDoc();
			xmlhttp.open("POST","GuiaCobro.php", true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("numguia=" + encodeURIComponent(numero));
			xmlhttp.send();
			GuiaCobro(numeroGuia)
		}else{
			destinoAJAX = "contenido";	
			$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');
			loadXMLDoc();
			xmlhttp.open("GET","EditarRegistro.php?caja=" + txtCaja + "&guia=" + numeroGuia + "&transf=" + txtTransf + "&valor="+ saldotr + "&cuota=" + txtCuota + "&banco=" + txtBanco + "&fechatr=" + txtFecha + "&factura=" + txtFactura + "&tipo=" + tipo + "&numero=" + numero_orden, true);
			xmlhttp.send();
			GuiaCobro(numeroGuia)
		}
	}else{
		if(txtBanco=='---' || txtBanco==null ){
			alert('Debe Seleccionar el Banco!!');
		}
		if(txtTransf=='' && (txtBanco!='---' || txtBanco==null)){
			alert('No ha Ingresado N\xfamero de Transferencia !!');
		}
		if(txtFecha=='' && (txtBanco!='---' || txtBanco==null ) && txtTransf!=''){
			alert('Debe escoger la FECHA de la Transferencia!!');
		}
	}
}

//FUNCION PARA REALIZAR TODAS LAS GESTIONES CON LAS  tarjetas de credito
function tarjetas(input, saldofactura,contador,numero_orden){
	valor = 'valortc' + contador + '';
	banco = 'bancotc' + contador + '';
	tarjeta = 'numtc' + contador + '';
	autorizacion = 'autorizaciontc' + contador + '';
	caja = 'cajatc' + contador + '';
	cuota = 'cuotatc' + contador + '';
	factura = 'facturatc' + contador + '';
	numtarjeta = 'numerotc' + contador + '';
	idtarjeta = 'idtc' + contador + '';

	var Banco = document.getElementById(banco).options.selectedIndex;
	var txtBanco= document.getElementById(banco).options[Banco].text;
	var txtValorItem = document.getElementById(banco).value;
	var txtTarjeta = document.getElementById(tarjeta).value;
	var txtAutoriza = document.getElementById(autorizacion).value;
	var numeroGuia = document.getElementById("numguia").value;
	var txtCaja = document.getElementById(caja).value;
	var txtCuota = document.getElementById(cuota).value;
	var saldotc = document.getElementById(valor).value;
	var txtFactura = document.getElementById(factura).value;
	var txtNumTarjeta = document.getElementById(numtarjeta).value;
	var tipo='tarjeta';

	if((txtBanco!='---' || txtBanco==null ) && txtTarjeta!='' && txtAutoriza!='' && txtNumTarjeta!=''){
		if(saldotc > saldofactura){
			alert('El valor ingresado $' + saldotc + ' es mayor al saldo y/o cuota de la factura \n\r El valor pendiente solo es: $' + saldofactura + '');
			var numero = document.getElementById("numguia").value;
			destinoAJAX = "contenido";	
			loadXMLDoc();
			xmlhttp.open("POST","GuiaCobro.php", true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("numguia=" + encodeURIComponent(numero));
			xmlhttp.send();
			GuiaCobro(numeroGuia)
		}else{
			//alert('saldo: ' + saldotc + '\n caja: ' + txtCaja + '\n guia: ' + numeroGuia + '\n tarjeta: ' + txtTarjeta + '\n cuota: ' + txtCuota + '\n retencion: ' + txtRetencion + '\n banco: ' + txtBanco + '\n autorizacion: ' + txtAutoriza + '\n factura: ' + txtFactura + '\n tipo: ' + tipo + '\n numero: ' + numero_orden + '\n numtarjeta: ' + txtNumTarjeta + '\n idtarjeta: ' + txtValorItem);
			destinoAJAX = "contenido";	
			$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
			loadXMLDoc();	
			xmlhttp.open("GET","EditarRegistro.php?caja=" + txtCaja + "&guia=" + numeroGuia + "&tarjeta=" + txtTarjeta + "&valor="+ saldotc + "&cuota=" + txtCuota + "&banco=" + txtBanco + "&autorizacion=" + txtAutoriza + "&factura=" + txtFactura + "&tipo=" + tipo + "&numero=" + numero_orden + "&numtarjeta=" + txtNumTarjeta + "&idtarjeta=" + txtValorItem, true);
			xmlhttp.send();
			GuiaCobro(numeroGuia)
		}
	}else{
		if(txtBanco=='---'){
			alert('Debe Seleccionar el Banco!!');
		}
		if(txtTarjeta=='' && txtBanco!='---'){
			alert('No ha Ingresado N\xfamero de Boucher !!');
		}
		if(txtAutoriza=='' && txtBanco!='---' && txtTarjeta!=''){
			alert('Debe ingresar el N\xfamero de Autorizacion del Boucher!!');
		}
		if(txtNumTarjeta=='' && txtBanco!='---' && txtTarjeta!='' && txtAutoriza!=''){
			alert('Debe ingresar el N\xfamero de Tarjeta!!');
		}
	}
}

//FUNCION PARA REALIZAR TODAS LAS GESTIONES CON LAS  tarjetas de credito
function retenciones(input, saldofactura,contador,numero_orden){

	valorRubro = 'valorrt' + contador + '';
	idRubro = 'rubrort' + contador + '';
	idImpuesto = 'imprt' + contador + '';
	detImpuesto = 'imprt' + contador + '';
	retencion = 'detallert' + contador + '';
	caja = 'cajart' + contador + '';
	cuota = 'cuotart' + contador + '';
	factura = 'facturart' + contador + '';
	
	var txtValRubro = document.getElementById(valorRubro).value;
	var rubro = document.getElementById(idRubro).options.selectedIndex;
	var txtRubro= document.getElementById(idRubro).options[rubro].text;
	var txtIdRubro = document.getElementById(idRubro).value;
	var txtCodImp = document.getElementById(idImpuesto).value;
	var impuesto = document.getElementById(detImpuesto).options.selectedIndex;
	var txtImpuesto= document.getElementById(detImpuesto).options[impuesto].text;
	var txtNumeroRet = document.getElementById(retencion).value;
	var txtCaja = document.getElementById(caja).value;
	var txtCuota = document.getElementById(cuota).value;
	var txtFactura = document.getElementById(factura).value;
	var numeroGuia = document.getElementById("numguia").value;
	var tipo='retencion';
	
	if(txtNumeroRet!=0 && (txtRubro!='---' || txtRubro==null )){
		if(txtValRubro > saldofactura){
			alert('El valor ingresado $' + txtValRubro + ' es mayor al saldo y/o cuota de la factura \n\r El valor pendiente solo es: $' + saldofactura + '');
			var numero = document.getElementById("numguia").value;
			destinoAJAX = "contenido";	
			loadXMLDoc();
			xmlhttp.open("POST","GuiaCobro.php", true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("numguia=" + encodeURIComponent(numero));
			xmlhttp.send();
			GuiaCobro(numeroGuia)
		}else{
			//alert('ValorRubro: ' + txtValRubro + '\n IdRubro: ' + txtIdRubro + '\n guia: ' + numeroGuia + '\n CodImp: ' + txtCodImp + '\n DetImpuesto: ' + txtImpuesto.substr(6) + '\n retencion: ' + txtNumeroRet + '\n Caja: ' + txtCaja + '\n Cuota: ' + txtCuota + '\n factura: ' + txtFactura + '\n tipo: ' + tipo + '\n numero: ' + numero_orden + '\n Detalle Rubro: ' + txtRubro);
			destinoAJAX = "contenido";	
			$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
			loadXMLDoc();	
			xmlhttp.open("GET","EditarRegistro.php?caja=" + txtCaja + "&guia=" + numeroGuia + "&valor="+ txtValRubro + "&cuota=" + txtCuota + "&retencion=" + txtNumeroRet + "&codImp=" + txtCodImp + "&detImp=" + txtImpuesto.substr(6) + "&factura=" + txtFactura + "&tipo=" + tipo + "&numero=" + numero_orden + "&idRubro=" + txtIdRubro, true);
			xmlhttp.send();
			GuiaCobro(numeroGuia)
		}
	}else{
		if(txtNumeroRet==0){
			alert('No ha ingresado el Numero de RETENCION!!');
		}
		if((txtRubro=='---' || txtRubro==null ) && txtNumeroRet!=0 ){
			alert('Debe escoger el RUBRO!!');
		}
	}
}

//FUNCION PARA BUSCAR UNA GU�A POR NUMERO INGRESADO
function BuscaGuia(input){
	var valor = input.value.replace(/\./g,'');
	if(isNaN(valor)){
	  	alert('Solo se permiten numeros  en \xe9ste Campo!!');
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}else{
		var numero = input.value;
		destinoAJAX = "contenido";	
		$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
		loadXMLDoc();
		xmlhttp.open("POST","GuiaCobro.php", true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("numguia=" + encodeURIComponent(numero));
		xmlhttp.send();
		GuiaCobro()
	}
}

//FUNCION CON LA CUAL SE NAVEGA EN LOS REGISTROS HACIA ADELANTE
function siguiente(ultimo){
	var numero = document.getElementById("numguia").value;
	if(numero >= ultimo ){
		 alert('\xc9ste es el \xdaltimo registro ');
		 document.getElementById("numguia").value = ultimo;
	}else{
		suma = parseInt(numero) + 1;
		destinoAJAX = "contenido";	
		loadXMLDoc();
		xmlhttp.open("POST","GuiaCobro.php", true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("numguia=" + encodeURIComponent(suma));
		xmlhttp.send();
		GuiaCobro()
	}

}
//FUNCION CON LA CUAL SE NAVEGA EN LOS REGISTROS HACIA ATRAS
function anterior(numero){
	if(numero <= 1 ){
		 alert('\xc9ste es el primer registro ');
		 document.getElementById("numguia").value = 1;
	}else{
		resta = parseInt(numero) - 1;
		destinoAJAX = "contenido";	
		loadXMLDoc();
		xmlhttp.open("POST","GuiaCobro.php", true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("numguia=" + encodeURIComponent(resta));
		xmlhttp.send();
		GuiaCobro()
	}
}

function calcular_prueba(input,tipo){
	switch(tipo){
	case "billetes":
		var uno = input.value;
		var denominacion = document.getElementById("uno").value;
		total = uno * denominacion;
		
		document.getElementById("totaluno").value = total;
				
	break;
	}
}

//FUNCION CHECKBOXES
function check(contador){
	//alert('Hola');
	chkbox = "chequepos" + contador;
	Link = "H" + contador;
	var chk = document.getElementById(chkbox);
	 var lnk = document.getElementById(Link);
	if(chk.checked==true){
		 chk.checked = false;
		 $(lnk).removeClass("check_ON");
		 $(lnk).addClass("check_OFF");
	  }else{
		  chk.checked = true;
		 $(lnk).removeClass("check_OFF");
		 $(lnk).addClass("check_ON");
	  }
}

function detalleefectivo(guia, tipo){
	$("ul.submenu").hide();
	switch(tipo){
	case 'billetes':
		destinoAJAX = "detallebilletes";	
		loadXMLDoc();
		xmlhttp.open("GET","DetalleBilletes.php?guia=" + guia + "&tipo=" + tipo, true);
		break;
	case 'monedas':
		destinoAJAX = "detallemonedas";	
		loadXMLDoc();
		xmlhttp.open("GET","DetalleMonedas.php?guia=" + guia + "&tipo=" + tipo, true);
		break;
	}
	xmlhttp.send();
	$("#" + destinoAJAX).show();
}

function calcular(input, guia, tipo){
	switch (tipo){
		case 'billetes':
			var valor = input.value.replace(/\./g,'');
			if(isNaN(valor)){
			  	alert('Solo se permiten numeros  en \xe9ste Campo!!');
				input.value = input.value.replace(/[^\d\.]*/g,'');
			}else{
				switch (input.id){
					case 'unoval':
						var cant = document.getElementById("uno").value;
						calculo = valor*cant;
						document.getElementById("totaluno").value = calculo;
							destinoAJAX = "detallebilletes";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 1 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
				
					break;
					case 'cincoval':
						var cant = document.getElementById("cinco").value;
						calculo = valor*cant;
						document.getElementById("totalcinco").value = calculo;
							destinoAJAX = "detallebilletes";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 5 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
							//location.href="InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 5 + "&cantidad=" + valor;
					break;
					case 'diezval':
						var cant = document.getElementById("diez").value;
						calculo = valor*cant;
						document.getElementById("totaldiez").value = calculo;
							destinoAJAX = "detallebilletes";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 10 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
							//location.href="InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 10 + "&cantidad=" + valor;
					break;
					case 'veinteval':
						var cant = document.getElementById("veinte").value;
						calculo = valor*cant;
						document.getElementById("totalveinte").value = calculo;
							destinoAJAX = "detallebilletes";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 20 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
							//location.href="InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 20 + "&cantidad=" + valor;
					break;
					case 'cincuentaval':
						var cant = document.getElementById("cincuenta").value;
						calculo = valor*cant;
						document.getElementById("totalcincuenta").value = calculo;
							destinoAJAX = "detallebilletes";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 50 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
							//location.href="InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 50 + "&cantidad=" + valor;
					break;
					case 'cienval':
						var cant = document.getElementById("cien").value;
						calculo = valor*cant;
						document.getElementById("totalcien").value = calculo;
							destinoAJAX = "detallebilletes";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 100 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
					break;
				}
			}
		break;
		case 'monedas':
			var valor = input.value.replace(/\./g,'');
			if(isNaN(valor)){
			  	alert('Solo se permiten numeros  en \xe9ste Campo!!');
				input.value = input.value.replace(/[^\d\.]*/g,'');
			}else{
				switch (input.id){
					case 'unocentval':
						var cant = document.getElementById("unocent").value;
						calculo = valor*cant;
						document.getElementById("totalunocent").value = calculo;
							destinoAJAX = "detallemonedas";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 0.01 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
					break;
					case 'cincocentval':
						var cant = document.getElementById("cincocent").value;
						calculo = valor*cant;
						document.getElementById("totalcincocent").value = calculo;
							destinoAJAX = "detallemonedas";	
							loadXMLDoc();
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 0.05 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
					break;
					case 'diezcentval':
						var cant = document.getElementById("diezcent").value;
						calculo = valor*cant;
						document.getElementById("totaldiezcent").value = calculo;
							destinoAJAX = "detallemonedas";	
							loadXMLDoc();	
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 0.10 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
					break;
					case 'veinticincocentval':
						var cant = document.getElementById("veinticincocent").value;
						calculo = valor*cant;
						document.getElementById("totalveinticincocent").value = calculo;
							destinoAJAX = "detallemonedas";	
							loadXMLDoc();	
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 0.25 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
					break;
					case 'cincuentacentval':
						var cant = document.getElementById("cincuentacent").value;
						calculo = valor*cant;
						document.getElementById("totalcincuentacent").value = calculo;
							destinoAJAX = "detallemonedas";	
							loadXMLDoc();	
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 0.50 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
					break;
					case 'ciencentval':
						var cant = document.getElementById("ciencent").value;
						calculo = valor*cant;
						document.getElementById("totalciencent").value = calculo;
							destinoAJAX = "detallemonedas";	
							loadXMLDoc();	
							xmlhttp.open("GET","InsertarNuevoRegistro.php?guia=" + guia + "&tipo=" + tipo + "&denominacion=" + 1.00 + "&cantidad=" + valor, true);
							xmlhttp.send();
							detalleefectivo(guia, tipo);
					break;
				}
			}
		break;
	}
}
			
function editarregistroefectivo(input, numero, tipo, denominacion){
	var num = input.value.replace(/\./g,'');
  	if(isNaN(num)){
  	  	alert('Solo se permiten numeros en \xe9ste Campo!!');
  		input.value = input.value.replace(/[^\d\.]*/g,'');
  		input.focus();
  	}else{
  		destinoAJAX = "contenido";	
		$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
		loadXMLDoc();	
		xmlhttp.open("GET","EditarRegistro.php?guia=" + numero + "&tipo=" + tipo + "&denominacion=" + denominacion + "&cantidad=" + num, true);
		xmlhttp.send();
		GuiaCobro(numero)
  	}
}

function eliminaregistro(factura, cuota, guia, tipo, caja, row, denominacion){
	switch(tipo){
		case "billetes":
			if (confirm("Eligi\xf3 ( ELIMINAR ) Billetes de $" + denominacion + ".00 .!! \n\n Desea Eliminarlo?.." )){
				destinoAJAX = "detalleefectivo";	
				loadXMLDoc();
				xmlhttp.open("GET","EliminarRegistro.php?numfac=" + factura + "&cuota=" + cuota + "&guia=" + guia + "&tipo=" + tipo + "&caja=" + caja + "&row=" + row + "&denominacion=" + denominacion, true);
				xmlhttp.send();
				detalleefectivo(guia, tipo);
			}
		break;
		case "monedas":
			if (confirm("Eligi\xf3 ( ELIMINAR ) Monedas de $" + denominacion + " .!! \n\n Desea Eliminarlo?.." )){
				destinoAJAX = "detalleefectivo";	
				loadXMLDoc();
				xmlhttp.open("GET","EliminarRegistro.php?numfac=" + factura + "&cuota=" + cuota + "&guia=" + guia + "&tipo=" + tipo + "&caja=" + caja + "&row=" + row + "&denominacion=" + denominacion, true);
				xmlhttp.send();
				detalleefectivo(guia, tipo);
			}
		break;
		case "efectivo":
			if (confirm("Eligi\xf3 ( ELIMINAR ) EL Registro.!! \n\n Desea Eliminarlo?.." )){
				destinoAJAX = "contenido";	
				loadXMLDoc();
				xmlhttp.open("GET","EliminarRegistro.php?numfac=" + factura + "&cuota=" + cuota + "&guia=" + guia + "&tipo=" + tipo + "&caja=" + caja + "&row=" + row + "&denominacion=" + denominacion, true);
				xmlhttp.send();
				GuiaCobro(guia);
			}
		break;
		case "cheque":
			if (confirm("Eligi\xf3 ( ELIMINAR ) EL Registro.!! \n\n Desea Eliminarlo?.." )){
				destinoAJAX = "contenido";	
				loadXMLDoc();
				xmlhttp.open("GET","EliminarRegistro.php?numfac=" + factura + "&cuota=" + cuota + "&guia=" + guia + "&tipo=" + tipo + "&caja=" + caja + "&row=" + row + "&denominacion=" + denominacion, true);
				xmlhttp.send();
				GuiaCobro(guia);
			}
		break;
		case "transferencia":
			if (confirm("Eligi\xf3 ( ELIMINAR ) EL Registro.!! \n\n Desea Eliminarlo?.." )){
				destinoAJAX = "contenido";	
				loadXMLDoc();
				xmlhttp.open("GET","EliminarRegistro.php?numfac=" + factura + "&cuota=" + cuota + "&guia=" + guia + "&tipo=" + tipo + "&caja=" + caja + "&row=" + row + "&denominacion=" + denominacion, true);
				xmlhttp.send();
				GuiaCobro(guia);
			}
		break;
		case "tarjeta":
			if (confirm("Eligi\xf3 ( ELIMINAR ) EL Registro.!! \n\n Desea Eliminarlo?.." )){
				destinoAJAX = "contenido";	
				loadXMLDoc();
				xmlhttp.open("GET","EliminarRegistro.php?numfac=" + factura + "&cuota=" + cuota + "&guia=" + guia + "&tipo=" + tipo + "&caja=" + caja + "&row=" + row + "&denominacion=" + denominacion, true);
				xmlhttp.send();
				GuiaCobro(guia);
			}
		break;
		case "retencion":
			if (confirm("Eligi\xf3 ( ELIMINAR ) EL Registro.!! \n\n Desea Eliminarlo?.." )){
				destinoAJAX = "contenido";	
				loadXMLDoc();
				xmlhttp.open("GET","EliminarRegistro.php?numfac=" + factura + "&cuota=" + cuota + "&guia=" + guia + "&tipo=" + tipo + "&caja=" + caja + "&row=" + row + "&denominacion=" + denominacion, true);
				xmlhttp.send();
				GuiaCobro(guia);
			}
		break;
	}	
} 

function guardarGuia(valor,transf,tarjeta,ret,diferencia, guia, bit){
	
	if (bit>0){
		if(valor==0 && transf==0 && tarjeta==0 && ret==0 && diferencia==0){
			var totmonedas = document.getElementById("totmonedas").value;
			var totbilletes = document.getElementById("totbilletes").value;
			var totefectivo = document.getElementById("totefectivo").value;
			var totcheques = document.getElementById("totcheques").value;
			var totfacturas = document.getElementById("totfacturas").value;
			var diferencia = document.getElementById("diferencia").value;
			var totalcobranza = document.getElementById("totalcobranza").value;
			var totalTransferencia = document.getElementById("totTransf").value;
			var totalTarjeta = document.getElementById("totTarjetas").value;
			var totalRet = document.getElementById("totRet").value;
			
			destinoAJAX = "contenido";	
			//$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
			//loadXMLDoc();	
			xmlhttp.open("GET","GrabarGuiaCobro.php?guia=" + guia + "&totmonedas=" + totmonedas + "&totefectivo=" + totefectivo + "&totbilletes=" + totbilletes + "&totcobranza=" + totalcobranza + "&totcheques=" + totcheques + "&totTransf=" + totalTransferencia + "&totTarjeta=" + totalTarjeta + "&totRet=" + totalRet, true);
			xmlhttp.send();
			//GuiaCobro(guia);
			//alert('Guia Grabada Con Exito !!');
		}
	}else{
		alert('La Guia NO puede grabarse con Valor ( 0 ) Verifique por favor!!');
	}	
	if(valor>0){
		alert('Hay uno o mas CHEQUES con datos incompletos \n Revise por favor!!');
	}else if(transf>0){
		alert('Hay una o mas TRANSFERENCIAS con datos incompletos \n Revise por favor!!');
	}else if(tarjeta>0){
		alert('Hay una o mas TARJETAS con datos incompletos \n Revise por favor!!');
	}else if(ret>0){
		alert('Hay una o mas RETENCIONES con datos incompletos \n Revise por favor!!');
	}
	
	if(diferencia!=0){
		alert('El valor de facturas recaudadas con el valor de Monedas y/o Billetes no es el mismo \n Revise por favor!!');
	}
}

function NuevaGuiaCobro(num){
		destinoAJAX = "contenido";	
		$("#contenido").html('<br><br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
		loadXMLDoc();	
		xmlhttp.open("GET","NuevaGuiaCobro.php", true);
		xmlhttp.send();
		GuiaCobro()
}

function FacturasP(guia,tipo){ 
	$("#Block").show();
	destinoAJAX = "facturasPendientes";	
	$("#facturasPendientes").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
	loadXMLDoc();	
	xmlhttp.open("GET","FacturasPendientes.php?guia=" + guia + "&tipo=" + tipo, true);
	xmlhttp.send();
	$("#facturasPendientes").show();
}

function facturaxCliente(input,guia,tipo){
	var valor = input.value.replace(/\./g,'');
	if(isNaN(valor)){
	  	alert('Solo se permiten numeros  en \xe9ste Campo!!');
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}else{
		var factura = input.value;
		$("#Block").show();
		destinoAJAX = "facturasPendientes";	
		$("#facturasPendientes").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
		loadXMLDoc();	
		xmlhttp.open("POST","FacturasPendientes1.php",true); 
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("numfactura=" + encodeURIComponent(factura) + "&guia=" + encodeURIComponent(guia) + "&tipo="+encodeURIComponent(tipo) + "&NoCache=" + Math.random());
		xmlhttp.send();
	}
}

function InsertarFactura(numero, valor, guia, tipo, cuota, caja){
	if (tipo == 'retencion'){
		if(confirm('Desea Conservar el Valor de la Factura?')){
			
		}else{
			valor = prompt('Ingrese el nuevo valor!',valor);
		}
	}
	//alert('numero: ' + numero + '\n valor: ' + valor + '\n guia: ' + guia + '\n tipo: ' + tipo + '\n cuota: ' + cuota + '\n caja: ' + caja);
	destinoAJAX = "contenido";	
	loadXMLDoc();	
	xmlhttp.open("GET","InsertarNuevoRegistro.php?numfac=" + numero + "&valor=" + valor + "&guia=" + guia + "&tipo=" + tipo + "&cuota=" + cuota + "&caja=" + caja, true);
	xmlhttp.send();
	GuiaCobro(guia);
}

function GuiaCobro(num){ 
	$("#facturasPendientes").hide();
	$("#Block").hide();
	$("ul.submenu").hide();
	$("ul.submenu").toggle();
	destinoAJAX = "contenido";	
	loadXMLDoc();	
	xmlhttp.open("GET","GuiaCobro.php?guia=" + num, true);
	xmlhttp.send();
	$("ul.submenu").toggle();
	$("#contenido").show();
}

function ConsultaClientes(){
	$("#facturasPendientes").hide();
	$("#Block").hide();
	$("ul.submenu").hide();
	$("ul.submenu").toggle();
	destinoAJAX = "contenido";	
	loadXMLDoc();	
	xmlhttp.open("GET","ConsultaClientes.php", true);
	xmlhttp.send();
	$("ul.submenu").toggle();
	$("#contenido").show();
}

function BusquedaCliente(tipo){
	var parametro = document.getElementById("buscarCli").value;
	if (parametro == ''){
		alert('Ingrese alg\xfan par\xe1metro de b\xfasquedA!!!!');
	}else{
		
		var radio_Nombre = document.getElementById("R_nombre");
		var radio_Rucced = document.getElementById("R_rucced");
		var radio_codigo = document.getElementById("R_codigo");
		var radio_monitoreo = document.getElementById("R_monitoreo");

		if (radio_Nombre.checked == true){
			destinoAJAX = "buscaparametro";	
			loadXMLDoc();	
			xmlhttp.open("GET","BuscarClientes.php?parametro=" + parametro + "&criterio=" + radio_Nombre.value + "&tipobusqueda=" + tipo, true);
			xmlhttp.send();
			$("#buscaparametro").show();
		}
		if (radio_Rucced.checked == true){
			destinoAJAX = "buscaparametro";	
			loadXMLDoc();	
			xmlhttp.open("GET","BuscarClientes.php?parametro=" + parametro + "&criterio=" + radio_Rucced.value + "&tipobusqueda=" + tipo, true);
			xmlhttp.send();
			$("#buscaparametro").show();
		}
		if (radio_codigo.checked == true){
			destinoAJAX = "buscaparametro";	
			loadXMLDoc();	
			xmlhttp.open("GET","BuscarClientes.php?parametro=" + parametro + "&criterio=" + radio_codigo.value + "&tipobusqueda=" + tipo, true);
			xmlhttp.send();
			$("#buscaparametro").show();	
		}
		if(tipo != 'proforma'){
			if (radio_monitoreo.checked == true){
				destinoAJAX = "buscaparametro";	
				loadXMLDoc();	
				xmlhttp.open("GET","BuscarClientes.php?parametro=" + parametro + "&criterio=" + radio_monitoreo.value + "&tipobusqueda=" + tipo, true);
				xmlhttp.send();
				$("#buscaparametro").show();	
			}
		}
	}
}

function salidaClientes(){
	destinoAJAX = "contenido";	
	loadXMLDoc();	
	xmlhttp.open("GET","SalidaClientes.php", true);
	xmlhttp.send();
	$("ul.submenu").toggle();
	$("#contenido").show();
}

function ConsultaProductos(tipo){
	destinoAJAX = "contenido";	
	loadXMLDoc();	
	xmlhttp.open("GET","ConsultaProductos.php?tipo=" + tipo, true);
	xmlhttp.send();
	$("ul.submenu").toggle();
	$("#contenido").show();
}

function BusquedaProductos(tipo, idProforma){
	var parametro = document.getElementById("criterio").value;
	if (parametro == ''){
		alert('Ingrese alg\xfan par\xe1metro de b\xfasquedA!!!!');
	}else{
		var radio_Nombre = document.getElementById("R_nombre");
		var radio_codigo = document.getElementById("R_codigo");
		//alert(radio_Nombre.value + ' -- ' +  radio_codigo.value + ' -- ' + parametro);
		if (radio_Nombre.checked == true){
			destinoAJAX = "buscaparametro";	
			loadXMLDoc();	
			xmlhttp.open("GET","buscarProducto.php?criterio=" + parametro + "&opcion=" + radio_Nombre.value + "&tipo=" + tipo + "&numProforma=" + idProforma, true);
			xmlhttp.send();
			$("#buscaparametro").show();
		}
		if (radio_codigo.checked == true){
			destinoAJAX = "buscaparametro";	
			loadXMLDoc();	
			xmlhttp.open("GET","buscarProducto.php?criterio=" + parametro + "&opcion=" + radio_codigo.value + "&tipo=" + tipo + "&numProforma=" + idProforma, true);
			xmlhttp.send();
			$("#buscaparametro").show();
		}
	}
}

function cancelaServicio(codCliente, contador, codMonitoreo){
	//alert('\n Cliente:'+ codCliente + '\n Monitoreo:' + codMonitoreo + '\n Contador:' + contador + '\n Nombre:' + NombreCli);
	var NombreCli = document.getElementById("nomFacturacion" + contador);
	$("ul.submenu").toggle();
	$("ul.submenu").hide();
	destinoAJAX = "buscaparametro";	
	loadXMLDoc();	
	xmlhttp.open("GET","SalidaClientesIngreso.php?cliente=" + codCliente + "&monitoreo=" + codMonitoreo + "&nomCliente=" + NombreCli.value, true);
	xmlhttp.send();
	$("ul.submenu").hide();
}

function ingresoGestionSalida(tipo, codCliente, codMonitoreo, valida){
	if(valida > 0){
		alert('Ya existe un registro en proceso para este CLIENTE');
		salidaClientes();
		$("ul.submenu").hide();
	}else{
		var txtMotivo = document.getElementById("motivo").options.selectedIndex;
		var motivo= document.getElementById("motivo").options[txtMotivo].text;
		var observacion = document.getElementById("observacion").value;
		var contacto = document.getElementById("contacto").value;
		if(motivo == null || motivo == '---'){
			alert('Por favor escoja un MOTIVO para continuar!!');
		}else if(observacion == null || observacion==''){
			alert('Por Favor ingrese una observaci�n!!');
		}else if(contacto == null || contacto==''){
			alert('Por Favor ingrese el NOMBRE del contacto!!');
		}else{
			//alert('\n Cliente:'+ codCliente + '\n Monitoreo:' + codMonitoreo + '\n Tipo:' + tipo + '\n motivo:' + motivo + '\n observacion:' + observacion);
			destinoAJAX = "buscaparametro";	
			loadXMLDoc();	
			xmlhttp.open("GET","InsertarNuevoRegistro.php?tipo=" + tipo + "&cliente=" + codCliente + "&codMonitoreo=" + codMonitoreo + "&gestion=" + motivo + "&observacion=" + observacion + "&contacto=" + contacto, true);
			xmlhttp.send();
			$("ul.submenu").toggle();
			salidaClientes();
			alert('Registro Satisfactorio');
			$("ul.submenu").hide();
		}
	}
}

function proformas(num){
	$("#facturasPendientes").hide();
	$("#Block").hide();
	$("ul.submenu").hide();
	$("ul.submenu").toggle();
	destinoAJAX = "contenido";	
	loadXMLDoc();	
	xmlhttp.open("GET","proforma.php?num=" + num, true);
	xmlhttp.send();
	$("ul.submenu").toggle();
	$("#contenido").show();
}

function proformaSeccion(seccion,num){
	$("#facturasPendientes").hide();
	loadXMLDoc();	
	xmlhttp.open("GET","proforma.php?num=" + num, true);
	xmlhttp.send();
	$("#" + seccion).addClass('active');
	$("#" + seccion).show();
}

function nevegadorProf(tipo, num){
	ultimo = $('#txtUltimo').val();
	switch (tipo) {
		case 'pri':
			num = parseInt(num) - 100;
			if(num <  0){
				num=0;
			}
			break;
		case 'ant':
			num = parseInt(num) - 1;
			break;
		case 'sig':
			num = parseInt(num) + 1;
			break;
	}
	if(num <= ultimo){
		$("#facturasPendientes").hide();
		$("#Block").hide();
		$("ul.submenu").hide();
		destinoAJAX = "contenido";	
		loadXMLDoc();	
		xmlhttp.open("GET","proforma.php?num=" + num, true);
		xmlhttp.send();
		$("#contenido").show();
	}
}

function insertaNuevoCliente(tipo){
	num = $('#txtNumPof').val();
	if (tipo == 'total'){
		textBoton ='Crea Proforma';
	}else{
		textBoton ='Editar Proforma';
	}
	$("#buscaparametro").empty();
	$("#buscaparametro").attr('style','background-color:#F2F2F2').append(
		$('<br>'),
		$('<form>').append(
			$('<input>').attr('type','hidden').attr('maxlength',150).attr('id','txtcontacto1'),
			$('<input>').attr('type','hidden').attr('maxlength',150).attr('id','txtCodCliente1'),
			$('<table>').append(
				$('<tr>').append(
					$('<td>').append(
						$('<label>').addClass('').text('Nombre :')
					),
					$('<td>').append(
						$('<input>').attr('type','text').attr('maxlength',150).addClass('form-control form-control-sm text-left text-transform:uppercase  border-1').attr('style','background-color:transparent').attr('id','txtNomCliente1').attr('placeholder','Ingrese nombre del Cliente').attr('required','required'),
					)
				),
				$('<tr>').append(
					$('<td>').append(
						$('<label>').addClass('').text('Ruc/Ced. :')
					),
					$('<td>').append(
						$('<input>').attr('type','number').attr('maxlength',13).addClass('form-control form-control-sm text-left text-transform:uppercase border-1').attr('style','background-color:transparent').attr('id','txtRuc1').attr('placeholder','Ingrese RUC o C.C.').attr('required','required'),
					)
				),
				$('<tr>').append(
					$('<td>').append(
						$('<label>').addClass('').text('Teléfono :')
					),
					$('<td>').append(
						$('<input>').attr('type','number').attr('maxlength',10).addClass('form-control form-control-sm text-left text-transform:uppercase  border-1').attr('style','background-color:transparent').attr('id','txtTelefono1').attr('placeholder','Ingrese Teléfono').attr('required','required')
					)
				),
				$('<tr>').append(
					$('<td>').append(
						$('<label>').addClass('').text('Dirección :')
					),
					$('<td>').append(
						$('<input>').attr('type','text').attr('maxlength',300).addClass('form-control form-control-sm text-left text-transform:uppercase  border-1').attr('style','background-color:transparent').attr('id','txtDireccion1').attr('placeholder','Ingrese Dirección').attr('required','required')
					)
				),
				$('<br>'),
				$('<tr>').append(
					$('<td colspan="2">').append(
						$('<center>').append(
							$('<button>').attr('type','button').addClass('btn btn-sm btn-success').attr('id','btnEnviar').text(textBoton),
						)
					)
				),
			)
		)
	);
	if (tipo == 'total'){
		$('#btnEnviar').attr('onClick','creaProforma(\'cabecera\',1,1);');
	}else{
		$('#btnEnviar').attr('onClick','editaCambiosProforma(' + num + ',\'cliente\',1);');
	}
}

function nuevaProforma(tema){
	calculaTotalesProforma($('#txtNumProforma').val());
	$("#Block").show();
	destinoAJAX = "agregaFicha";	
	$("#agregaFicha").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
	loadXMLDoc();	
	xmlhttp.open("GET","nuevaProforma.php?busqueda=" + tema, true);
	xmlhttp.send();
	$("#agregaFicha").show();
}

function editaClienteProforma(tema, proforma){
	calculaTotalesProforma($('#txtNumProforma').val());
	$("#Block").show();
	destinoAJAX = "agregaFicha";	
	$("#agregaFicha").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
	loadXMLDoc();	
	xmlhttp.open("GET","nuevaProforma.php?busqueda=" + tema + "&num=" + proforma, true);
	xmlhttp.send();
	$("#agregaFicha").show();
}

function duplicaProforma(num){
	calculaTotalesProforma(num);
	var r = confirm('\u00BFEstá seguro de DUPLICAR esta Proforma?.');
	if(r==true){
		var json_data ={
			tipo : 'duplicar',
			numProforma : num
		};
		$.ajax({
			type: "POST",
			url: "insertarProforma.php",
			dataType : "json",
			data: json_data,
			success:function(r){
				if(r['0']==0){
					alert("Error al Crear la nueva proforma");
				}else{
					alert("Se Duplicó Correctamente la Proforma # " + num);
					proformas(json_data.numero);
				}
			}
		});
	}
}

function listarProformasxCliente(cont){
	idCliente = $('#txtCodCliente' + cont).val();
	nomCliente = $('#txtNomCliente' + cont).val();
	nomContacto = $('#txtContacto' + cont).val();
	rucCliente = $('#txtRuc' + cont).val();
	//destinoAJAX = "listaProf" + cont;	
	destinoAJAX = "buscaparametro";	
	loadXMLDoc();	
	xmlhttp.open("GET","proformasxCliente.php?nombre=" + nomCliente + "&id=" + idCliente + "&ruc=" + rucCliente + "&contacto=" + nomContacto , true);
	xmlhttp.send();
	//$("#listaProf" + cont).show();
	$("#buscaparametro").show();
}

function nuevoRegistroProforma(idProforma, tipo, detProductos){
	calculaTotalesProforma(idProforma);
	$("#Block").show();
	destinoAJAX = "agregaFicha";	
	$("#agregaFicha").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
	loadXMLDoc();	
	xmlhttp.open("GET","nuevoRegistroProforma.php?numProforma=" + idProforma + "&tipo=" + tipo + "&detalle=" + detProductos, true);
	xmlhttp.send();
	$("#agregaFicha").show();

}

function cierraVentana(){
	$("#agregaFicha").hide();
	$("#Block").hide();
}

function editaCambiosProforma(num, tipo, cont){
	ursCreacion = $('#txtUsrCreacion').val();
	usrActivo = $('#txtUsrActivo').val();
	hayProducto = $('#txtContadorTotSrv').val();
	if(ursCreacion == usrActivo){
		switch (tipo) {
			case 'cabecera':
				var fecha = new Date($('#txtFecha').val());
				var dia = fecha.getDate();
				var mes = ("0" + (fecha.getMonth() + 1));
				var anio = fecha.getFullYear();
				var fechaTot = anio + "/" + mes + "/" + dia;
				var json_data = {
					numero : num,
					tipo: tipo,
					fecha : fechaTot,
					direccion : $('#txtDireccion').val(),
					email : $('#txtEmail').val(),
					vendedor : $('#txtVendedor').val(),
					asesor : $('#txtAsesor').val(),
					formaPago : $('#txtFormaPago').val(),
					diasPlazo : $('#txtDiasFP').val(),
					canalCom : $('#txtCanalC').val(),
					observacion : $('#txtObservacion').val()
				};
				formaPagoInicial = $('#fpInicial').val();
				if(hayProducto > 1){
					$('#txtFormaPago').val(formaPagoInicial);
					alert('NO puede cambiar la forma de pago si existen Productos ingresados.\nElimine los detalles y para poder cambiarla.!');
					return;
				} 

				$.ajax({
					type: "POST",
					url: "editarDatosProforma.php",
					dataType : "json",
					data: json_data,
					success:function(r){
						if(r==1){
							alert("Se creó la Proforma Correctamente");
						}else{
							alert("Error al Crear la nueva proforma");
						}
					}
				});

				proformas(json_data.numero);
				break;
			case 'cliente':
				if($('#txtNomCliente' + cont).val() == ''){
					alert('El Campo Nombre No debe estar vacio..');
					return;
				}else if($('#txtNomCliente' + cont).val().length > 150 ){
					alert('El Campo Nombre No debe contener mas de 150 caracteres ..');
					return;
				}
				if($('#txtRuc' + cont).val() == ''){
					alert('El Campo Ruc/Ced No debe estar vacio..');
					return;
				}else if($('#txtRuc' + cont).val().length > 13 || $('#txtRuc' + cont).val().length < 10){
					alert('El Campo Ruc/Ced debe contener entre 10 y 13 caracteres ..');
					return;
				}

				if($('#txtTelefono' + cont).val() == ''){
					alert('El Campo Teléfono No debe estar vacio..');
					return;
				}else if($('#txtTelefono' + cont).val().length > 10){
					alert('El Campo Teléfono No debe contener mas de 10 caracteres ..');
					return;
				}

				if($('#txtDireccion' + cont).val() == ''){
					alert('El Campo Dirección No debe estar vacio..');
					return;
				}else if($('#txtDireccion' + cont).val().length > 300){
					alert('El Campo Dirección No debe contener mas de 300 caracteres ..');
					return;
				}
				if($('#txtcontacto' + cont) == null || $('#txtcontacto' + cont) == ''){
					$('#txtcontacto' + cont).val($('#txtNomCliente' + cont).val());
				}
				var json_data = {
					tipo: tipo,
					numero : num,
					telefono : $('#txtTelefono' + cont).val(),
					codigo : $('#txtCodCliente' + cont).val(),
					cliente : $('#txtcontacto' + cont).val(),
					nombre : $('#txtNomCliente' + cont).val(),
					ruc : $('#txtRuc' + cont).val(),
					direccion : $('#txtDireccion' + cont).val()
				};

				$.ajax({
					type: "POST",
					url: "editarDatosProforma.php",
					dataType : "json",
					data: json_data,
					success:function(r){
						if(r==1){
							alert("Se creó la Proforma Correctamente");
						}else{
							alert("Error al Crear la nueva proforma");
						}
					}
				});
				proformas(json_data.numero);
				break;
				
			case 'servicioDetalle':
				valorDesc = $('#txtPrecioB' + cont).val();
				ValorMin = $('#txtValMinimo' + cont).val();
				descuento = $('#txtDscto' + cont).val();
				precioUnidad = $('#txtPUniadSrv' + cont).val();
				catProducto = $('#txtCategoria' + cont).val();
				formaPago = $('#txtFormaPago').val();
				
				if(formaPago == 1){
					//SI LA FORMA DE PAGO ES CONTADO, SE LIBERA EL DESCUENTO DEL 5% AL VALOR MINIMO
					valorDesc = ValorMin;
				}

				if(parseFloat(precioUnidad) < parseFloat(ValorMin)) {
					alert('El Valor ingresado en el item ' + cont + ',\nNo puede ser menor al Mínimo <$ '+ ValorMin +'>');
					$('#txtPUniadSrv' + cont).val(ValorMin);
					precioUnidad = ValorMin;
					$('#txtDscto' + cont).val(0);
					descuento = 0;

				}else if(parseFloat(descuento) > 5 || parseFloat(descuento) < 0){
					alert('El porcentaje de Descuento en el item ' + cont + ',\nNo puede ser Mayor al 5%');
					$('#txtDscto' + cont).val(0);
					descuento = 0;
				}else if(parseFloat(precioUnidad) < parseFloat(valorDesc) && parseFloat(descuento) != 0){
					alert('Descuento No aplica en el item ' + cont + ',\nEl valor unitario es menor al Requerido ($ ' + valorDesc + ')');
					$('#txtDscto' + cont).val(0);
					descuento = 0;
				}
				
				if($('#txtEnterasSrv' + cont).val() < 0){
					$('#txtEnterasSrv' + cont).val(0);

				}

				if($('#txtUnidadesSrv' + cont).val() < 0){
					$('#txtUnidadesSrv' + cont).val(0);

				}
				if($('#txtEnterasSrv' + cont).val() == 0 && $('#txtUnidadesSrv' + cont).val() == 0){
					$('#txtUnidadesSrv' + cont).val(1);
				}

				if(catProducto != 1){
					if(precioUnidad < valorDesc){
						alert('El descuento no Aplica en Materiales');
						$('#txtPUniadSrv' + cont).val(valorDesc);
						precioUnidad = valorDesc;
					}else if(descuento != 0 ){
						alert('El descuento no Aplica en Materiales');
						$('#txtDscto' + cont).val(0);
						descuento = 0;
					}
				}

				unidadxEntera = $('#txtUnidadxentera' + cont).val();
				enteras = $('#txtEnterasSrv' + cont).val();
				unidades = $('#txtUnidadesSrv' + cont).val();

				precioEntera = parseFloat(precioUnidad) * parseFloat(unidadxEntera);
				$('#txtPEnteraSrv' + cont).val(precioEntera.toFixed(2));
				totalItem = (((parseFloat(enteras) * parseFloat(unidadxEntera) + parseFloat(unidades)) * parseFloat(precioUnidad)) - ((((parseFloat(enteras) * parseFloat(unidadxEntera)) + parseFloat(unidades)) * parseFloat(precioUnidad))* parseFloat(descuento) / 100));
				$('#txtTotSrv' + cont).val(totalItem.toFixed(2));
				ivaItem = parseFloat(totalItem) * 0.12;
				$('#txtIvaSrv' + cont).val(ivaItem.toFixed(2));

				calculaTotalesProforma(num);

				//alert(subtotal + ' -- ' + ivaTotal + ' -- ' + totProforma);
				var json_data = {
					tipo : tipo,
					numProforma : num,
					totRegistro : totalItem,
					idProducto : $('#txtIdProductoSrv'+ cont).val().replace('@',' '),
					dscto : descuento,
					cantEnteras : enteras,
					cantUnidades : unidades,
					valUnidad : precioUnidad,
					valEntera : precioEntera,
					iva : ivaItem.toFixed(2)
				};

				$.ajax({
					type: "POST",
					url: "editarDatosProforma.php",
					dataType : "json",
					data: json_data,
					success:function(r){
						if(r==1){
							alert("Se insertó el Registro Correctamente");
						}else{
							alert("Error al Insertar el Registro");
						}
					}
				});

				//alert('edicion de registro ServicioDetalle' + precioEntera.toFixed(2) + ' Total Item: $ ' + totalItem.toFixed(2));
				break;
			case 'produccionDetalle':
					ValorMin = $('#txtValMinimoPrd' + cont).val();
					descuento = $('#txtDsctoPrd' + cont).val();
					precioUnidad = $('#txtPUniadPrd' + cont).val();
					if(parseFloat(precioUnidad) < parseFloat(ValorMin)) {
						alert('El Valor ingresado en el item ' + cont + ',\nNo puede ser menor al Mínimo <$ '+ ValorMin +'>');
						$('#txtPUniadPrd' + cont).val(ValorMin);
						precioUnidad = ValorMin;
		
					}else if(parseFloat(descuento) > 5 || parseFloat(descuento) < 0){
						alert('El porcentaje de Descuento en el item ' + cont + ',\nNo puede ser Mayor al 5%');
						$('#txtDsctoPrd' + cont).val(0);
						descuento = 0;
					}
					
					if($('#txtEnterasPrd' + cont).val() < 0){
						$('#txtEnterasPrd' + cont).val(0);
		
					}
		
					if($('#txtUnidadesPrd' + cont).val() < 0){
						$('#txtUnidadesPrd' + cont).val(0);
		
					}
					if($('#txtEnterasPrd' + cont).val() == 0 && $('#txtUnidadesPrd' + cont).val() == 0){
						$('#txtUnidadesPrd' + cont).val(1);
					}
		
					unidadxEntera = $('#txtUnidadxenteraPrd' + cont).val();
					enteras = $('#txtEnterasPrd' + cont).val();
					unidades = $('#txtUnidadesPrd' + cont).val();
		
					precioEntera = parseFloat(precioUnidad) * parseFloat(unidadxEntera);
					$('#txtPEnteraPrd' + cont).val(precioEntera.toFixed(2));
					totalItem = (((parseFloat(enteras) * parseFloat(unidadxEntera) + parseFloat(unidades)) * parseFloat(precioUnidad)) - ((((parseFloat(enteras) * parseFloat(unidadxEntera)) + parseFloat(unidades)) * parseFloat(precioUnidad))* parseFloat(descuento) / 100));
					$('#txtTotPrd' + cont).val(totalItem.toFixed(2));
					ivaItem = parseFloat(totalItem) * 0.12;
					$('#txtIvaPrd' + cont).val(ivaItem.toFixed(2));
		
					calculaTotalesProforma(num);
		
					//alert(subtotal + ' -- ' + ivaTotal + ' -- ' + totProforma);
					var json_data = {
						tipo : tipo,
						numProforma : num,
						totRegistro : totalItem,
						idProducto : $('#txtIdProductoPrd'+ cont).val().replace('@',' '),
						dscto : descuento,
						cantEnteras : enteras,
						cantUnidades : unidades,
						valUnidad : precioUnidad,
						valEntera : precioEntera,
						iva : ivaItem.toFixed(2)
					};
					$.ajax({
						type: "POST",
						url: "editarDatosProforma.php",
						dataType : "json",
						data: json_data,
						success:function(r){
							if(r==1){
								alert("Se insertó el Registro Correctamente");
							}else{
								alert("Error al Insertar el Registro");
							}
						}
					});
				break;
			case 'servicio':
				var json_data = {
					tipo : tipo,
					numProforma: num,
					idProducto : $('#txtIdProducto' + cont).val(),
					precioA : $('#txtPrecioA' + cont).val(),
					precioB : $('#txtPrecioB' + cont).val(),
					Xentera : $('#txtUnidxEntera' + cont).val()
				};

				//VERIFICO SI EL PRODUCTO ESTÁ EN LA PRESENTE PROFORMA O NO
				repetido = 0;
				auxProductos = $('#txtContador').val();
				for(var i=1; i <= auxProductos - 1; i++){	
					if($('#txtidProductoN' + i).val().replace('@',' ') == json_data.idProducto){	
						repetido ++;
					}
				}
				if(repetido>0){
					alert('El producto Seleccionado ya se encuentra en la Proforma..!!');
					return;
				}
				//------------------------------

				$.ajax({
					type: "POST",
					url: "insertarProforma.php",
					dataType : "json",
					data: json_data,
					success:function(r){
						if(r==1){
							alert("Se insertó el Registro Correctamente");
						}else{
							alert("Error al Insertar el Registro");
						}
					}
				});

				proformas(json_data.numProforma);
				break;

			case 'produccion':
					var json_data = {
						tipo : tipo,
						numProforma: num,
						idProducto : $('#txtIdProductoPrd'+ cont).val(),
						precio : $('#txtPrecioPrd' + cont).val(),
						Xentera : $('#txtUnidxEnteraPrd' + cont).val()
					};
					//VERIFICO SI EL PRODUCTO ESTÁ EN LA PRESENTE PROFORMA O NO
					repetido = 0;
					auxProductos = $('#txtContador').val();
					for(var i=1; i<=auxProductos - 1; i++){	
							if($('#txtidProductoN' + i).val().replace('@',' ') == json_data.idProducto){	
							repetido ++;
						}
					}
					if(repetido>0){
						alert('El producto Seleccionado ya se encuentra en la Proforma..!!');
						return;
					}
					//------------------------------
		
					$.ajax({
						type: "POST",
						url: "insertarProforma.php",
						dataType : "json",
						data: json_data,
						success:function(r){
							if(r==1){
								alert("Se insertó el Registro Correctamente");
							}else{
								alert("Error al Insertar el Registro");
							}
						}
					});
		
					proformas(json_data.numProforma);
				break;
		}
	}else{
		alert('No puede Realizar Cambios a la Presente Proforma..!!');
	}
}

function calculaTotalesProforma(num){
	contadorSrv = $('#txtContadorTotSrv').val();
	contadorPrd = $('#txtContadorTotPrd').val();

	ivaTotSrv = 0;
	subTotalSrv = 0;
	ivaTotPrd = 0;
	subTotalPrd = 0;
	subtotal = 0;
	subTotFicha =0;
	ivaFicha =0;

	if(contadorSrv > 1 ){
		for (var i=1; i<contadorSrv; i++) {
			ivaRowSrv = $('#txtIvaSrv' + i).val();
			ivaTotSrv = parseFloat(ivaTotSrv) + parseFloat(ivaRowSrv);
			totRowSrv = $('#txtTotSrv' + i).val();
			subTotalSrv = parseFloat(subTotalSrv) + parseFloat(totRowSrv);
		}
	}

	if(contadorPrd > 1 ){
		for (var j=1; j<contadorPrd; j++) {
			ivaRowPrd = $('#txtIvaPrd' + j).val();
			ivaTotPrd = parseFloat(ivaTotPrd) + parseFloat(ivaRowPrd);
			totRowPrd = $('#txtTotPrd' + j).val();
			subTotalPrd = parseFloat(subTotalPrd) + parseFloat(totRowPrd);
		}
	}

	if ($('#txtExisteFicha').val() != 0){
		subTotFicha = $('#lblSubtotFicha').text();
		ivaFicha = $('#lblIvaFicha').text();
	}


	subtotal = parseFloat(subTotFicha) + parseFloat(subTotalSrv) + parseFloat(subTotalPrd);
	$('#txtSubTotProforma').val(subtotal.toFixed(2));
	//ivaTotal = parseFloat(ivaTotSrv) + parseFloat(ivaFicha) + parseFloat(ivaTotPrd);
	ivaTotal = subtotal * 0.12;
	$('#txtTotIvaProforma').val(ivaTotal.toFixed(2));
	totProforma = parseFloat(subtotal) + parseFloat(ivaTotal);
	$('#txtTotalProforma').val(totProforma.toFixed(2));

	var json_data = {
		tipo : 'totales',
		numProforma : num,
		sTotFicha : subTotFicha,
		IFicha :ivaFicha,
		totPRD : subTotalPrd,
		iPRD : ivaTotPrd,
		totSRV : subTotalSrv,
		iSRV : ivaTotSrv
	};

	$.ajax({
		type: "POST",
		url: "editarDatosProforma.php",
		dataType : "json",
		data: json_data,
		success:function(r){
			if(r==1){
				var editado = true;
			}else{
				alert("Hubo un error al Eliminar el Registro");
			}
		}
	});
}

function eliminaRegistroProforma(cont, tipo){
		valorEliminado = 0;
		if(tipo=='servicio'){
			idProd = $('#txtIdProductoSrv' + cont).val();
			nomProd = $('#txtNomProductoSrv' + cont).val();
			valorEliminado = $('#txtTotSrv' + cont).val();
		}else if(tipo=='produccion'){
			idProd = $('#txtIdProductoPrd' + cont).val();
			nomProd = $('#txtNomProductoPrd' + cont).val();
			valorEliminado = $('#txtTotPrd' + cont).val();
		}
		
	var json_data ={
		tipo : tipo,
		numProforma : $('#txtNumProforma').val(),
		idProducto : idProd.replace('@', ' '),
		nomProducto : nomProd,
		valor : valorEliminado
	};
	var r = confirm('\u00BFEstá seguro de eliminar ' + json_data.nomProducto + ' de esta Proforma?.');
	if(r==true){
		$.ajax({
			type: "POST",
			url: "eliminaItemProforma.php",
			dataType : "json",
			data: json_data,
			success:function(r){
				if(r==1){
					alert("El Registro se eliminó Correctamente");
				}else{
					alert("Hubo un error al Eliminar el Registro");
				}
			}
		});
		proformas(json_data.numProforma);
	}	
	//alert(numProforma + '- - '  + idProducto + '- - '  + cont);
}

function creaProforma(tipo, cont, nuevo){
	if($('#txtNomCliente' + cont).val() == ''){
		alert('El Campo Nombre No debe estar vacio..');
		return;
	}else if($('#txtNomCliente' + cont).val().length > 150 ){
		alert('El Campo Nombre No debe contener mas de 150 caracteres ..');
		return;
	}
	if($('#txtRuc' + cont).val() == ''){
		alert('El Campo Ruc/Ced No debe estar vacio..');
		return;
	}else if($('#txtRuc' + cont).val().length > 13){
		alert('El Campo Ruc/Ced No debe contener mas de 13 caracteres ..');
		return;
	}

	if(nuevo == 1){
		if($('#txtTelefono' + cont).val() == ''){
			alert('El Campo Teléfono No debe estar vacio..');
			return;
		}else if($('#txtTelefono' + cont).val().length > 10){
			alert('El Campo Teléfono No debe contener mas de 10 caracteres ..');
			return;
		}
	}
	if($('#txtDireccion' + cont).val() == ''){
		alert('El Campo Dirección No debe estar vacio..');
		return;
	}else if($('#txtDireccion' + cont).val().length > 300){
		alert('El Campo Dirección No debe contener mas de 300 caracteres ..');
		return;
	}
	var json_data = {
		tipo : tipo,
		nomCliente : $('#txtNomCliente' + cont).val(),
		codCliente : $('#txtCodCliente' + cont).val(),
		contacto : $('#txtcontacto' + cont).val(),
		telefono : $('#txtTelefono' + cont).val(),
		ruc : $('#txtRuc' + cont).val(),
		direccion : $('#txtDireccion' + cont).val(),
		email : $('#txtEmail' + cont).val()
	};
	$.ajax({
		type: "POST",
		url: "insertarProforma.php",
		dataType : "json",
		data: json_data,
		success:function(r){
			if(r==1){
				alert("Se agregó");
			}else{
				alert("Se falla");
			}
		}
	});
	proformas();
}

function listadoFichas(num){
	$("#Block").show();
	destinoAJAX = "agregaFicha";	
	$("#agregaFicha").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
	loadXMLDoc();	
	xmlhttp.open("GET","fichaProforma.php?num=" + num, true);
	xmlhttp.send();
	$("#agregaFicha").show();
}

function insertaFicha(numproforma, numficha){
	var r = confirm('\u00BFSeleccionó la Ficha # ' + numficha + '\nDesea AÑADIRLA a esta Proforma?.');
	if(r==true){
		$("#Block").show();
		destinoAJAX = "agregaFicha";	
		$("#agregaFicha").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
		loadXMLDoc();	
		xmlhttp.open("GET","fichaOperaciones.php?numProf=" + numproforma + "&numFich=" + numficha + "&tipo=I", true);
		xmlhttp.send();
		$("#agregaFicha").show();
		alert('Insertando Ficha # ' + numficha +  ' a la proforma: ' + numproforma);
		proformas(numproforma);
	}
}

function eliminaFicha(numproforma, numficha){
	var r = confirm("\u00BFEstá seguro de eliminar la Ficha de esta Proforma.?.");
	if(r==true){
		loadXMLDoc();	
		xmlhttp.open("GET","fichaOperaciones.php?numProf=" + numproforma + "&numFich=" + numficha + "&tipo=E", true);
		xmlhttp.send();
	}
	proformas(numproforma);
}

//SECCION ARTEMISA

function ingresoGestionCitas(idusuario, desde, hasta){
	destinoAJAX = "contenido";
	if (desde == null || hasta == null){
		 desde = document.getElementById("dtpDesde").value;
		 hasta = document.getElementById("dtpHasta").value;
		 //alert('Desde: ' + desde + ' Hasta: ' + hasta);
		 $("ul.submenu").toggle();
	}
	//$("#contenido").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
	loadXMLDoc();	
	xmlhttp.open("GET","citasArtemisa.php?usuario=" + idusuario + "&desde=" + desde + "&hasta=" + hasta, true);
	xmlhttp.send();
	$("ul.submenu").toggle();
	$("#contenido").show();
}

function CerrarGestiones(usr){
	ingresoGestionCitas(usr);
	//$("ul.submenu").toggle();
}

function tipoGestion(Id,tipo) {
	sleep (100);
	  //FUNCION INGRESO DE GESTION EN CITAS ARTEMISA
	  var body = document.body,
     html = document.documentElement;
     var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
     $("#Block").height(BodyHeight);
	  $("#Block").show();
	  destinoAJAX = "Gestion";
	  $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
	  loadXMLDoc();	  
	  if(tipo=='C'){		   
		   xmlhttp.open("GET","ejecucionCitas.php?numot=" + Id + "&NoCache=" + Math.random(),true);
		   xmlhttp.send();						
	  }
	  $("#Gestion").fadeIn(300,"swing");
}

function gestionCitas(idcita,tipo, usr){
	$("#Block").show();
	destinoAJAX = "ingresoGestionCitas";	
	$("#ingresoGestionCitas").html('<br /><center><img src="images/Cargando.gif"/><br />Cargando...</center>');	
	loadXMLDoc();	
	xmlhttp.open("GET","ejecucionCitas.php?idCita=" + idcita + "&tipo=" + tipo + "&idUsr=" + usr, true);
	xmlhttp.send();
	$("#ingresoGestionCitas").show();
}

//MAS INFORMACION DE la cita
var divs; 
function MasInfo(id) {
	  //FUNCTION PARA VISUALIZAR MAS INFORMACION
	  $(".MasInfo_detalle").slideUp('fast');
	  divs = document.getElementById(id);
	  fila = document.getElementById('tr_' + id);
	  if($(divs).is(':hidden')){ 
		  $(divs).slideDown('fast');
		  $("#Cuenta").text(id);
		  $("#Cuenta").show();		  
	  } else {
		  $(divs).slideUp('fast');
		  $("#Cuenta").hide();
	  }	  
}

var DarBaja = 0;
function MarcarEjecutado(){
	  if($("#Marca").attr("src")=="images/ejecutado_0.png"){
		  $("#Marca").attr("src","images/ejecutado_1.png");
		  $("#MarcaTexto").text("MARCADO COMO EJECUTADO");
		  DarBaja = 1

	  }else{
		  $("#Marca").attr("src","images/ejecutado_0.png");
		  $("#MarcaTexto").text("MARCAR COMO EJECUTADO");
		  DarBaja = 0
	  }	  
  }

function RegistrarGestion(idCita,tipo,idUsuario){
	var txtGestion = document.getElementById("txtGestion").value; 
	  if(txtGestion.trim()==''){
		  alert('Debe ingresar gesti\u00f3n antes de continuar.');
	  }else{
		  //CONTINUAR
		  //VERIFICAR SI DESEA DAR DE BAJA EL MANTENIMIENTO
		  destinoAJAX = "ingresoGestionCitas";
		  //$("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
		  loadXMLDoc();
		  xmlhttp.open("POST","ingresaGestionCitas.php",true);
		  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

		  if(DarBaja==1){
			  //DAR DE BAJA
			  xmlhttp.send("txtidCita=" + encodeURIComponent(idCita) + "&txtGestion=" + encodeURIComponent(txtGestion) + "&tipo=C&opt=1&NoCache=" + Math.random());
			  //ACTUALIZAR LISTA DE CITAS
			  sleep(3);
			  CerrarGestiones(idUsuario);
		  }else{
			  //NO DAR DE BAJA
			  xmlhttp.send("txtidCita=" + encodeURIComponent(idCita) + "&txtGestion=" + encodeURIComponent(txtGestion) + "&tipo=C&NoCache=" + Math.random());
			  //ACTUALIZAR LISTA DE GESTIONES
			  gestionCitas(idCita,tipo,idUsuario);
		  }
	   }
}

function posponerCita(idCita,idUsuario,tipo){
	fecha = document.getElementById("fechaPosponer").value;
	hIni = document.getElementById("horaIni").value;
	hFin = document.getElementById("horaFin").value;
	if (fecha == "" || hIni == "" || hFin == ""){
		alert('NO PUEDEN HABER CAMPOS DE FECHA Y HORA VACIOS.. VERIFIQUE.!');
	}else{
		if (confirm("Eligi\xf3 POSFECHAR la presente cita  hasta el " + fecha + "\n\nEsta seguro de Realizar esta acci\xf3n??.!! " )){
			//CONTINUAR
			//VERIFICAR SI DESEA DAR DE BAJA EL MANTENIMIENTO
			destinoAJAX = "ingresoGestionCitas";
			//$("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
			loadXMLDoc();
			xmlhttp.open("POST","ingresaGestionCitas.php",true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("txtidCita=" + encodeURIComponent(idCita) + "&fecha=" + encodeURIComponent(fecha) + "&horaIni=" + encodeURIComponent(hIni) + "&horaFin=" + encodeURIComponent(hFin) + "&tipo=P&NoCache=" + Math.random());
			CerrarGestiones(idUsuario);
		}else{
			//alert('Operaci\xf3n Cancelada.' + hIni + ' - ' + hFin + ' - ' + fecha);
		}
	}
}

//END SECCION ARTEMISA

function totales(num){
		var valor= num.value;
}

function RecibosCaja(num){
	var valor= num.value;
}

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds){
			break;
		}
	}
}

function pasaValores(clave, usuario,md5,sistema,local){
	$("#Block").show();
    destinoAJAX = "cambioContrasenia";
	//$("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
	loadXMLDoc();
    xmlhttp.open("POST","../../cambioContrasenia.php",true);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	xmlhttp.send("usuario=" + usuario + "&clave=" + clave + "&sistema=" + sistema + "&md5=" + md5 + "&local=" + local + "&tipo=1");
	$("#cambioContrasenia").show();
}

function cambioContrasenia(tipo){
	sistema = $('#txtSistema').val();
	if(sistema =='scaiin' || sistema =='SCAIIN'){
		opcion = 1;
	}else{
		opcion = 2;
	}
	switch (tipo) {
		case 2:
			var json_data = {
				nomUsuario : $('#txtUsuario').val(),
				claveAnterior : $('#txtClave').val(),
				claveNueva : $('#pwdNueva').val(),
				optValidacion : 2,
				tipo : tipo
			};
			if(json_data.claveAnterior != $('#pwdAnterior').val()){
				alert('La Clave Actual ingresada no es la Correcta..!!');
				return;
			}else if($('#pwdNueva').val() != $('#pwdConfirma').val()){
				alert('La confirmación de contraseña no coincide..!!');
				return;
			}else if($('#pwdNueva').val() == $('#pwdAnterior').val()){
				alert('La Nueva contraseña NO debe ser igual a la Anterior..!!');
				return;
			}
			pwd = $('#pwdNueva').val();
			if(validarContrasenia(pwd) == false){
				return;
			}else{
            	$.ajax({
					type: "POST",
					url: "../../actualizaDatosUsr.php",
					dataType : "text",
					data: json_data,
					success: function(r){
						console.log(r);
                        if(r=='Usuario o contraseña no existen.'){
							alert(r);
                        }else{
                            alert(r);
                            $("#mensaje").text("");
                            $("#cambioContrasenia").hide();
                            $("#Block").hide();
                            $("#cajaUsuario").val(json_data.nomUsuario);
                            $("#cajaPwd").focus();
                        }
					}
				});
			}
		break;
	}
}

function validarContrasenia(pwd){
	var sizePwd = pwd.length;
	mayus = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ';
	hayNum = 0;
	
	if(sizePwd < 8){
		alert('La Contraseña debe contener (8) caracteres mínimo.');
		return false;
	}
	for(i=0; i<sizePwd; i++ ){
		if(!isNaN(pwd[i]))
			hayNum++;
	}
	if(hayNum == 0){
		alert('La Contraseña debe contener al menos un número.');
		return false;
	}

	hayMayus=0;
	for(j=0; j<sizePwd; j++){
		for(h=0; h<mayus.length; h++){
			if(pwd[j] == mayus[h]){	
				hayMayus++;
			}
		}
	}
	if(hayMayus==0){
		alert('La Contraseña debe contener al menos una Letra Mayúscula.');
		return false;
	}
	
	hayArroba=0;
	for(x=0; x<sizePwd; x++){
		if(pwd[x] == '@'){	
			hayArroba++;
		}
	}
	if(hayArroba == 0){
		alert('La Contraseña debe contener al menos una Arroba (@)..');
		return false;
	}
}

//FUNCIONES JQUERY
$(document).ready(function(){  
	$("ul.submenu").hide();
    $("a.desplegable").toggle(
      function() {
          $(this).parent().find("ul.submenu").slideDown('fast');
      },
      function() {
    	  $(this).parent().find("ul.submenu").slideUp('fast');
      }
    );    
  });
