<?php 
    require_once __DIR__ . '../../../app/Config.php';
    require_once __DIR__ . '../../../app/Model.php';
    
   require_once('../../libs/mpdf/mpdf.php');
    
    //$usuario = $_SESSION['log_IDUSUARIO'];
    $numero =  $_GET['guia'];
    $local = $_GET['local'];
    
    $archivo = '<html>
        <head>
            <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        </head>
        <body id="contenedor">';

        $guias= $cone->GuiasCobro($numero,$local);
        $empleado = $guias[0][Empleado];
        // ENCABEZADO  --------------------------
        $estadoguia = $guias[0][Estado];
        switch ($local){
            case 1:
                $sucursal = 'MATRIZ';
            break;
            case 2:
                $sucursal = 'QUITO';
                break;
            case 3:
                $sucursal = 'ESMERALDAS';
                break;
            case 4:
                $sucursal = 'CONCORDIA';
                break;
            case 6:
                $sucursal = 'QUEVEDO';
                break;
            case 8:
                $sucursal = 'LAGO AGRIO';
                break;
            case 12:
                $sucursal = 'MANABI';
                break;
            
        }
    $archivo .= '<div class="gradient">
        <div id="encabezado"><table rules="all" border=0.1mm >
        <tr><td></td><img src="../images/logo.png" id="logo"><td><h3>SUCURSAL '. $sucursal .'</h3><br><h3>GUIA DE COBRO # '. $numero .'</h3></br></td></tr>
        <tr><td><b>Fecha: ' . date('Y-m-d',strtotime($guias[0][Fecha])) . ' Hora: ' . date('H:i',strtotime($guias[0][Hora])) . '</td><td><b>Creado Por:</b>  ' . utf8_decode($guias[0][usuario]) . ' </td></tr>
        <tr><td><b>Recaudador:</b> ' . utf8_decode($guias[0][empleadonombre]) .'</td><td><b>Recibo de Caja:</b>  ' . $guias[0][RecibodeCaja] . '</td></tr>
        <tr><td><b>Estado:</b> ' . $estadoguia . '</td><td><b>Creacion R.C.:</b>  ' . $guias[0][Creacion_ReciboCaja] . '</td></tr></tr>
        </table><hr></div></div>';
        // **************************************
        // EFECTIVO *-------------------------------
        $totaldetalle = $cone->totaldetalle($numero,"efectivo",$local);
        $totefectivo = number_format(floatval((double)$totaldetalle[0][total]), 2, '.', '');
        if ($totefectivo != 0){
    $archivo .= '<div id="efectivo">
            <div class="titulo">
                Efectivo	-	 Suman: $' . $totefectivo . '
            </div>
            <div class="info">';
        $efectivo = $cone->detalle($numero,$local);
    $archivo .= '<table rules="all" border=1>
            <tr style="background-color:#D6DBDF;">
            <td><b>Factura</td>
            <td><b>Valor</td>';
            $cont=1;
            foreach ($efectivo as $detfectivo) {
    $archivo .= '<tr>
                <td width="20%">' . $detfectivo[Factura] .'</td>
                <td width="20%">' . $detfectivo[Valorcobro] .'</td>
                <input type="hidden" name="cajaefectivo' . $cont . '" id="cajaefectivo' . $cont . '" value="' . $detfectivo[Caja] . '" ></td>';
                $cont++;
            }
    $archivo .= '</table></div><br></div>';
        }
        // ***************************************
        // CHEQUE ------------------------------
        $totaldetalle = $cone->totaldetalle($numero,"cheque",$local);
        $totcheque = number_format(floatval($totaldetalle[0][total]), 2, '.', '');
        if ((double)$totcheque != 0){
    $archivo .= '
            <div class="titulo">
                Cheques	-	Suman: $' . $totcheque . '
            </div>';
            $hoy = date('d')+1 . '-' . date('m'). '-' . date('Y');
            $hoy = date('Y-m-d',strtotime($hoy));
            $cheque = $cone->cheques($numero,$local);
    $archivo .=  '<div id="cheque"><table rules="all" border=1>
            <tr style="background-color:#D6DBDF;">
            <td><b>Factura</td>
            <td><b>Valor</td>
            <td><b>Cheque</td>
            <td><b>FechaPosf</td></tr>';
            $cont=1;
            foreach ($cheque as $detcheque) {
                $bancos = $cone->bancos();
    $archivo .= '<tr>
                <td width="10%">' . $detcheque[Factura] . '</td>
                <td width="10%">' . $detcheque[Valor] . '</td>
                <td width="10%">' . $detcheque[Cheque] . '</td>';
                if($detcheque[fechaP]!=null){
    $archivo .= '<td width="5%">' . date('Y-m-d', strtotime($detcheque[fechaP])) . '</td>';
                } else {
    $archivo .=  '<td width="5%"></td>';
                }
    $archivo .= '<input type="hidden" name="cajacheque' . $cont . '" id="cajacheque' . $cont . '" value="' . $detcheque[Caja] . '" ></td>
                <input type="hidden" name="cuotacheque' . $cont . '" id="cuotacheque' . $cont . '" value="' . $detcheque[Cuota] . '" ></td>';
                $cont++;
            }
    $archivo .= '</tr></table><br>
            </div>';
        }
        //*******************************************************
        // TRANSFERENCIAS  ------------------------------
        $totaldetalle = $cone->totaldetalle($numero,"transferencia",$local);
        $tottransferencia = number_format($totaldetalle[0][total], 2, '.', '');
        if ((double)$tottransferencia != 0){
    $archivo .= '
            <div class="titulo">
                Transferencias	-	Suman: $' . $tottransferencia .'
            </div>';
        $transf = $cone->transacciones($numero,"TR",$local);
    $archivo .= '<div id="transferencias">
            <table rules="all" border=1>
            <tr style="background-color:#D6DBDF;">
            <td><b>Factura</td>
            <td><b>Valor</td>
            <td><b>Transf.</td>
            <td><b>Fecha</td></tr>';
            $cont=1;
            foreach ($transf as $tr) {
                $bancos = $cone->bancos();
    $archivo .= '<tr>
                <td width="10%">' . $tr[factura] . '</td>
                <td width="10%">' . $tr[valor] . '</td>
                <td width="10%">' . $tr[documento] . '</td>';
                if($tr[fechaTransferencia]!=null){
    $archivo .= '<td width="5%">' . date('Y-m-d', strtotime($tr[fechaTransferencia])) . '</td>';
                }else{
    $archivo .= '<td width="5%"></td>';
                }
    $archivo .= '<input type="hidden" name="cajatr' . $cont . '" id="cajatr' . $cont . '" value="' . $tr[Caja] . '" >';
                $cont++;
            }
    $archivo .= '</tr></table></div><br>';
        }
        //*******************************************************
        // TARJETAS DE CREDITO  ------------------------------
        $totaldetalle = $cone->totaldetalle($numero,"tarjeta",$local);
        $tottarjeta = number_format($totaldetalle[0][total], 2, '.', '');
        $totprueb = (real) $totbillete;
        if ((double)$tottarjeta != 0){
    $archivo .= '
            <div class="titulo">
               Tarjetas	-	Suman: $'. $tottarjeta .'
            </div>
            <div class="info">';
        $tarjeta = $cone->transacciones($numero,"TC",$local);
    $archivo .= '<div id="tarjetas">
            <table rules="all" border=1>
            <tr style="background-color:#D6DBDF;">
            <td><b>Factura</td>
            <td><b>Valor</td>
            <td><b>N.Tarjeta</td></tr>';
            $cont=1;
            foreach ($tarjeta as $tc) {
                $bancos = $cone->bancosTarjetas();
    $archivo .= '<tr>
                <td width="10%">' . $tc[factura] . '</td>
                <td width="10%">' . $tc[valor] . '</td>
                <td width="15%">' . $tc[numTarjeta] . '</td>
                <input type="hidden" name="cajatc' . $cont . '" id="cajatc' . $cont . '" value="' . $tc[Caja] . '" ></td>
                <input type="hidden" name="idtc' . $cont . '" id="idtc' . $cont . '" value="' . $tc[tarjeta] . '" ></td>';
                $cont++;
            }
    $archivo .= '</tr></table></div><br>
            </div></div>';
        }
        //*******************************************************
        // RETENCIONES  ------------------------------
        $totaldetalle = $cone->totaldetalle($numero,"retencion",$local);
        $totret = number_format($totaldetalle[0][total], 2, '.', '');
        if ((double)$totret != 0){
    $archivo .= '
            <div class="titulo">
                Retenciones	-	Suman: $'. $totret .'
            </div>
            <div class="info">';
        $retencion = $cone->retenciones($numero,$local);
    $archivo .= '<div id="retencion">
            <table rules="all" border=1>
            <tr style="background-color:#D6DBDF;">
            <td><b>Factura</td>
            <td><b>Valor</td>
            <td><b># Retencion</td>';
            $cont=1;
            foreach ($retencion as $rt) {
                $rubros = $cone->tipoRubro();
                $impuestos = $cone-> detalleImpuesto();
    $archivo .= '<tr>
                <td width="10%">' . $rt[factura] . '</td>
                <td width="8%">' . number_format($rt[valorRubro], 2, '.', '') . '</td>
                <td width="15%">' . $rt[numDocumento] . '</td>
                <input type="hidden" id="cajart' . $cont . '" value="' . $rt[Caja] . '" ></td>
                <input type="hidden" id="cuotart' . $cont . '" value="' . $rt[cuota] . '" ></td>';
                $cont++;
            }
    $archivo .=  '</tr></table><br></div></div></div>';
            
        }
        //*******************************************************
        //DETALLE BILLETES---------------
        $totaldetalle = $cone->totaldetalle($numero,"billetes",$local);
        $totbillete = number_format($totaldetalle[0][total], 2, '.', '');
        $billetes = $cone->billetes($numero,$local);
        $archivo .= '    <div class="flush">';
        if ((double)$totbillete > 0){

    $archivo .=	 '<div id="billetes">
                  <div class="titulo"> 
                    Billetes	- Suman: $ '. $totbillete .'
                  </div>
    			<table rules="all" border=1>
    				<tr style="background-color:#D6DBDF;">
    					<td><b>Den. Billetes</td>
    					<td><b>Cantidad</td>
    					<td><b>Total</td>
    				</tr>';
                foreach($billetes as $billete){
    $archivo .= '<tr>
					<td>' . $billete[UnidxBillete] .'</td>
                    <td>' . $billete[Cantidad] .'</td>
                    <td>' . ($billete[UnidxBillete] * $billete[Cantidad]) .'</td>
					</tr>';
                }
    $archivo .= '</table><br></div>';
 
        }
        //--------------------------------------------------------
        //DETALLE MONEDAS-------------------------------
        $totaldetalle = $cone->totaldetalle($numero,"monedas",$local);
        $totmonedas = number_format(floatval($totaldetalle[0][total]), 2, '.', '');
        $monedas = $cone->monedas($numero,$local);
        if ((double)$totmonedas != 0){
    $archivo .= '<div id="detallemonedas"> 
            <div class="titulo">
               Monedas	- Suman: $ '. $totmonedas .'
            </div>
            <table rules="all" border=1>
				<tr style="background-color:#D6DBDF;">
					<td><b>Den. Moneda</td>
					<td><b>Cantidad</td>
					<td><b>Total </td>
				</tr>';
                foreach($monedas as $moneda){
    $archivo .= '<tr>
                    <td>'. $moneda[UnidxMoneda] .'</td>
					<td>'. $moneda[Cantidad] .'</td>
					<td> '. ($moneda[UnidxMoneda] * $moneda[Cantidad]) .'</td>
                </tr>';
                }
    $archivo .= '</table><br></div>';
        }
        //--------------------------------------------------------
        // lineas  para presentar el valor en formato de numero con 2 decimales
        $totalcobranza=number_format($totbillete+$totmonedas+$totcheque+$tottransferencia+$tottarjeta+$totret, 2, '.', '');
        $totfacturas = number_format($totefectivo+$totcheque+$tottransferencia+$tottarjeta+$totret, 2, '.', '');
        $diferencia = (double)$totfacturas - (double) $totalcobranza;
        $diferencia = number_format((double) $diferencia, 2, '.', '');
        //**********************************************
        //TOTALES DE LA GUIA ---------
    $archivo .= '</div><br><div class="gradientf">
        <div id="totales"><table>
        <tr><td><b>Total Cobranza: $</td><td>' . $totfacturas . '</td></tr>
        <tr><td><b>Diferencia: $</td><td>' . $diferencia . '</td></tr>
        <tr><td><b>Total Recaudacion: $</td><td><b>' . $totalcobranza . '</td></tr>
        </table></div>
        <div class="codigobarras">

        </div></div>';
        //**********************************
    $archivo .= '</body></html>';
    
    $mpdf = new mPDF('Verdana','A4');
    $css = file_get_contents('../css/estilospdf.css');
    $mpdf->writeHTML($css,1);
    $mpdf->writeHTML(utf8_encode($archivo));
    $mpdf->Output('Guia de Cobro # ' . $numero . '.pdf','I');


?>