<?php
	session_start();
	$usuario=$_SESSION['log_USUARIO'];	
	
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
	$codigo = $_GET['codigo'];
	
	$datoscli = $x->ObtenerDatos('Cuentas_Monitoreo_Adt','*','Where Codigo=\'' . $codigo . '\'','');


?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
<?php 
	echo '<div class="ventana_titulo">INGRESO MANTENIMIENTOS';
	echo '<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones()"><img src="images/cerrar_ventana.png" style="width:100%" /></a></div>';
	echo '</div>';
	if($datoscli != null){
		echo '<div id="G_Contenedor">';
		echo '<table class="VistaDatos" width="100%">';
		echo '<tr><td>C&oacute;digo :</td>';
		echo '<td><input type="text" class="Edicion" name="txtCodCli" id="txtCodCli" value=\'' . $datoscli[0][Codigo] . '\' readonly></td></tr>';
		echo '<tr><td>Nombre :</td>';
		echo '<td><input type="text" class="Edicion" name="txtNomCli" id="txtNomCli" value=\'' . $datoscli[0][Nombre] . '\' readonly></td></tr>';
		$srv = $x-> MantenimientoServicios(0);
		$servicioSeleccionado;
		echo '<tr><td>Servicio :</td>';
		echo'<td><div id="_servicio">';
		echo '<select class="Combo Extendido" name="servicios" id="servicios" onchange="CargarSubServicios()" onload="CargarSubCategoria(' . $servicioSeleccionado .')">';
		foreach($srv as $serv){
			echo '<option value="'. $serv[IdServicio] .'"';
			if(isset($serv[IdServicio]) && ($serv[IdServicio]== -1) ){
				echo (' selected');
				$servicioSeleccionado = $serv[IdServicio];
			}
			echo '>';
			echo $serv[Servicio];
			echo '</option>';
		}
		echo '</select></div></td></tr>';
		echo '<input type="hidden" id="servicioSeleccionado" value =' . $servicioSeleccionado .'>';
		$subservicios = $x-> MantenimientoSubServicios($servicioSeleccionado);
		$subservicioSeleccionado;
		echo '<tr><td>SubServicio :</td>';
		echo'<td><div id="_subservicio">';
		echo '<select class="Combo Extendido" name="subservicios" id="subservicios" onchange="CargarSubCategoria(' . $servicioSeleccionado .')" onClick="CargarSubCategoria(' . $servicioSeleccionado .')">';
		foreach($subservicios as $sub){
			echo '<option value="'. $sub[IdSubServicio] .'"';
			if(isset($sub[IdSubServicio]) && ($sub[IdSubServicio]== -101) ){
				echo (' selected');
				$subservicioSeleccionado = $sub[IdSubServicio];
			}
			echo '>';
			echo $sub[SubServicio];
			echo '</option>';
		}
		echo '</select></div></td></tr>';
		$subcategoria = $x-> MantenimientoSubCategoria($subservicioSeleccionado);
		if($subservicioSeleccionado != 32){
			echo '<tr><td>SubCategoria :</td>';
			echo'<td><div id="_subcategoria">';
			echo '<select class="ComboN Extendido" name="subcategorias" id="subcategorias">';
			foreach($subcategoria as $cat){
				echo '<option value="'. $cat[idSubCategoria] .'">';
				echo $cat[SubCategoria];
				echo '</option>';
			}
			echo '</select></div></td></tr>';
			echo '<input type="hidden" id="subservicioSeleccionado" value =' . $subservicioSeleccionado .'>';
		}
		echo '<tr><td>Requerimiento:</td>';
		echo '<td><input type="text" wrap="soft" class="Edicion" name="txtRequerimiento" id="txtRequerimiento" ></td></tr>';
		echo '<tr><td>Contacto :</td>';
		echo '<td><input type="text" class="Edicion" name="txtContacto" id="txtContacto" ></td></tr>';
		
	//	echo '<tr><td>Prioridad :</td>';
	//	echo'<td><div id="_prioridad">';
	//	echo '<select class="Combo Extendido" name="prioridad" id="prioridad">';
	//		echo '<option value="0">Baja</option>';
	//		echo '<option value="1" selected>Media</option>';
	//		echo '<option value="2">Alta</option>';
	//	echo '</select></div></td></tr>';
		echo '</table></div>';
		
		echo('<br/><center>');
		echo('<a class="Boton_Guardar icon_guardar" onClick="grabaRequerimiento()">&nbsp;Grabar Requerimiento</a>');
		echo('</center>');
	}else{
		echo '<center><h3>NO SE HA ENCONTRADO CLIENTE CON EL DATO INGRESADO !!<h3>';
		echo('<br/>');
		echo('<a class="Boton_Guardar icon_cancelar" onClick="CerrarGestiones()">&nbsp;Regresar</a>');
		echo('</center>');
	}
?>
</body>
</html>