<?php 

	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';

    $local = $_SESSION['log_LOCAL'];
    //VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
    $ver->VerificarSCAIIN('index.php',$local);
    $usuario = $_SESSION['log_IDUSUARIO'];
    //<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
      <!-- Bootstrap core CSS -->    
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/select2.css">
		<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="js/script.js"></script> 
    <script type="text/javascript" src="../js/jquery-3.4.1.js"></script>
		<script type="text/javascript" src="../js/select2.js"></script>
  </head>
  <body>
    <main role="main" class="container">
      <?php
        $num = $cone->UltimoNumero($local,2);
        $numProforma = $_GET['num'];
        if($numProforma == 'undefined' || $numProforma == null){
          $numProforma = $num['0']['Numero'];
        }
        $proformas = $cone->proforma($local,$numProforma);

        $subtotalPS=0;
        $subtotalPR=0;
      echo('
        <br>
        <form>
          <div class="table-responsive" >
          <div id="agregaFicha" class="ventanaFicha" title="Listado de Fichas Técnicas" style="display: none"></div>
          <div id="Block" style="display: none"></div>
            <table class="table table-sm table-striped">
              <div id="navForm">
                <tr>
                  <td colspan="12">
                    <div class="input-group">
                    <button id="btnPrimero" type="button" class="btn btn-sm btn-primary" onclick=nevegadorProf(\'pri\',txtNumProforma.value)><i class="fa fa-fast-backward"></i></button>
                    <button id="btnAnterior" type="button" class="btn btn-sm btn-info " onclick=nevegadorProf(\'ant\',txtNumProforma.value)><i class="fa fa-step-backward"></i></button>
                    <input type="text" class="form-control form-control-lg font-weight-bold text-center text-lg"  id="txtNumProforma" value="' . $numProforma . '" onChange=nevegadorProf(\'\',this.value) />
                    <button id="btnSiguiente" type="button" class="btn btn-sm btn-info" onclick=nevegadorProf(\'sig\',txtNumProforma.value)><i class="fa fa-step-forward"></i></button>
                    <button id="btnUltimo" type="button" class="btn btn-sm btn-primary" onclick=nevegadorProf(\'ult\',' . $num['0']['Numero'] . ') ><i class="fa fa-fast-forward"></i></button>
                    <button id="btnBuscar" type="button" class="btn btn-sm btn-ligth" onclick=duplicaProforma(' . $numProforma . ') ><i class="fa fa-files-o fa-2x" style="color:gray"></i></button>     
                    <button id="btnBuscar" type="button" class="btn btn-sm btn-ligth" onclick=nuevaProforma(\'proforma\') ><i class="fa fa-search fa-2x" style="color:gray"></i></button>
                    <input type="hidden" value="' . $num['0']['Numero'] . '" id="txtUltimo" />
                    <input type="hidden" value="' . $proformas['0']['ID_Usuario_C'] . '" id="txtUsrCreacion" />
                    <input type="hidden" value="' . $usuario . '" id="txtUsrActivo" />
                    </div>
                  </td>
                </tr>
              </div>
              <div  id="oprProforma">
                <tr>
                  <td colspan="12">
                    ');
                      if($proformas['0']['estado'] == 0){
                        echo '<small><p><strong>Origen Creacion:</strong> <strong class="text-primary"><i class="fa fa-home"></i> Oficina</strong>
                        <strong><a onclick=calculaTotalesProforma(' . $numProforma . ') href="Reportes/rptProforma.php?num='. $numProforma .'&server=stodgo&local='. $local .'" target="blank" class="btn btn-sm btn-warning float-right"><i class="fa fa-print"></i> Imprimir</a><strong></span></small>';
                      }else{
                        echo '<small><p><strong>Origen Creacion:</strong> <strong class="text-primary"><i class="fa fa-mobile"></i> Movil</strong>
                        <strong><a onclick=calculaTotalesProforma(' . $numProforma . ') href="Reportes/rptProforma.php?num=' . $numProforma . '&server=stodgo&local='. $local .'" target="blank" class="btn btn-sm btn-warning  float-right"><i class="fa fa-print"></i> Imprimir</a><strong></p></small>';
                      }
                      echo '<center><button id="btnNuevo" type="button" class="btn btn-sm btn-success" onclick=nuevaProforma(\'total\')><i id="nuevoIco" class="fa fa-wpforms"></i> Nueva Proforma</button>';
                    echo('
                  </center></td>
                </tr>
              </div>
              <div id="datosProforma">
                <tr>
                  <td colspan="4"><strong>Fecha:</strong><input type="date" class="form-control form-control-sm" value="'. date('Y-m-d',strtotime($proformas['0']['Fecha']) + 1) .'" onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtFecha"/></td>
                  <td colspan="4"><strong>Cliente: </strong><input type="text" class="form-control form-control-sm" value="'. $proformas['0']['Cliente_act'] .'" id="txtIdCliente" readonly />');
                    if($proformas['0']['ID_Usuario_C'] == $usuario){
                      echo('<button id="btnEditaCli" type="button" class="btn btn-sm btn-danger" onclick=editaClienteProforma(\'cliente\','. $numProforma .')><i id="nuevoIco" class="fa fa-user-o"></i></button>');
                    }
                  echo('</td>
                  <td colspan="4"><strong>OT:  </strong><label class="form-control form-control-sm">'. $proformas['0']['cod_ZonaCobro'] .'<label></td>
                </tr>
                <tr>
                  <td colspan="4"><strong>RUC:</strong><input type="text" class="form-control form-control-sm" value="'. $proformas['0']['RUC'] .'" id="txtRuc" readonly /></td>
                  <td colspan="12"><strong>Razón Social:</strong><input type="text" class="form-control form-control-sm" value="'. $proformas['0']['Nombre'] .'" id="txtNombre" readonly /></td>
                </tr>
                <tr>
                  <td colspan="12"><strong>Dirección:</strong><input type="text" class="form-control form-control-sm" value="'. $proformas['0']['Dirección'] .'" onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtDireccion" /></td>
                </tr>
                <tr>
                  <td colspan="6"><strong>E-mail:</strong><input type="text" class="form-control form-control-sm" value="'. $proformas['0']['Email'] .'" onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtEmail" /></td>
                  <td colspan="6"><strong>Teléfono:</strong><input type="text" class="form-control form-control-sm" value="'. $proformas['0']['Teléfono'] .'" id="txtTelefono" readonly/></td>
                </tr>
                <tr>
                  <td colspan="6"><strong>Vendedor:</strong>');
                    $vendedores = $cone->vendedores($local);
                      echo '<select class="form-control form-control-sm" onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtVendedor">
                          <option>Seleccionar Vendedor</option>';
                      foreach ($vendedores as $vendedor) {
                        echo '<option value="' . $vendedor['cod_vendedor'] . '"';
                        if(isset($proformas['0']['NombreVendedor']) && ($proformas['0']['NombreVendedor'] == $vendedor['nom_vendedor'])){
                          echo ('selected');
                        }
                        echo '>';
                        echo $vendedor['nom_vendedor'];
                        echo '</option>';
                      }
                    
                    echo('</select>
                  </td>
                  <td colspan="6"><strong>Asesor:</strong>');
                    $asesores = $cone->Empleados();
                    echo '<select class="form-control form-control-sm"  onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\')  id="txtAsesor" >
                        <option>Seleccionar Asesor</option>';
                    foreach ($asesores as $asesor) {
                      echo '<option value="' . $asesor['Empleado'] . '"';
                      if(isset($proformas['0']['Empleado_Asesor']) && ($proformas['0']['Empleado_Asesor'] == $asesor['Empleado'])){
                        echo ('selected');
                      }
                      echo '>';
                      echo $asesor['Nombre'];
                      echo '</option>';
                    }
                    echo('</select>
                  </td>
                </tr>
                <tr>
                  <td colspan="4"><strong>Forma Pago:</strong>');
                      $formaPago = $cone->formaPagoFacturas();
                      echo '<select class="form-control form-control-sm" onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtFormaPago">';
                      foreach ($formaPago as $fPago) {
                        echo '<option value="' . $fPago['FormaPago'] . '"';
                        if(isset($proformas['0']['Forma_Pago']) && ($proformas['0']['Forma_Pago'] == $fPago['FormaPago'])){
                          echo ('selected');
                        }
                        echo '>';
                        echo $fPago['Descripción'];
                        echo '</option>';
                      }
                      echo '<input type="hidden" value="' . $proformas['0']['Forma_Pago'] . '" id="fpInicial">';
                      echo('</select>
                  </td>
                  <td colspan="2"><strong>Días:</strong><input input-sm type="text" class="form-control form-control-sm" value="'. $proformas['0']['Plazo_Días'] .'" onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtDiasFP" /></td>
                  <td colspan="6"><strong>CanalCom.:</strong>');
                    $canalComercial = $cone->canalComercial($local);
                    echo '<select class="form-control form-control-sm"  onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtCanalC">';
                        foreach ($canalComercial as $ccom) {
                          echo '<option value="' . $ccom['Tipo_Cliente'] . '"';
                          if(isset($proformas['0']['CanalCom']) && ($proformas['0']['CanalCom'] == $ccom['Tipo_Cliente'])){
                            echo ('selected');
                          }
                          echo '>';
                          echo $ccom['Descripcion'];
                          echo '</option>';
                        }
                        echo('</select>
                  </td>
                </tr>
                <tr>
                  <td colspan="12"><strong>Observaciones:</strong><textarea class="form-control form-control-sm " rows="2" onChange=editaCambiosProforma(txtNumProforma.value,\'cabecera\') id="txtObservacion">'. $proformas['0']['Observación'] .'</textarea></td>
                </tr> 
              </div>
            </table>
          </div>
          <div>
            <input type="hidden" value="' .  $proformas['0']['Ficha'] . '" id="txtExisteFicha" />');
            $fichaDet = $cone->proformaDetalle('ficha', $local, $numProforma);
            if(count($fichaDet) > 0){
              echo ('
                <div class="table-responsive">
                  <table class="table table-sm table-bordered" border="1">
                    <thead class="thead-light">
                    <tr>
                      <th colspan="4"><strong class="text-primary">Ficha # ' . $proformas['0']['Ficha'] . '</strong></th>
                      <th colspan="8">
                        <center>');
                          if($proformas['0']['ID_Usuario_C'] == $usuario){
                            echo '<button type="button" class="btn btn-sm btn-danger" onClick=eliminaFicha('. $numProforma .',' . $proformas['0']['Ficha'] . ')>Eliminar Ficha</button>';
                          }
                        echo('</center>
                      </th>
                    </tr>
                    </thead>
                    <tbody class="table-warning">
                    <tr>
                      <td colspan="4">V.Ficha:<strong> $<label id="lblSubtotFicha" >'. $proformas['0']['TotalBrutoFicha'] .'</label></strong></td>
                      <td colspan="4">Iva Ficha:<strong> $<label id="lblIvaFicha" > '. $proformas['0']['TotalIvaFicha'] .'</label></strong></td>
                      <td colspan="4">Total Ficha:<strong> $<label id="lblTotalFicha">' . $proformas['0']['TotalFicha'] . '</label></strong></td>
                    </tr>
                    </tbody>
                  </table>
                </div>');
            }else{
              echo ('
              <div class="table-responsive">
                <table class="table table-sm table-bordered">
                  <thead class="thead-light">
                    <tr>
                      <th colspan="12">');
                        if($proformas['0']['ID_Usuario_C'] == $usuario){
                          echo '<center><button type="button" class="btn btn-primary" onClick=listadoFichas('. $numProforma .')>Agregar Ficha</button></center>';
                        }
                      echo('</th>
                    </tr>
                  </head>
                </table>
              </div>
              ');
            }
           echo('
          </div>
          <div class="table-responsive">
            <div id="cheque">
              <table class="table table-sm table-bordered table-hover">
                <thead class="thead-light">
                  <tr>
                    <th colspan="9"><strong>PRODUCTOS/SERVICIOS</strong></th>
                  </tr>
                  <tr class="active">
                    <th class="bg-dark text-white" style="font-size:16; width:1%"></td>
                    <th class="bg-dark text-white" style="font-size:16; width:20%">Nom. Producto</td>
                    <th class="bg-dark text-white" style="font-size:16; width:4%">Ent.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:4%">Und.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">P Unit.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">P Ent.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">Dcto</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">Iva</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">Tot.</td>
                  </tr>
                </thead>
                <tbody>
                  ');
                  $detalle = $cone->proformaDetalle('prodServ',$local, $numProforma);
                  $contSrv = 1;
                  foreach ($detalle as $detalles) {
                    $iva =  ($detalles['precio_real']); 
                    $precioUnidad =  ($detalles['Precio_Unidad']); 
                    $precioEntera = ($detalles['Precio_Unidad'] * $detalles['Unidadxentera']);
                    $totDetalle = ((($detalles['Enteras'] * $detalles['Unidadxentera']) + $detalles['Unidades']) * $detalles['Precio_Unidad']) - (((($detalles['Enteras'] * $detalles['Unidadxentera']) + $detalles['Unidades']) * $detalles['Precio_Unidad'])* ($detalles['Dscto'] / 100));
                    if($detalles['ficha'] == false){
                      $arraySRV .= str_replace(' ','@', $detalles['Producto']) . ',';
                      echo '<tr>
                        <td width="1%">';
                          if($proformas['0']['ID_Usuario_C'] == $usuario){
                            echo '<button type="button" class="btn btn-sm btn-danger" onclick=eliminaRegistroProforma('. $contSrv .',\'servicio\')> <i class="fa fa-minus"></i></button>';
                          }else{
                            echo '<button type="button" class="btn btn-sm btn-danger" disabled> <i class="fa fa-minus"></i></button>';
                          }
                        echo '</td>
                        <td width="20%"><input type="text" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-left input-sm border-0" type="text" value="' . $detalles['Nombre_Producto'] . '"  id="txtNomProductoSrv' . $contSrv . '" readonly/></td>
                        <td width="4%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' . $detalles['Enteras'] . '" id="txtEnterasSrv' . $contSrv . '" onChange=editaCambiosProforma(txtNumProforma.value,\'servicioDetalle\','. $contSrv .') /></td>
                        <td width="4%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' . $detalles['Unidades'] . '" id="txtUnidadesSrv' . $contSrv . '" onChange=editaCambiosProforma(txtNumProforma.value,\'servicioDetalle\','. $contSrv .') /></td>
                        <td width="7%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' . $precioUnidad . '" id="txtPUniadSrv' . $contSrv . '" onChange=editaCambiosProforma(txtNumProforma.value,\'servicioDetalle\','. $contSrv .') /></td>
                        <td width="7%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' . $precioEntera . '" id="txtPEnteraSrv' . $contSrv . '" readonly /></td>
                        <td width="7%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' . $detalles['Dscto'] . '" id="txtDscto' . $contSrv . '" onChange=editaCambiosProforma(txtNumProforma.value,\'servicioDetalle\','. $contSrv .') /></td>
                        <td width="7%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' .  number_format($detalles['precio_real'],2,'.',''). '" id="txtIvaSrv' . $contSrv . '" readonly /></td>
                        <td width="7%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' . number_format($totDetalle,2,'.','') . '" id="txtTotSrv' . $contSrv . '" readonly /></td>
                        <input type="hidden" value="' . str_replace(' ','@', $detalles['Producto']) . '" id="txtIdProductoSrv' . $contSrv . '" />
                        <input type="hidden" value="' . $detalles['Unidadxentera'] . '" id="txtUnidadxentera' . $contSrv . '" />
                        <input type="hidden" value="' .  $detalles['Precio_Minimo'] . '" id="txtValMinimo' . $contSrv . '" />
                        <input type="hidden" value="' .  $detalles['PrecioB'] . '" id="txtPrecioB' . $contSrv . '" />
                        <input type="hidden" value="' .  $detalles['Categoria'] . '" id="txtCategoria' . $contSrv . '" />
                      </tr>';
                      $contSrv ++;
                    }
                    $subtotalPS = ($subtotalPS + $totDetalle);
                  }
                  echo('
                  <input type="hidden" value="' .  $contSrv . '" id="txtContadorTotSrv" />
                </tbody>
              </table>
              <div style="margin-left: 0.5em; margin-top: -0.5em">');
                  if($proformas['0']['ID_Usuario_C'] == $usuario){
                    $arraySRV= substr($arraySRV, 0, -1);
                    $strSRV = explode(",",$arraySRV);
                    echo '<button type="button" class="btn btn-success" onclick=nuevoRegistroProforma('. $numProforma .',\'servicio\',' . json_encode($strSRV) . ')> <i class="fa fa-plus"> </i></button>
                    <span class="text-success span-sm">Añadir Registro</span>';
                  }
              echo('</div>
              <br>
            </div>
          </div>
          <div class="table-responsive">
            <div id="cheque">
              <table id="tablaProduccion"class="table table-sm table-bordered table-hover">
                <thead class="thead-light">
                  <tr>
                    <th colspan="9"><strong>PRODUCCIÓN</strong></th>
                  </tr>
                  <tr class="active">
                    <th class="bg-dark text-white" style="font-size:16; width:1%"></td>
                    <th class="bg-dark text-white" style="font-size:16; width:20%">Nom. Producto</td>
                    <th class="bg-dark text-white" style="font-size:16; width:4%">Ent.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:4%">Und.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">P Unit.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">P Ent.</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">Dcto</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">Iva</td>
                    <th class="bg-dark text-white" style="font-size:16; width:7%">Tot.</td>
                  </tr>
                </thead>
                <tbody>
                  ');
                  $detalle = $cone->proformaDetalle('produccion',$local, $numProforma);
                  $contPrd = 1;
                  foreach ($detalle as $detalles) {
                    $iva =  ($detalles['precio_real']); 
                    $precioUnidad =  ($detalles['Precio_Unidad']); 
                    $precioEntera = ($detalles['Precio_Unidad'] * $detalles['Unidadxentera']);
                    $totDetalle = ((($detalles['Enteras'] * $detalles['Unidadxentera']) + $detalles['Unidades']) * $detalles['Precio_Unidad']) - (((($detalles['Enteras'] * $detalles['Unidadxentera']) + $detalles['Unidades']) * $detalles['Precio_Unidad']) * ($detalles['Dscto'] / 100));
                    if($detalles['ficha'] == false){
                      $arrayPRD .= str_replace(' ','@', $detalles['Producto']) . ',';
                      echo '<tr>
                        <td width="1%">';
                        if($proformas['0']['ID_Usuario_C'] == $usuario){
                          echo '<button type="button" class="btn btn-sm btn-danger" onclick=eliminaRegistroProforma('. $contPrd .',\'produccion\')><i class="fa fa-minus"> </i></button>';
                        }else{
                          echo '<button type="button" class="btn btn-sm btn-danger" disabled><i class="fa fa-minus"> </i></button>';
                        }
                        echo '</td>
                        <td width="20%"><input style="background-color:transparent; font-size:12" class="form-control form-control-sm text-left  border-0" type="text" value="' . $detalles['Nombre_Producto'] . '" id="txtNomProductoPrd' . $contPrd . '" readonly /></td>
                        <td width="4%"><input style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right  border-0" type="text" value="' . $detalles['Enteras'] . '" id="txtEnterasPrd' . $contPrd . '" onChange=editaCambiosProforma(txtNumProforma.value,\'produccionDetalle\','. $contPrd .') /></td>
                        <td width="4%"><input style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right  border-0" type="text" value="' . $detalles['Unidades'] . '" id="txtUnidadesPrd' . $contPrd . '" onChange=editaCambiosProforma(txtNumProforma.value,\'produccionDetalle\','. $contPrd .') /></td>
                        <td width="7%"><input style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right  border-0" type="text" value="' . $precioUnidad . '" id="txtPUniadPrd' . $contPrd . '" readonly /></td>
                        <td width="7%"><input style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right  border-0" type="text" value="' . $precioEntera . '" id="txtPEnteraPrd' . $contPrd . '" readonly /></td>
                        <td width="7%"><input type="number" style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right border-0" value="' . $detalles['Dscto'] . '" id="txtDsctoPrd' . $contPrd . '" readonly /></td>
                        <td width="7%"><input style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right  border-0" type="text" value="' . number_format($iva,2,'.','') . '" id="txtIvaPrd' . $contPrd . '" readonly /></td>
                        <td width="7%"><input style="background-color:transparent; font-size:12" class="form-control form-control-sm text-right  border-0" type="text" value="' . number_format($totDetalle,2,'.','') . '" id="txtTotPrd' . $contPrd . '" readonly /></td>
                        <input type="hidden" value="' . str_replace(' ','@',$detalles['Producto']) . '" id="txtIdProductoPrd' . $contPrd . '" />
                        <input type="hidden" value="' . $detalles['Unidadxentera'] . '" id="txtUnidadxenteraPrd' . $contPrd . '" />
                        <input type="hidden" value="' .  $detalles['Precio_Minimo'] . '" id="txtValMinimoPrd' . $contPrd . '" />
                      </tr>';
                      $contPrd++;
                    }
                    $subtotalPR = ($subtotalPR + $totDetalle);
                  }
                  $subtotal = ($subtotalPS + $subtotalPR);
                  $ivaTotal = ($subtotal * 0.12);
                  $totalCompleto = ($subtotal + $ivaTotal);
                  echo(' 
                  <input type="hidden" value="' .  $contPrd . '" id="txtContadorTotPrd" />
                </tbody>
              </table>
              <div style="margin-left: 0.5em; margin-top: -0.5em">');
                if($proformas['0']['ID_Usuario_C'] == $usuario){
                  $arrayPRD= substr($arrayPRD, 0, -1);
                  $strPRD = explode(",",$arrayPRD);
                  echo '<button id="btnInServicio" onclick=nuevoRegistroProforma('. $numProforma .',\'produccion\',' . json_encode($strPRD) . ') type="button" class="btn btn-success"> <i class="fa fa-plus"> </i></button>
                      <span class="text-success span-sm">Añadir Registro</span>';
                }
              echo('</div>
              <br>
            </div>
          </div>
          <div class="table-responsive-sm">
            <table class="table table-sm table-dark">
              <tr>
                <td colspan="4"><strong>Total Prod:</strong><input  class="form-control form-control-sm bg-primary text-white" value="'. number_format($subtotal,2,'.','') .'" id="txtSubTotProforma" readonly /></td>
                <td colspan="4"><strong>Iva:</strong><input class="form-control form-control-sm bg-secondary text-white" value="'. number_format($ivaTotal,2,'.','') .'"  id="txtTotIvaProforma" readonly /></td>
                <td colspan="4"><strong>Total Prof.:</strong><input class="form-control form-control-sm bg-danger text-white" value="' . number_format($totalCompleto,2,'.','') . '" id="txtTotalProforma" readonly /></td>
              </tr>
            </table>
          </div>
        </form>
      ');
      ?>
    </main><!-- /.container --> 
  </body>
</html>