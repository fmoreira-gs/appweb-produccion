<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	if(!isset($_SESSION['log_USUARIO'])){
	    header('location:/final/index.php');
	}

	$tipo=$_GET['tipo'];

?>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title><?php echo(Config::$mvc_titulo); ?></title>
		<!--  ESTILOS  -->
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/select2.css">
		<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<script type="text/javascript" src="js/script.js"></script> 
		<script type="text/javascript" src="../js/jquery-3.4.1.js"></script>
		<script type="text/javascript" src="../js/select2.js"></script>
	</head>
	<body>
		<center><b><p class="mensaje">BUSQUEDA DE PRODUCTOS</p></b></center>
		<input type="radio" name="opcion" id="R_codigo" value="CODIGO" style="width: 5%; height:5%;" > C&Oacute;DIGO&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="radio" name="opcion" id="R_nombre" value="NOMBRE" style="width: 5%; height:5%;" checked > NOMBRE
		<br>
		<input class="textbuscar" type="text" name="criterio" id="criterio" >
		<input class="botonBuscar" type="button" value="Buscar" id="buscar" onClick=BusquedaProductos(<?php echo('\'' . $tipo .  '\''); ?>) ><br>
		<div id = "buscaparametro"> 
		</div>
	</body>
</html>