<?php 
	class Config
	{
		//LISTA DE SERVIDORES PARA CONEXION WEB (MSSQL SERVER 2012)
		static public $mvc_server = array(
			'SANTODOMINGO'	=>	'MSSQL_SERVERSD',
			'QUITO'			=>	'MSSQL_SERVIDORQUITO',
			'QUEVEDO'		=>	'MSSQL_SERVERQUEVEDO',
			'CONCORDIA'		=>	'MSSQL_SERVERCON',
			'ESMERALDAS'	=>	'MSSQL_SERVERESM',
			'LAGOAGRIO'		=>	'MSSQL_SERVERLAGO',
			'MANTA'			=>	'MSSQL_SERVERMANTA',
			'PRUEBAS'		=>	'MSSQL_PRUEBAS',
			'FABIAN'		=>	'MSSQL_FABIAN'
		);
	
		//LISTA DE BASE DE DATOS DISPONIBLES
		static public $mvc_database = array(
			'SCAIINEXPRESS'		=>	'ScaiinExpress',
			'SCAIIN_PRUEBAS'	=>	'ScaiinExpress_Pruebas',
			'SCAIINQUEVEDO'		=>	'ScaiinExpressQuevedo',
			'SCANNERCOP'		=>	'ScannerCop',
			'ARTEMISA'			=>	'Artemisa'
		);
	
		//CREDENCIALES DE CONEXION MEDIANTE USUARIO
		/// SUPERADMINISTRADOR (SA)
		static public $mvc_user = 'sa';
		static public $mvc_pass = 'Systemadmin696';


		//CREDENCIALES DE CONEXION MEDIANTE USUARIO AA SERVIDOR POSTGRESQL
		static public $nameServer = '192.168.3.115';
		static public $bddName = 'gserp';
		static public $user = 'sistemas';
		static public $pass = 'Systemadmin696';
	
		//TABLAS Y VISTAS GENERALES
		static public $mvc_v_usuarios = 'V_USUARIOS';
	
		//TITULOS DE PAGINAS
		static public $mvc_titulo = 'Sistema Web';
	
		//SISTEMAS
		static public $mvc_scain = ' SCAIIN';
		static public $mvc_siac = ' SIAC';
	
		//ESTILOS
		static public $mvc_estilo = 'estilo.css';
	
		//VARIABLES
		static public $MaxItem = 190; //DEFINE EL NUMERO TOTAL DE ITEMS (ZONAS, PERSONAL AUTORIZADO/EMERGENTE, ENTRE OTROS)
		static public $MaxHistorial = 20; //DEFINE CUANTOS REGISTROS DEL HISTORIAL MOSTRAR
		static public $MaxClientes = 5;  //DEFINE CUANTOS CLIENTES MOSTRAR POR CONSULTA
		static public $MaxProductos = 10; // DEFINE MAXIMO 10 PRODUCTOS EN LISTA
		static public $RutaLOGS = '/var/www/html/final/app/logs/';
	
		static public $Empresa = 1;
        //static public $Local = 1;
		static public $Caja = 1;
	}
?>
