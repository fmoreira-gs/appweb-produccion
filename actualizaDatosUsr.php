<?php
    include('lib/nusoap.php'); 

    $nomUsuario = $_POST['nomUsuario'];
    $claveAnterior = $_POST['claveAnterior'];
    $claveNueva = $_POST['claveNueva'];
    $optValidacion = $_POST['optValidacion'];

    $dir = 'http://192.168.3.100:85/webApi.asmx?WSDL';

    $client = new nusoap_client($dir,true);
    
    $params = array('nomUsuario' => utf8_decode($nomUsuario), 'claveAnterior' => utf8_decode($claveAnterior),'claveNueva' => utf8_decode($claveNueva), 'optValidacion' => $optValidacion);

    $result = $client->call('cambiarPass',$params);

    echo utf8_encode($result['cambiarPassResult']);

?>
