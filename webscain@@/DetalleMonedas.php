<?php 
	
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	$local = $_SESSION['log_LOCAL'];

	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->VerificarSCAIIN('index.php',$local);

	$empresa = Config::$Empresa;
	$caja = Config::$Caja;
    $guia = $_GET['guia'];
    $tipo= $_GET['tipo'];
    
    
    $monedas = $cone->monedas($guia,$local);
    $totaldetalle = $cone->totaldetalle($guia,$tipo,$local);
	$totaldetalle =$totaldetalle[0][total];
?>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
		<!--  ESTILOS  -->
		<link rel="stylesheet" type="text/css" href="css/estilos.css" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body><div class='menu_acordeon'>
		<div id="detallemonedas">
		<?php 
		  $guias = $cone->GuiasCobro($guia,$local);
			echo "<div class='titulo'>";
			echo "<p class='tutulo'>Detalle Monedas	-	<label class='totalmenu'>Suman: $ $totaldetalle</label> </p>";
			echo "</div>";
			echo "<div class='info' id='info'>";
		?>
		<table rules="all" border="1">
			<tr style="background-color:#00a0d0;">
				<td>Den. Moneda</td>
				<td>Cantidad</td>
				<td> Total </td>
			</tr>
			<tr>
				<td><input type="text" class="textefectivo"  id="unocent" value=0.01 readonly></td>
				<td><input type="number" class="textefectivo"  value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.01){echo ($moneda[Cantidad]); $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="unocentval" onchange=calcular(this,'<?php echo($guia); ?>',"monedas"); <?php if($val==1){echo 'readonly';} ?>></td>
				<td><input type="text" class="textefectivo"  value="<?php if($val!=0){echo$total;}?>" id="totalunocent" readonly></td>
				<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'monedas','','',0.01)></a></td></tr>";	}}?>
			</tr>
			<tr>
				<td><input type="text" class="textefectivo" id="cincocent" value=0.05 readonly></td>
				<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.05){echo ($moneda[Cantidad]); $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="cincocentval" onchange=calcular(this,'<?php echo($guia); ?>',"monedas"); <?php if($val==1){echo 'readonly';} ?>></td>
				<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcincocent" readonly></td>
				<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'monedas','','',0.05)></a></td></tr>";	}}?>
			</tr>
			<tr>
				<td><input type="text" class="textefectivo" id="diezcent" value=0.10 readonly></td>
				<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.10){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="diezcentval" onchange=calcular(this,'<?php echo($guia); ?>',"monedas"); <?php if($val==1){echo 'readonly';} ?>></td>
				<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totaldiezcent" readonly></td>
				<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'monedas','','',0.10)></a></td></tr>";	}}?>
			</tr>
			<tr>
				<td><input type="text" class="textefectivo" id="veinticincocent" value=0.25 readonly></td>
				<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.25){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="veinticincocentval" onchange=calcular(this,'<?php echo($guia); ?>',"monedas"); <?php if($val==1){echo 'readonly';} ?>></td>
				<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalveinticincocent" readonly></td>
				<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'monedas','','',0.25)></a></td></tr>";	}}?>
			</tr>
			<tr>
				<td><input type="text" class="textefectivo" id="cincuentacent" value=0.50 readonly></td>
				<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.50){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="cincuentacentval" onchange=calcular(this,'<?php echo($guia); ?>',"monedas"); <?php if($val==1){echo 'readonly';} ?>></td>
				<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcincuentacent" readonly></td>
				<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'monedas','','',0.50)></a></td></tr>";	}}?>
			</tr>
			<tr>
				<td><input type="text" class="textefectivo" id="ciencent" value=1.00 readonly></td>
				<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==1.00){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="ciencentval" onchange=calcular(this,'<?php echo($guia); ?>',"monedas"); <?php if($val==1){echo 'readonly';} ?>></td>
				<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalciencent" readonly></td>
				<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){ echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$guia,'monedas','','',1)></a></td></tr>";}}?>
			</tr>
		</table>
		<?php echo '</div></div>'; ?>
		<!--  <center><br><a  href="#" onclick="GuiaCobro()" class="vinculo icon_cancelar" style="font-family: italic;" > Atras</a></center>  -->
	</div></body>
</html>
