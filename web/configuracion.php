<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');
	
	//SETEAR CARACTERES A UTF-8
	//ini_set('mssql.charset', 'UTF-8');
	
	//CONFIGURACION
	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);	
?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?php echo(Config::$mvc_titulo); ?></title>
<!--  ESTILOS  -->
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
</head>
<body>
<?php	
	echo('<div id="Titulo">CONFIGURACION</div>'); 	
?>
</body>
</html>