<?php
    class Model
    {
        
        # Region COMUN
        
        //FUNCION PUBLICA (CONSTRUCTOR) PARA REALIZAR LA CONEXION AL SERVIDOR
        public function __construct($dbhost, $dbname, $dbuser, $dbpass){
            session_start();
            $usuario = $_SESSION['log_USUARIO'];
            $usuario = str_replace("�","�",$usuario);
            $mvc_conexionBDD = mssql_connect($dbhost,$dbuser,$dbpass, true);
            if(!$mvc_conexionBDD){
                $this->GuardaLOGS("GENERAL","ERROR","#M01. Ocurrio un error al intentar enlazar con el servidor - Usuario[" . $usuario . "] - HOSTNAME[" . $dbhost . "] USER[" . $dbuser . "] PASS[***************].");
                die('<b>Error #M01:</b> Ocurri&oacute; un error al intentar enlazar con el servidor.');
            }
            //CONECTAR A LA BASE DE DATOS SELECCIONADA
            $conectar = mssql_select_db($dbname, $mvc_conexionBDD);
            if(!$conectar){
                $this->GuardaLOGS("GENERAL","ERROR","#M02. Ocurrio un error al intentar abrir la base de datos - Usuario[" . $usuario . "] - DATABASE[" . $dbname . "].");
                die('<b>Error #M02:</b> Ocurri&oacute; un error al intentar abrir la base de datos.</b>');
            }
            //VARIABLE QUE ALMACENA LA CONEXION PARA FUTURAS INSTRUCCIONES/SQL
            $this->conexion = $mvc_conexionBDD;
        }
        
        //FUNCION PRIVADA GENERICA PARA OBTENER UN ARRAY DE DATOS
        //PARAMETROS INGRESADO:  TABLA, CAMPOS, CONDICION, ORDEN
        //PARAMETRO DEVUELTO: ARRAY DE DATOS
        public function ObtenerDatos($tabla, $campos, $condicion, $orden){
            session_start();
            $usuario = $_SESSION['log_USUARIO'];
            $usuario = str_replace("�","�",$usuario);
            $SQL = 'select ' . $campos . ' from ' . $tabla . ' ' . $condicion . ' ' . $orden;
            //echo utf8_encode($SQL);
            $result = mssql_query($SQL,$this->conexion);
            if(!$result){
                $this->GuardaLOGS($usuario,"ERROR","#M03. Error al obtener los datos solicitados SQL[" . $SQL . "].");
                ('<b>Error #M03:</b> Error al obtener los datos solicitados. ' . $SQL . '</b>');
            }
            $this->GuardaLOGS($usuario,"CONSULTA","Se registro una consulta en la base de datos SQL[" . $SQL . "].");
            $datos = array();
            while($row = mssql_fetch_assoc($result)){
                $datos[] = $row;
            }
            return $datos;
        }
        
        //FUNCION PUBLICA GENERICA PARA OBTENER UN ARRAY DE DATOS
        //PARAMETROS INGRESADOS: PROCEDIMIENTO, PARAMETROS
        //PARAMETRO DEVUELTO: ARRAY DE DATOS
        public function ObtenerDatosSP($procedimiento, $parametros){
            session_start();
            $usuario = $_SESSION['log_USUARIO'];
            $usuario = str_replace("�","�",$usuario);
            $SQL = 'exec ' . $procedimiento . ' ' . $parametros;
            $result = mssql_query($SQL,$this->conexion);
            if(!$result){
                $this->GuardaLOGS($usuario,"ERROR","#M04. Error al obtener los datos solicitados SQL[" . $SQL . "].");
                die('<b>Error #M04:</b> Error al obtener los datos solicitados. ' . $SQL . '</b>');
            }
            $this->GuardaLOGS($usuario,"CONSULTA","Se registro una consulta en la base de datos SQL[" . $SQL . "].");
            $datos = array();
            while($row = mssql_fetch_assoc($result)){
                $datos[] = $row;
            }
            return $datos;
        }
        //FUNCION PRIVADA GENERICA PARA INGRESAR DATOS EN TABLA
        //PARAMETROS INGRESADOS: TABLA, CAMPOS, VALORES
        //PARAMETRO DEVUELTO: NINGUNO
        private function IngresarDatos($tabla, $campos, $valores){
            $SQL = 'insert into ' . $tabla . ' (' . $campos . ') values (' . $valores . ')';
            //echo '<h1>' . utf8_encode($SQL) . '<h1>';
            $result = mssql_query($SQL,$this->conexion);
            session_start();
            $usuario = $_SESSION['log_USUARIO'];
            $usuario = str_replace("�","�",$usuario);
            if(!$result){
                $this->GuardaLOGS($usuario,"ERROR","#M05. Error al ingresar datos SQL[" . $SQL . "].");
                die('<b>Error #M05:</b> Error al ingresar datos.</b>');
            }
            $this->GuardaLOGS($usuario,"INGRESO","Se registro una entrada a la base de datos SQL[" . $SQL . "].");
        }
        //FUNCION PRIVADA GENERICA PARA ACTUALIZAR DATOS EN TABLA
        //PARAMETROS INGRESADOS: TABLA, CAMPOS=VALOR, CONDICION
        //PARAMETRO DEVUELTO: NINGUNO
        private function ActualizarDatos($tabla, $campos, $condicion){
            $SQL = 'update ' . $tabla . ' set ' . $campos . ' ' . $condicion;
            $SQL = str_replace("�","�",$SQL);
            //echo utf8_encode($SQL);
            $result = mssql_query($SQL,$this->conexion);
            session_start();
            $usuario = $_SESSION['log_USUARIO'];
            $usuario = str_replace("�","�",$usuario);
            if(!$result){
                $this->GuardaLOGS($usuario,"ERROR","#M06. Error al actualizar datos SQL[" . $SQL . "].");
                die('<b>Error #M06:</b> Error al actualizar datos.</b>');
            }
            $this->GuardaLOGS($usuario,"EDICION","Se registro una actualizacion en la base de datos SQL[" . $SQL . "].");
        }
        
        private function EliminarDatos($tabla, $condicion){
            $SQL = 'DELETE FROM ' . $tabla . ' WHERE ' . $condicion;
            //$result = mssql_query(utf8_encode($SQL),$this->conexion);
            $result = mssql_query($SQL,$this->conexion);
            session_start();
            $usuario = $_SESSION['log_USUARIO'];
            $usuario = str_replace("�","�",$usuario);
            if(!$result){
                $this->GuardaLOGS($usuario,"ERROR","#M06. Error al actualizar datos SQL[" . $SQL . "].");
                die('<b>Error #M06:</b> Error al ELIMINAR datos.</b>');
            }
            $this->GuardaLOGS($usuario,"ELIMINACION","Se registro una ELIMINACION en la base de datos SQL[" . $SQL . "].");
        }
        
        //FUNCION OBTENER FECHA
        private function Fecha(){
            return date('d-m-Y');
        }
        private function Hora(){
            return date('H:i:s');
        }

        //FUNCION PARA DETERMINAR EL NIVEL DE PERMISOS QUE TIENE EL USUARIO
        private function rolUsuario($idUsuario){
            $Insert = $this->ObtenerDatos('Usuarios','Permiso','Where idUsuario ='. $idUsuario .'','');
            $result = $Insert[0]['Permiso'];
            switch ($result) {
                case 'CONSULTA':
                    $resp = array($result,0);
                    return $resp;
                    break;
                case 'TECNICO':
                    $resp = array($result,1);
                    return $resp;
                    break;
                case 'ADMINISTRADOR':
                    $resp = array($result,2);
                    return $resp;
                    break;
                case 'CALL CENTER':
                    $resp = array($result,3);
                    return $resp;
                    break;
                default:
                    $resp = array($result,0);
                    return $resp;
                break;
           }
       }
        
        //FUNCION PUBLICA PARA AUTENTICAR USUARIOS
        public function Autenticar($usuario, $clave, $posicion, $LATLON, $sistema, $local){
            $SQL = 'select * from ' . Config::$mvc_v_usuarios . ' where CLAVE = \'' . $this->CrearMD5($clave) . '\' and USUARIO = \'' . strtoupper($usuario) . '\' and SISTEMA = \'' . strtoupper($sistema) . '\'';
            //echo $SQL;
            $result = mssql_query($SQL,$this->conexion);
            if(!$result){
                $this->GuardaLOGS($usuario,"ERROR","#M03. Error al intentar conectar con la tabla de autenticacion SQL[" . $SQL . "]");
                die('<b>Error #M03:</b> Error al intentar conectar con la tabla de autenticaci&oacute;n.');
            }
            
            $datos = array();
            while($row = mssql_fetch_assoc($result)){
                $datos[] = $row;
            }
            if(count($datos)>0){
                //Llenar variables de inicio de sesion
                if($datos[0]['DIASPASSWORD'] <= 90){
                    if($datos[0]['INTENTOSLOGIN'] < 3){
                        session_start();
                        $_SESSION['log_IDUSUARIO'] = $datos[0]['ID_USUARIO'];
                        $_SESSION['log_CEDULA'] = $datos[0]['CEDULA'];
                        $_SESSION['log_EMPLEADO'] = $datos[0]['EMPLEADO'];
                        $_SESSION['log_IDEMPLEADO'] = $datos[0]['ID_EMPLEADO'];
                        $_SESSION['log_USUARIO'] = $datos[0]['USUARIO'];
                        $_SESSION['log_IDTECNICO'] = $datos[0]['ID_TECNICO'];
                        $_SESSION['log_IDZONA'] = $datos[0]['ID_ZONA'];
                        $_SESSION['log_SISTEMA'] = $datos[0]['SISTEMA'];
                        $_SESSION['log_LOCAL'] = $local;
                        $_SESSION['log_TIEMPOSESION'] = time();
                        $_SESSION['log_rolUsr'] = $this->rolUsuario($datos[0]['ID_USUARIO'])[1];
                        $_SESSION['log_nomRolUsr'] = $this->rolUsuario($datos[0]['ID_USUARIO'])[0];
                        $this->GuardaLOGS($usuario,"SESION","Usuario conectado GPS[" . $LATLON . "][" . $posicion . "].");
                        return '1';
                    }else{
                        $this->GuardaLOGS($usuario,"SESION","Usuario BLOQUEADO..");
                        return '3';
                    }
                }else{
                    return '2';
                    $this->GuardaLOGS($usuario,"SESION","La contrase�a el usuario ha caducado.");
                }
            }else{
                $this->GuardaLOGS($usuario,"SESION","Usuario/Constrase�a incorrectos.");
                return '0';
            }
        }
        //FUNCION PRIVADA PARA OBTENER CLAVE MD5
        private function CrearMD5($clave){
            return md5($clave);
        }
        
        //FUNCION PARA GUARDAR LOGS
        public function GuardaLOGS($usuario, $tipo, $mensaje){
            //ASIGNAR PERMISOS AL INICIAR SESION
            $usuario = strtolower(trim($usuario));
            $usuario = str_replace("�","�",$usuario);
            chmod(Config::$RutaLOGS . $usuario . "STO" . $this->Fecha() . ".log",  0777);
            //ABRIR ARCHIVO
            $LOG = fopen(Config::$RutaLOGS . $usuario . "STO" . $this->Fecha() . ".log", "a");
            fwrite($LOG, $this->Hora()."\t[" . $tipo . "]\t" . $mensaje . "\r\n");
            fclose($LOG);
        }
        
        public function grabaIntentosLogin($sistema,$usuario,$intentos){
            if ($sistema == 'SIAC'){
                $this->ActualizarDatos('Usuarios','intentosLogin=' . $intentos . ' + 1','where Usuario=\'' . $usuario . '\'');
            }else if ($sistema == 'SCAIIN'){
                $this->ActualizarDatos('usrUsuarios','intentosLogin=' . $intentos . ' + 1','where nom_usuario=\'' . $usuario . '\'');
            }
        }
        #End Region COMUN
        
        # Region SIAC
        //FUNCION PARA OBTENER EL ID INSERT DE LA TABLA SMSIACPARAMETER
        public function idInsertAgencia(){
            $idins = $this->ObtenerDatos('SmSiacParameters','IdInserts','','');
            return $idins[0]['IdInserts'];
        }
        
        //FUNCION PUBLICA PARA OBTENER HISTORIAL
        public function CargarHistorial($cuenta,$top){
            return $this->ObtenerDatos('Datos_Seniales','top ' . $top . ' Linea, Particion, Senial, Zona, Evento, Descripcion, Llegada','where Cuenta = \'' . $cuenta . '\'','order by Llegada desc');
        }
        
        //FUNCION PRIVADA PARA OBTENER LA CONSTANTE INSERTS DE APP MOVIL
        public function InsertsMovil(){
            $Insert =$this->ObtenerDatos('constantes','descripcion','Where idconstante=4','');
            return $Insert[0]['descripcion'];
        }
        //FUNCION PARA OBTENER EL CONTENIDO DE UNA COLUMNA DEL MANTENIMIENTO COMO EL ESTADO
        public function EstadoOrden($columna, $numorden){
            $Estado=$this->ObtenerDatos('ControlOrdenes','' . $columna .'' ,'Where IdOrden=\'' . $numorden . '\'','');
            return $Estado[0]['' . $columna . ''];
        }
        
        //FUNCION PARA OBTENER EL CONTENIDO DE UNA COLUMNA DEL MANTENIMIENTO COMO EL ESTADO
        public function EstadoMantenimiento($columna, $idmantenimiento){
            $EstadoOrden=$this->ObtenerDatos('Mantenimientos_Clientes','' . $columna .'' ,'Where Id_Mantenimiento=\'' . $idmantenimiento . '\'','');
            return $EstadoOrden[0]['' . $columna . ''];
        }
        
        //FUNCION PUBLICA PARA VERIFICAR SI EL MANTENIMIENTO YA SE PUEDE DAR DE BAJA O NO
        public function comprobarEstadoBaja($id){
            $respuesta = $this->ObtenerDatos('Mantenimientos_Clientes_Detalle','Count(IdReq) as num','where idMantenimiento = \'' . $id . '\' and estado <> 2 and estado <> 3','');
            return $respuesta[0]['num'];
        }
        
        //FUNCION PUBLICA PARA ALMACENAR LA GESTION
        public function AgregarMantenimiento($ID,$GESTION1,$DARBAJA,$TIPO, $IDREQ, $IDRMA, $tipoDoc){
            //Funcion para evitar el ingreso de comillas simples en la BDD
            $GESTION = str_replace("'","",$GESTION1);
            //OBTENER ID DE USUARIO ACTUAL
            session_start();
            $usuario = $_SESSION['log_USUARIO'];
            //$usuario = str_replace("�","�",$usuario);
            $idUsuario = $_SESSION['log_IDUSUARIO'];
            //OBTENER NUMERO DE GESTION
            $idGestion = ($this->NoGestion($ID)) + 1;
            
            //OBTENER IDENTIFICADO DE INSERTS PARA MOVIL
            $idMovil = $this->InsertsMovil();
            if($TIPO=='M'){
                //ALMACENAR GESTION
                $this->IngresarDatos('Notas_Mantenimientos_Clientes','Id_Mantenimiento,Fecha,Nota,Usuario,Id_Nota,idReq','\'' . $ID . '\',GETDATE(),\'' . $GESTION . '\',\'' . $usuario . '\',\'' . $idMovil . $ID . $idGestion . '\',' . $IDREQ . '');
                //ACTUALIZAR DATOS
                $this->ActualizarDatos('Mantenimientos_Clientes','MsgEjecutado=1','where Id_Mantenimiento=\'' . $ID . '\'');
                //ACTUALIZA ESTADO DEL REQUERIMIENTO
                $this->ActualizarDatos('Mantenimientos_Clientes_Detalle','Estado=1','where idMantenimiento=\'' . $ID . '\' and IdReq='. $IDREQ .'');
                //VERIFICAR SI HAY QUE DAR DE BAJA
                if($DARBAJA=='true'){
                    //ACTUALZA EL ESTADO DEL REQUERIMIENTO A 2
                    $this->ActualizarDatos('Mantenimientos_Clientes_Detalle','estado=2, fechaRealizado=GETDATE(),usrRealizado=' . $idUsuario . '','where idMantenimiento=\'' . $ID . '\' and IdReq='. $IDREQ .'');
                    $Listo = $this->comprobarEstadoBaja($ID);
                    if ($Listo == 0){
                        //CODIGO DE PRUEBA
                        //ELIMINARLO EN CUANTO DEJE DE SER NECESARIO
                        $this->GuardaLOGS($usuario,"MANTENIMIENTO|BAJA","ID[" . $ID . "].");
                        //FIN CODIGO
                        $this->ActualizarDatos('Mantenimientos_Clientes','Estado=1, Id_Ejecutor=\'' . $idUsuario .'\', Auditor=\'' . $usuario . '\', FechaAuditor=GETDATE()','where Id_Mantenimiento=\'' . $ID . '\'');
                    }
                }
            }
            if($TIPO=='A'){
                $idGestion = $this->NumGestion($ID);
                if($idGestion == null){$idGestion=0;}
                $this->IngresarDatos('NotasOrdenesTrabajo','IdOrden,Fecha,Nota,Usuario,IdNota','\'' . $ID . '\',GETDATE(),\'' . $GESTION . '\',\'' . $usuario . '\',\'' . $idMovil . $ID . $idGestion . '\'');
                $this->ActualizarDatos('ControlOrdenes','MsgEjecutado=1','where IdOrden=\'' . $ID . '\'');
                //VERIFICAR SI HAY QUE DAR DE BAJA
                if($DARBAJA=='true'){
                    //CODIGO DE PRUEBA
                    //ELIMINARLO EN CUANTO DEJE DE SER NECESARIO
                    $this->GuardaLOGS($usuario,"ASISTENCIA|BAJA","ID[" . $ID . "].");
                    //FIN CODIGO
                    $this->ActualizarDatos('ControlOrdenes','Estado=2, IdEjecutor=\'' . $idUsuario .'\', UsuarioEjecucion=\'' . $usuario . '\', FechaEjecucion=GETDATE()','where IdOrden=\'' . $ID . '\'');
                    //SENTENCIA PARA ACTUALIZAR EL ESTADO DE LA ASISTENCIA EN POSTGRESQL
                    conxPostgreSQL::cambioEstadoRMA('' . $IDRMA . '',2, ''. $tipoDoc .'');
                }
            }
        }
        
        //FUNCION PUBLICA PARA CARGAR LOS SERVICIOS PARA REQUERIMIENTOS
        public function MantenimientoServicios($tipo){
            if ($tipo==0){
                return $this->ObtenerDatos('Servicios','*','where Estado = 1','order by Servicio Asc');
            }elseif ($tipo == 1){
                return $this->ObtenerDatos('Servicios','*','where Estado = 1 and IdServicio = 32','order by Servicio Asc');
            }
        }
        
        //FUNCION PUBLICA PARA CARGAR LOS SUB-SERVICIOS DE ACUERDO AL SERVICO SELECCIONADO
        public function MantenimientoSubServicios($id){
            return $this->ObtenerDatos('SubServicios','*','where IdServicio=' . $id . ' and estado = 1','order by SubServicio');
        }
        
        //FUNCION PUBLICA PARA CARGAR LAS SUB-CATEGORIAS DE ACUERDO AL SUB-SERVICO SELECCIONADO
        public function MantenimientoSubCategoria($id){
            return $this->ObtenerDatos('RequerimientoSubCategoria','*','where IdSubServicio=' . $id . ' and estado = 1','order by SubCategoria');
        }
        
        //FUNCION PUBLICA SABER EL NUMERO M�XIMO DE REQUERIMIENTOS EN PARA INGRESAR
        public function mantenimientoMax(){
            $MaxNum = $this->ObtenerDatos('Requerimientos','IsNull(max(Cast(substring(IdRequerimiento,5,len(IdRequerimiento)-4) as numeric)),0) + 1 as MaxId','','');
            return $MaxNum[0]['MaxId'];
        }
        
        //FUNCION PUBLICA SABER EL NUMERO RE QUEQUERIMIENTO CON EL QUE FUE CREADO EN PRIMERA INSTANCIA
        public function mantenimientoObservacion($id){
            return $this->ObtenerDatos('Mantenimientos_Clientes','top(1) isnull(cast(Observacion as nvarchar),0) as observacion,Cita,Contacto','WHERE Id_Mantenimiento = \'' . $id . '\'','');
        }
        
        //FUNCION PUBLICA PARA OBTENER DATOS DEL REQUERIMIENTO
        public function datosRequerimientoDetalle($id,$idreq){
            return $this->ObtenerDatos('Mantenimientos_Clientes_Detalle','top(1) idMantenimiento, requerimiento,prioridad,codCliente','where idMantenimiento = \'' . $id . '\' and idReq=\'' . $idreq . '\'','');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL DATO DE LAS POSIBLES ORDENES SI ESTAN O NO REPETIDAS
        public function posibleOrdenRepetida($id){
            $MaxNum = $this->ObtenerDatos('RequerimientosOrden','count(IdRequerimiento) as num','where IdRequerimiento= \'' . $id . '\'','');
            return $MaxNum[0]['num'];
        }
        
        //FUNCION P�BLICA PARA CAMBIAR DE ESTADO AL REQUERIMIENTO QUE PASA COMO POSIBLE ORDEN
        public function actualizaMantenimiento($tabla,$campos,$condicion){
            $this->ActualizarDatos($tabla,$campos,$condicion);
        }
        
        //FUNCION P�BLICA QUE CONSULTA LAS NOTAS QUE TENGAN LOS MANTENIMIENTOS
        public function notasMantenimientos($id,$idreq){
            return $this->ObtenerDatos('Notas_Mantenimientos_Clientes','*','where id_Mantenimiento = \'' . $id . '\' and idReq=\'' . $idreq . '\'','Order by Fecha desc');
        }
        
        //FUNCION P�BLICA QUE DEVUELVE EL Número DE NOTAS QUE TENGAN LOS MANTENIMIENTOS
        public function cuentaRegistros($tipo,$id,$idreq){
            switch ($tipo){
                case 0:
                    $respuesta = $this->ObtenerDatos('NotasPOrdenesTrabajo','count(Fecha) as num','where idOrden = \'' . $id . '\'','');
                    //$val = $respuesta[0]['num'] + 1;
                    return $respuesta[0]['num'];
                    break;
                case 1:
                    $respuesta = $this->ObtenerDatos('Notas_Mantenimientos_Clientes','count(Fecha) as num','where Id_Mantenimiento = \'' . $id . '\' and idReq=\'' . $idreq . '\'','');
                    $val = $respuesta[0]['num'] + 1;
                    return $val;
                    break;
            }
        }
        
        // FUNCION P�BLICA PARA INGRESAR UN NUEVO REQUERIMIENTO
        public function nuevoRequerimiento($tabla, $campo,$valor){
            $this -> IngresarDatos($tabla,$campo,$valor);
        }
        
        //FUNCTION PUBLICA PARA BUSCAR CLIENTES
        public function BuscarCliente($buscarPor, $criterio){
            switch($buscarPor){
                case 'nombre':
                    return $this->ObtenerDatos('Cliente_Monitoreo_Adt','Top ' . Config::$MaxClientes . ' Codigo, Nombre, Direccion, Telefonos, Referencia','where Nombre like \'%' . $criterio . '%\' and State = \'A\'','order by Nombre Asc');
                    break;
                case 'codigo':
                    return $this->ObtenerDatos('Cliente_Monitoreo_Adt','Top ' . Config::$MaxClientes . 'Codigo, Nombre, Direccion, Telefonos, Referencia','where Codigo = \'' . $criterio . '\' and State = \'A\'','order by Nombre Asc');
                    break;
                case 'RUC':
                    return $this->ObtenerDatos('Cliente_Monitoreo_Adt','Top ' . Config::$MaxClientes . 'Codigo, Nombre, Direccion, Telefonos, Referencia','where RucCi = \'' . $criterio . '\' and State = \'A\'','order by Nombre Asc');
                    //return $this->ObtenerDatos('V_BUSQUEDACLIENTES','Top ' . Config::$MaxClientes . ' Codigo_Monitoreo as Codigo, Nombre, Direccion, Telefonos, Referencia','where RUC like \'%' . $criterio . '%\' and State = \'A\'','order by Nombre Asc');
                    break;
            }
        }
        //FUNCION PUBLICA PARA OBTENER LISTA HORARIOS DISPONIBLES PARA CLIENTES
        public function Horarios(){
            return $this->ObtenerDatos('SMShorariosClientes','HorarioInicio, HorarioFin','','ORDER BY HorarioInicio');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL NUMERO M�XIMO DE MAILS INGRESADOS AL CLIENTE
        public function maxMail($cod){
            $datos = $this->ObtenerDatos('e_mail_adt','ISNULL(Max(orden), 0) as Max','Where codigo=' . $cod . '','');
            return $datos[0]['Max'];
        }
        
        //FUNCION PUBLICA PARA OBTENER LISTA HORARIOS X CLIENTE
        public function HorariosXcliente($ID){
            return $this->ObtenerDatos('HorariosCuenta_adt','*','where Cuenta=\'' . $ID . '\'' ,'');
        }
        //FUNCION PRIVADA PARA CONOCER EL NUMERO DE REGISTROS (GESTIONES EN ASISTENCIAS)
        private function NumGestion($ID){
            $datos = $this->ObtenerDatos('NotasOrdenesTrabajo','count(IdNota) as Numero','Where IdOrden=\'' . $ID . '\'','');
            return $datos[0]['Numero'];
        }
        //FUNCION PRIVADA PARA CONOCER EL NUMERO DE REGISTROS (GESTIONES EN MANTENIMIENTOS)
        private function NoGestion($ID){
            $datos = $this->ObtenerDatos('Notas_Mantenimientos_Clientes','count(Id_Nota) as Numero','Where Id_Mantenimiento=\'' . $ID . '\'','');
            return $datos[0]['Numero'];
        }
        //FUNCION PUBLICA PARA OBTENER LISTA DE ZONAS
        public function Zonas(){
            return $this->ObtenerDatos('SECTOR_TECNICOS','ID_ZONA, UPPER(ZONA) AS [ZONA]','WHERE ACTIVO = 0','ORDER BY ID_ZONA ASC');
        }
        //FUNCION PUBLICA PARA OBTENER LISTA DE TECNICOS
        public function Tecnicos(){
            return $this->ObtenerDatos('TECNICOS','ID_TECNICO, UPPER(NOMBRE) AS [NOMBRE]','WHERE ACTIVO = 0','ORDER BY NOMBRE ASC');
        }
        
        //FUNCION PUBLICA PARA OBTENER NUMERO DE REGISTROS
        public function NRegistros($tabla,$campos,$condicion){
            return count($this->ObtenerDatos($tabla,$campos,$condicion,''));
        }
        
        //FUNCTION PUBLICA PARA AUTENTICAR Y OBTENER DATOS DE USUARIO
        public function Empleados(){
            return $this->ObtenerDatos('Empleados','*','','');
        }
        //FUNCION PUBLICA PARA VER MANTENIMIENTOS
        public function Mantenimientos($ZONA){
            return $this->ObtenerDatos('V_MANTENIMIENTOS','*','WHERE ZONA = \'' . $ZONA . '\' and tipo = -1 AND Cita <= getdate() + 1','ORDER BY ORDEN DESC, CITA');
        }
        
        //FUNCION PUBLICA QUE LISTA EL DETALLE DE MANTENIMIENTOS SEGUN EL ID DE MANTENIMIENTO DADO
        public function MantenimientosDetalle($id){
            return $this->ObtenerDatos('V_MantenimientosDetalle','*','WHERE IdMantenimiento = \'' . $id . '\' and estado != 2 and estado != 3','ORDER BY IdPrioridad Desc');
        }
        
        //FUNCION PUBLICA PARA REVISAR GESTIONES (MANTENIMIENTOS)
        public function MantenimientosGestiones($ID, $idReq){
            if ($idReq == undefined){
                return $this->ObtenerDatos('V_MANTENIMIENTOS_GESTIONES','TOP(10) *','WHERE ID = \'' . $ID . '\'','ORDER BY FECHA DESC');
            }else{
                return $this->ObtenerDatos('V_MANTENIMIENTOS_GESTIONES','TOP(5) *','WHERE ID = \'' . $ID . '\' and idReq=' . $idReq . '','ORDER BY FECHA DESC');
            }
        }
        
        //FUNCION PUBLICA PARA OBTENER INFORMACION DE MANTENIMIENTO
        public function DatosMantenimiento($ID){
            return $this->ObtenerDatos('V_MANTENIMIENTOS','*','WHERE ID = \'' . $ID . '\'','');
        }
        //FUNCION PUBLICA PARA REVISAR GESTIONES (ASISTENCIAS)
        public function AsistenciasGestiones($ID){
            return $this->ObtenerDatos('NotasOrdenesTrabajo','Top(5) *','where IdOrden=\'' . $ID . '\'','order by Fecha Desc');
        }
        //FUNCION PUBLICA PARA OBTENER INFORMACION DE LAS ASISTENCIAS
        public function DatosAsistencias($ID){
            return $this->ObtenerDatos('OrdenesTrabajoConsulta','*','WHERE Numero = \'' . $ID . '\' and Estado =\'Asignado\'','');
        }
        //FUNCION PUBLICA PARA OBTENER LAS ASISTENCIAS ASIGNADAS A LOS TECNICOS
        public function OrdenesAsignadas($ID){
            return $this->ObtenerDatos('OrdenesTrabajoConsulta  OT LEFT JOIN ControlOrdenes_codigos OTC ON OT.Numero= OTC.IdOrden',
                    '*','Where Tipo<>\' xxx \' and IdEncargado=\'' . $ID . '\' and Estado =\'Asignado\'','Order by Cita');
        }
        
        //FUNCION PUBLICA PARA OBTENER LAS PRIORIDADES DEL PERSONAL EMERGENTE DE DATOS DE CLIENTES
        public function prioridadEmergente(){
            return $this->ObtenerDatos('personalEmergentePrioridad','*','Where estado = 1','Order by idPrioridad');
        }
        
        //FUNCION PUBLICA PARA OBTENER LOS CARGOS DEL PERSONAL EMERGENTE DE DATOS DE CLIENTES
        public function cargosEmergente(){
            return $this->ObtenerDatos('personalEmergenteCargos','*','Where estado = 1','Order by idCargo');
        }
        
        //FUNCION PUBLICA PARA EDICION DE REGISTROS SISTEMA SIAC
        public function EdicionRegistros($tabla,$campos,$condicion){
            $this->ActualizarDatos($tabla,$campos,$condicion);
        }
        
        //FUNCION P�BLICA PARA INSERTAR REGISTROS
        public function insertaRegistros($tabla, $campos, $valores){
            $this-> IngresarDatos($tabla, $campos, $valores);
        }

        //FUNCTION PUBLICA PARA BUSCAR CLIENTES SIN POSICIONAMIENTO
        public function clienteSinCoordenada($buscarPor, $criterio){
            switch($buscarPor){
                case 'nombre':
                    return $this->ObtenerDatos('V_CLIENTE_SINCOORDENADAS','Top ' . Config::$MaxClientes . ' Codigo_Monitoreo as Codigo, Nombre, Direccion, Telefonos, Referencia','where Nombre like \'%' . $criterio . '%\'','order by Nombre Asc');
                    break;
                case 'codigo':
                    return $this->ObtenerDatos('V_CLIENTE_SINCOORDENADAS','Top ' . Config::$MaxClientes . ' Codigo_Monitoreo as Codigo, Nombre, Direccion, Telefonos, Referencia','where Codigo_Monitoreo =  \'' . $criterio . '\'','order by Nombre Asc');
                    break;
                case 'RUC':
                    return $this->ObtenerDatos('V_CLIENTE_SINCOORDENADAS','Top ' . Config::$MaxClientes . ' Codigo_Monitoreo as Codigo, Nombre, Direccion, Telefonos, Referencia','where RUC like \'%' . $criterio . '%\'','order by Nombre Asc');
                    break;
            }
        }
        
        public function tipoRegistroHoraAsistencias($idAsistencia){
            $usuario = $_SESSION['log_USUARIO'];
            $traslado = $this->ObtenerDatos('controlOrdenes','trasladoTecnico','where idOrden= \'' . $idAsistencia .'\'','');
            $cierre = $this->ObtenerDatos('horaAsistencias','count(idAsistencia) as num',' where idAsistencia = \'' . $idAsistencia .'\' and tipoDetalle= \'C-TB\' and usuario=\'' . $usuario .'\'','');
            $ultimoReg = $this->ObtenerDatos('horaAsistencias','top(1) isnull(tipoDetalle,\'\') as tipoDetalle',' where idAsistencia= \'' . $idAsistencia .'\' and usuario=\'' . $usuario .'\'',' order by creacion desc');
            switch($ultimoReg[0][tipoDetalle]) {
                case '':
                    //$where = 'WHERE idTipo = \'I-TB\' OR idTipo = \'F-TB\' OR idTipo =\'C-TB\'';
                    if($traslado[0][trasladoTecnico] == 1){
                        $where = ' WHERE idTipo = \'A-TB\' OR idTipo =\'I-TS\'';
                    }else{
                        $where = ' WHERE idTipo = \'A-TB\'';
                    }
                    break;
                case 'A-TB':
                    $where = 'WHERE idTipo = \'F-TB\' ';
                    
                    break;
                case 'I-TB':
                    $where = ' WHERE idTipo = \'F-TB\' ';
                    break;
                case 'F-TB':
                    $where = 'WHERE idTipo = \'I-TB\' OR idTipo =\'C-TB\'';
                    break;
                case 'C-TB':
                    if($traslado[0][trasladoTecnico] == 1){
                        $where = 'WHERE idTipo =\'I-TS\'';
                    }else{
                        $where = 'WHERE idTipo=\'HOLA\'';
                    }
                    break;
                case 'I-TS':
                    $where = 'WHERE idTipo =\'F-TS\'';
                    break;
                case 'F-TS':
                    if ($cierre[0][num] == 0){
                        $where = 'WHERE idTipo = \'A-TB\'';
                    }else{
                        $where = 'WHERE idTipo=\'HOLA\'';
                    }
                    
                    break;
            }
            return $this->ObtenerDatos('horaAsistenciasTipo','idTipo, UPPER(descripcion) as descripcion','' . $where .'','');
        }
        
        public function ingresaRegistroHoras($idOrden, $tipoDet, $lat, $lon){
            $usuario = $_SESSION['log_USUARIO'];
            $this->IngresarDatos('horaAsistencias','idAsistencia,tipoDetalle,latitud,longitud,usuario,creacion','\'' . $idOrden . '\',\'' . $tipoDet . '\',\'' . $lat . '\',\'' . $lon . '\',\'' . $usuario . '\',GETDATE()');
        }
        
        #End Region SIAC
        
        #Region SCAIIN
        //FUNCION PUBLICA PARA OBTENER TODAS LAS FACTURAS QUE TIENE SALDO PENDIENTE - SCAIIN
        public function FacturasPendientes($num, $codcli, $caja, $var, $local){
            return $this->ObtenerDatos('vFacturaxCliente','*','Where Numero=' . $num . ' and Cliente=' . $codcli . ' and IDcaja=' . $caja . ' and ' . $var . ' > 0 and Local = '. $local . '','Order by Numero');
        }
        // FUNCION PUBLICA PARA DISTINGUIR CUANTOS CLIENTES TIENEN EL MISMO NUMERO DE FACTURA
        public function CuentaClientes($num, $local){
            //return $this->ObtenerDatos('vClientesxFactura','Cliente, IDcaja','Where Número=' . $num . '','Group by Cliente');
            return $this->ObtenerDatos('vClientesxFactura','Cliente, IDcaja','Where Número=' . $num . ' and Local = '. $local . '','GROUP BY IDcaja, Cliente');
        }
        //FUNCION P�BLICA PARA OBTENER EL VALOR PENDIENTE LA LA FACTURA O DE LA CUOTA DE LA FACTURA
        public function saldofactura($cuota, $num, $caja,$local){
            if ($cuota == 0){
                $detalle = 'A.saldoFactura';
            }else{
                $detalle = 'A.saldovalor';
            }
            return $this->ObtenerDatos('(SELECT dbo.Facturas.Número, dbo.Facturas.Fecha,dbo.Facturas.Cliente, dbo.Facturas.TotalFactura, dbo.Facturas.SaldoFactura, dbo.Facturas.NombreCliente,0 as Cuota,
        			0 saldovalor,Facturas.caja IDcaja,caja.Descripcion Caja  FROM dbo.Facturas
        			inner join Caja on Caja.Numero=Facturas.Caja and facturas.local = caja.Local where dbo.Facturas.SaldoFactura>0 and facturas.forma_pago<>2 and facturas.Local = '. $local .'
        			Union
        			SELECT dbo.Facturas.Número, dbo.Facturas.Fecha,dbo.Facturas.Cliente, dbo.Facturas.TotalFactura, dbo.Facturas.SaldoFactura, dbo.Facturas.NombreCliente,isnull(dbo.FormaPagoFactura_Detalle.Formapago,0) as Cuota,
        			dbo.FormaPagoFactura_Detalle.saldovalor,Facturas.caja IDcaja,caja.Descripcion Caja  FROM dbo.Facturas inner join dbo.FormaPagoFactura_Detalle ON dbo.Facturas.Número = dbo.FormaPagoFactura_Detalle.Factura  and Facturas.Caja=FormaPagoFactura_Detalle.Caja
                    and FormaPagoFactura_Detalle.local = facturas.local
        			inner join Caja on Caja.Numero=Facturas.Caja and caja.Local = facturas.Local where dbo.Facturas.SaldoFactura>0 and facturas.Local = '. $local .') A','A.Número As Numero, ' . $detalle . '','Where Número=' . $num . '  and A.Cuota=' . $cuota . ' and A.IDcaja=' . $caja . '','Order by Numero');
        }
        
        //FUNCION PUBLICA PARA OBTENER LAS GUIAS DE COBRO
        public function GuiasCobro($numguia,$local){
        return $this->ObtenerDatos('CobranzaFacturación','[Empresa],[Local],[Caja],[Número] as Numero,[Fecha],[Hora],[Estado],[Empleado],[Documento],[Observaciones],[TotalbrutoE],[TotalMonedas],
                    [TotalBilletes],[Id_usuario_C],[creacion],[Id_Usuario_A],[Actualizacion],[TotalCobranza],[TotalCheque],[RecibodeCaja],[Creacion_ReciboCaja],[callCenter],
                    (select nom_usuario from usrusuarios where cod_usuario=CobranzaFacturación.id_usuario_c) as usuario,(select nombre from empleados where empleado=CobranzaFacturación.empleado) as empleadonombre,origen',
            'WHERE Número=' . $numguia . ' and local = '. $local . '','order by Número');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL CODIGO DEL EMPLEADO QUE REALIZA LA GU�A
        public function codigoempleado($codigo, $sistema){
            switch ($sistema){
                case 'SIAC':
                    return $this->ObtenerDatos('Usuarios','Empleado','WHERE IdUsuario=' . $codigo . '','');
                    break;
                case 'SCAIIN':
                    return $this->ObtenerDatos('UsrUsuarios','Empleado','WHERE cod_usuario=' . $codigo . '','');
                    break;
            }
        }
        
        //FUNCION PUBLICA PARA VERIFICAR SI EL USUARIO TIENE PERMISO A GUIAS DE COBRO EN SCAIIN EXPRESS
        public function permisoguia($cod){;
        return $this->ObtenerDatos('UsrUsuar_Formulario','count(*) as dato','WHERE cod_formulario = 161 AND tipo_permiso =\'E\' AND cod_usuario =' . $cod . '','');
        }
        
        //FUNCION PUBLICA PARA OBTENER LAS GUIAS DE COBRO
        public function UltimoNumero($local, $opt){
            if($opt==1){
                return $this->ObtenerDatos('CobranzaFacturación','max(Número) as Numero','Where local = ' . $local . '','');
            }else{ 
                return $this->ObtenerDatos('proformas','max(Número) as Numero','Where local = ' . $local . '','');
            }
        }
        
        //FUNCION PUBLICA PARA OBTENER LAS GUIAS DE COBRO por usuario
        public function UltimoNumero1($usuario,$local){
            return $this->ObtenerDatos('CobranzaFacturación','max(Número) as Numero','Where Id_Usuario_C= ' . $usuario . ' and local = ' . $local . '','');
        }
        
        //FUNCION PUBLICA PARA OBTENER VALOR COBRADO
        public function valorcobrado($numero, $factura, $cuota, $tipo, $local){
            switch ($tipo){
                case 'efectivo':
                    return $this->ObtenerDatos('[CobranzaFacturación detalle]','sum(Valorcobro) as vCobro','Where Número=\'' . $numero . '\' and Factura=' . $factura .' and cuota=' . $cuota . ' and Local = '. $local .' ','');
                    break;
                    
            }
        }
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE LA FACTURA
        public function detallefactura($num,$local){
            return $this->ObtenerDatos('[Reporte_Factura_detalle]','Nompro, Detalles','Where Número=' . $num . ' and Local = '. $local .'','');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE COBRANZA
        public function detalle($numero,$local){
            return $this->ObtenerDatos('[CobranzaFacturación detalle]','*','Where Número=\'' . $numero . '\' and Local = '. $local .'','');
        }
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE BILLETES
        public function billetes($numero,$local){
            return $this->ObtenerDatos('CobranzaFacturaciónBilletes','*','Where Número=\'' . $numero . '\' and local = '. $local .'','order by UnidxBillete');
        }
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE MONEDAS
        public function monedas($numero,$local){
            return $this->ObtenerDatos('CobranzaFacturaciónMonedas','*','Where Número=\'' . $numero . '\' and Local = '. $local .'','order by UnidxMoneda');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE CHEQUES
        public function cheques($numero, $local){
            return $this->ObtenerDatos('CobranzaFacturaciónCheque','*','Where Número=\'' . $numero . '\' and Local = '. $local .'','');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE CHEQUES PARA GUARDAR
        public function validacheque($numero,$local){
            return $this->ObtenerDatos('CobranzaFacturaciónCheque','banco,Cheque,cuenta','Where Número=\'' . $numero . '\' and Local = '. $local .'','');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL LISTADO DE BANCOS
        public function bancos(){
            return $this->ObtenerDatos('[Recibos de Caja_Bancos]','Banco, Codigo','','Order by Banco');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL LISTADO DE LAS AGENCIAS DE LA EMPRESA
        public function agencias(){
            //return $this->ObtenerDatos('Clientes_Tipo','Descripci�n as Descripcion, Denominativo,url','WHERE Denominativo IS NOT NULL','Order by Descripcion desc');
            return $this->ObtenerDatos('Clientes_Tipo','Descripción as Descripcion, Denominativo,Tipo_Cliente','WHERE Denominativo IS NOT NULL','Order by Descripcion desc');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE tansferencias Y/O tarjetas de credito
        public function transacciones($numero, $tipo, $local){
            return $this->ObtenerDatos('CobranzaFacturacionTransacciones','*','Where numero=\'' . $numero . '\' and tipoTransaccion=\'' . $tipo . '\' and Local = '. $local .'','order by row');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL LISTADO DE BANCOS CON EL NOMBRE DE LAS TARJETS DE CREDITO
        public function bancosTarjetas(){
            return $this->ObtenerDatos('[Recibos de Caja_TipoTarjeta]','codt as COD, rtrim(tarjeta)+\' - \'+rtrim(banco)as Tarjeta','','Order by Banco');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL DETALLE DE RETENCNIONES
        public function retenciones($numero,$local){
            return $this->ObtenerDatos('CobranzaFacturacionRetenciones','*','Where numero=\'' . $numero . '\' and Local = '. $local .' order by row');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL LISTADO DE LOS TIPOS DE RUBRO PARA LAS RETENCIONES
        public function tipoRubro(){
            return $this->ObtenerDatos('[Recibos de Caja_TipoRubro]','Rubro, Decsripción as Descripcion','','Order by Descripcion');
        }
        
        //FUNCION PUBLICA PARA OBTENER EL LISTADO DE CODIGOS DE IMPUESTOS Y SU DETALLE
        public function detalleImpuesto(){
            return $this->ObtenerDatos('T10_ConceptosAIRPrincipal','CodImp, Detalle','','Order by CodImp');
        }
        
        // FUNCION PUBLICA PARA INSERTAR NUEVOS REGISTROS A LA GUIA DE COBRO
        public function NuevoRegistro($tipo,$valoref,$valorch,$billetes,$monedas,$transferencia,$tarjeta,$retencion,$salidaCliente) {
            switch ($tipo) {
                case 'efectivo':
                    $this->IngresarDatos('[CobranzaFacturación detalle]','Empresa, [Local],Caja,Número,Factura,Valorcobro,Cuota,origen', '' . $valoref . '');
                    break;
                case 'cheque':
                    $this->IngresarDatos('[CobranzaFacturaciónCheque]','Empresa, [Local],Caja,Número,Factura,Valor,Cuota,origen','' . $valorch . '');
                    break;
                case 'billetes':
                    $this->IngresarDatos('[CobranzaFacturaciónBilletes]','Empresa,[Local],Caja,Número,UnidxBillete,Cantidad,origen','' . $billetes . '');
                    break;
                case 'monedas':
                    $this->IngresarDatos('[CobranzaFacturaciónMonedas]','Empresa,[Local],Caja,Número,UnidxMoneda,Cantidad,origen','' . $monedas . '');
                    break;
                case 'transferencia':
                    $this->IngresarDatos('[CobranzaFacturacionTransacciones]','Empresa, [Local],Caja,Numero,factura,valor,cuota,documento,tipoTransaccion,banco,origen','' . $transferencia . '');
                    break;
                case 'tarjeta':
                    $this->IngresarDatos('[CobranzaFacturacionTransacciones]','Empresa, [Local],Caja,Numero,factura,valor,cuota,documento,tipoTransaccion,banco,origen','' . $tarjeta . '');
                    break;
                case 'retencion':
                    $this->IngresarDatos('[CobranzaFacturacionRetenciones]','Empresa, [Local],Caja,Numero,factura,valorRubro,cuota,numDocumento,origen','' . $retencion . '');
                    break;
                case 'salidaCliente':
                    $this->IngresarDatos('[clientesCancelacionServicio]','cliente, codMonitoreo,motivoGestion,observacion,fechaCreacion,idUsuarioCreacion,estado','' . $salidaCliente . '');
                    break;
            }
        }
        
        // FUNCION PUBLICA PARA VALIDAR EL INGRESO DE NUEVAS FACTURAS A LA GUIA DE COBRO
        public function ValidaFactura($cuota,$factura,$guia,$saldo,$caja,$local) {
            
            $valida = $this->ObtenerDatos('(select Valorcobro as Valor from [CobranzaFacturación detalle] where Número=' . $guia . ' and factura=' . $factura . ' and cuota=' . $cuota . ' and caja=' . $caja . ' and Local = '. $local .'
        					union select Valor as Valor	from [CobranzaFacturaciónCheque] where Número=' . $guia . '  and factura=' . $factura . ' and cuota=' . $cuota . ' and caja=' . $caja . ' and Local = '. $local .'
        					union select valor as Valor from [CobranzaFacturacionTransacciones] where numero=' . $guia . '  and factura=' . $factura . ' and cuota=' . $cuota . ' and caja=' . $caja . ' and Local = '. $local .'
        					union select valorRubro as Valor from [cobranzaFacturacionRetenciones] where numero=' . $guia . '  and factura=' . $factura . ' and cuota=' . $cuota . ' and caja=' . $caja . ' and Local = '. $local .') A','Sum(A.Valor) as Suma', '','');
            $valida = $valida[0][Suma];
            if ($valida > 0 && $valida <= $saldo){
                return  $valida;
            }elseif($valida > $saldo ){
                return $valida;
            }else{
                return $valida = 0;
            }
        }
        
        // FUNCION PUBLICA PARA ELIMINAR REGISTROS DE COBRANZA A LA GUIA DE COBRO
        public function EliminaRegistro($tipo, $guia, $caja, $cuota,$factura,$denominacion,$orden,$local) {
            switch ($tipo) {
                case 'efectivo':
                    $this->EliminarDatos('[CobranzaFacturación detalle]','Número=' . $guia . ' AND caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row='. $orden .' and Local = '. $local .'');
                    break;
                case 'cheque':
                    $this->EliminarDatos('[CobranzaFacturaciónCheque]','Número=' . $guia . ' AND caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row='. $orden .' and Local = '. $local .'');
                    break;
                case 'billetes':
                    $this->EliminarDatos('[CobranzaFacturaciónBilletes]','Número=' . $guia . ' AND UnidxBillete=' . $denominacion . ' and local = '. $local .'');
                    break;
                case 'monedas':
                    $this->EliminarDatos('[CobranzaFacturaciónMonedas]','Número=' . $guia . ' AND UnidxMoneda=' . $denominacion . ' and Local = '. $local .'');
                    break;
                case 'transferencia':
                    $this->EliminarDatos('[CobranzaFacturacionTransacciones]','numero=' . $guia . ' AND Caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row='. $orden .' and Local = '. $local .'');
                    break;
                case 'tarjeta':
                    $this->EliminarDatos('[CobranzaFacturacionTransacciones]','numero=' . $guia . ' AND Caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row='. $orden .' and Local = '. $local .'');
                    break;
                case 'retencion':
                    $this->EliminarDatos('[CobranzaFacturacionRetenciones]','numero=' . $guia . ' AND Caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row='. $orden .' and Local = '. $local .'');
                    break;
            }
        }
        
        function totaldetalle($guia,$tipo,$local){
            switch ($tipo){
                case 'billetes':
                    return $this->ObtenerDatos('[CobranzaFacturaciónBilletes]','sum(unidxbillete*cantidad) as total','Where Número=\'' . $guia . '\' and local = '. $local .'','');
                    break;
                case 'monedas':
                    return $this->ObtenerDatos('[CobranzaFacturaciónMonedas]','sum(unidxmoneda*cantidad) as total','Where Número=\'' . $guia . '\' and Local = '. $local .'','');
                    break;
                case 'efectivo':
                    return $this->ObtenerDatos('[CobranzaFacturación detalle]','sum(Valorcobro) as total','Where Número=\'' . $guia . '\' and Local = '. $local .'','');
                    break;
                case 'cheque':
                    return $this->ObtenerDatos('[CobranzaFacturaciónCheque]','sum(valor) as total','Where Número=\'' . $guia . '\' and Local = '. $local .'','');
                    break;
                case 'transferencia':
                    return $this->ObtenerDatos('[CobranzaFacturacionTransacciones]','sum(valor) as total','Where Numero=\'' . $guia . '\' and tipoTransaccion=\'TR\' and Local = '. $local .'','');
                    break;
                case 'tarjeta':
                    return $this->ObtenerDatos('[CobranzaFacturacionTransacciones]','sum(valor) as total','Where Numero=\'' . $guia . '\' and tipoTransaccion=\'TC\' and Local = '. $local .'','');
                    break;
                case 'retencion':
                    return $this->ObtenerDatos('[CobranzaFacturacionRetenciones]','sum(valorRubro) as total','Where Numero=\'' . $guia . '\' and Local = '. $local .'','');
                    break;
            }
        }
        
        // FUNCION PUBLICA PARA EDITAR LOS REGISTROS DE COBRANZA A LA GUIA DE COBRO
        public function EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$documento,$valor,$posfechado,$retencion,$cuenta,$fechapos,$denominacion,$numero_orden,$local) {
            switch ($tipo) {
                case 'efectivo':
                    $this->ActualizarDatos('[CobranzaFacturación detalle]','Valorcobro=' . $valor . ',detalle=' . $retencion . '','WHERE Número=' . $guia . ' AND Caja=' . $caja . ' AND Cuota=' . $cuota . ' AND Factura=' . $factura . ' AND row =' . $numero_orden . ' and Local = '. $local .'');
                    break;
                case 'cheque':
                    if($fechapos!='NULL'){
                        $this->ActualizarDatos('[CobranzaFacturaciónCheque]','Cheque=' . $documento . ',Valor=' . $valor . ',Posfecha=' . $posfechado . ',Cuota=' . $cuota . ',Detalles=' . $retencion . ',banco=\'' . $banco . '\',cuenta=' . $cuenta . ',fechaP=\'' . $fechapos . '\'','WHERE Número=' . $guia . ' AND caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row =' . $numero_orden . ' and Local = '. $local .'');
                    }else{
                        $this->ActualizarDatos('[CobranzaFacturaciónCheque]','Cheque=' . $documento . ',Valor=' . $valor . ',Posfecha=' . $posfechado . ',Cuota=' . $cuota . ',Detalles=' . $retencion . ',banco=\'' . $banco . '\',cuenta=' . $cuenta . ',fechaP=' . $fechapos . '','WHERE Número=' . $guia . ' AND caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row =' . $numero_orden . ' and Local = '. $local .'');
                    }
                    break;
                case 'billetes' :
                    $this->ActualizarDatos('[CobranzaFacturaciónBilletes]','Cantidad=' . $valor . '','WHERE Número=' . $guia . ' AND UnidxBillete=' . $denominacion . ' and local = '. $local .'');
                    break;
                case 'monedas' :
                    $this->ActualizarDatos('[CobranzaFacturaciónMonedas]','Cantidad=' . $valor . '','WHERE Número=' . $guia . ' AND UnidxMoneda=' . $denominacion . ' and Local = '. $local .'');
                    break;
                case 'transferencia' :
                    $this->ActualizarDatos('[CobranzafacturacionTransacciones]','valor=' . $valor . ',detalle=' . $retencion .', banco=\'' . $banco . '\',documento='.$documento.',fechaTransferencia=\''.$cuenta.'\'','WHERE numero=' . $guia . ' AND Caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row =' . $numero_orden . ' and tipoTransaccion=\'TR\' and Local = '. $local .'');
                    break;
                case 'tarjeta' :
                    $this->ActualizarDatos('[CobranzafacturacionTransacciones]','valor=' . $valor . ',detalle=' . $retencion .', banco=\'' . $banco . '\',documento='.$documento.',autorizacion='.$posfechado.',numTarjeta='. $cuenta .',tarjeta='. $fechapos .'','WHERE numero=' . $guia . ' AND Caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row =' . $numero_orden . ' and tipoTransaccion=\'TC\' and Local = '. $local .'');
                    break;
                case 'retencion' :
                    $this->ActualizarDatos('[CobranzafacturacionRetenciones]','valorRubro=' . $valor . ',numDocumento=' . $retencion .', codAir=\'' . $documento . '\',rubro='.$cuenta.',detalleImpuesto=\''. $posfechado.'\' ','WHERE numero=' . $guia . ' AND Caja=' . $caja . ' AND cuota=' . $cuota . ' AND factura=' . $factura . ' AND row =' . $numero_orden . ' and Local = '. $local .'');
                    break;
            }
        }
        
        //FUNCION PUBLICA PARA VERIFICAR QUE LA GUIA DE COBRO
        public function ValidaGuia0($guia,$local) {
            $valida = $this->ObtenerDatos('(select Valorcobro as valor from [CobranzaFacturación detalle] where Número = ' . $guia . ' and Local = '. $local .' union all
    					select Valor as valor from [CobranzaFacturaciónCheque] where Número =' . $guia . ' and Local = '. $local .' union all select valor as valor from [CobranzaFacturacionTransacciones] where Numero =' . $guia . ' and Local = '. $local .'
    					union all select valorRubro as valor from [cobranzaFacturacionRetenciones] where numero =' . $guia . ' and Local = '. $local .' )X','isnull(sum(x.valor),0) as valor', '','');
            $valida = $valida[0][valor];
            return $valida;
        }
        
        //FUNCION PUBLICA PARA EDITAR LOS REGISTROS DE COBRANZA A LA GUIA DE COBRO
        public function GuardarGuia($guia, $totefectivo, $totmonedas, $totbilletes, $totcobranza, $totcheque, $tottransf, $tottarjeta, $totret,$local) {
            return $this->ActualizarDatos('[CobranzaFacturación]','TotalbrutoE=' . $totefectivo . ',TotalMonedas=' . $totmonedas . ',
    			TotalBilletes=' . $totbilletes . ',TotalCobranza=' . $totcobranza . ',TotalCheque=' . $totcheque . ',Estado=\'Actualizada\',
    			totalTransferencia=' . $tottransf . ',totalTarjetaC=' . $tottarjeta . ',totalRetenciones=' . $totret . '','WHERE Número=' . $guia . ' and Local = '. $local .'');
        }
        
        //FUNCION PUBLICA PARA VERIFICAR SI EL USUARIO TIENE ALGUNA GUIA DE COBRO PENDIENTE
        public function GuiaPendiente($usuario,$local) {
            return $this->ObtenerDatos('[CobranzaFacturación]','Número as Numero','Where (Estado=\'Movil\' or Estado=\'Pendiente\') and  Id_usuario_C =' . $usuario . ' and Local = ' . $local . '','');
        }
        
        //FUNCION PUBLICA PARA VERIFICAR LA GUIA NO EST� EN ESTADO PASIVO AUN
        public function GuiaGuardada($num,$local) {
            return $this->ObtenerDatos('[CobranzaFacturación]','Estado','Where Número=' . $num . ' and Local = '. $local .'','');
        }
        
        //FUNCION PUBLICA PARA BUSCAR CLIENTES Y SUS FACTURAS QUE ADEUDAN
        public function ConsultaClientesFacturas($tipo, $parametro, $local){
            switch ($tipo) {
                case 'RUC':
                    return $this->ObtenerDatos('(Select RUC,Cliente,Número,isnull(SaldoValor,SaldoFactura)as SaldoValor,isnull(FormaPago,0)AS Cuota,Fecha,
            					Facturas.serieFactura From Facturas left join FormaPagoFactura_Detalle on FormaPagoFactura_Detalle.Factura=Facturas.Número and
            					FormaPagoFactura_Detalle.serie=Facturas.serieFactura AND FACTURAS.LOCAL = FORMAPAGOFACTURA_DETALLE.LOCAL  where RUC=\'' . $parametro  . '\' and SaldoFactura>0 and Facturas.Estado=\'Actualizada\' and Facturas.Local = '. $local .') A',
            					'(select Nombre from Clientes WHERE Cliente = a.Cliente and Clientes.Local = '. $local .') AS CLIENTE,sum(a.SaldoValor) as Saldo, a.Número as Facturas, a.Fecha, a.Cliente',
            					'where (SaldoValor>0 )','group by a.Cliente,a.Número, a.Fecha order by a.Fecha');
                    break;
                case 'Codigo':
                    return $this->ObtenerDatos('(Select Número Facturas,isnull(SaldoValor,SaldoFactura) as Saldo,isnull(FormaPagoFactura_Detalle.FormaPago,0)AS Cuota,Fecha,
                        		Facturas.caja , FACTURAS.LOCAL, cliente  From Facturas  left join FormaPagoFactura_Detalle on FormaPagoFactura_Detalle.Factura=Facturas.Número and
                        		FormaPagoFactura_Detalle.serie=Facturas.caja AND FACTURAS.LOCAL = FORMAPAGOFACTURA_DETALLE.LOCAL  where cliente=\'' . $parametro  . '\' and SaldoFactura>0 and
                        		Facturas.Estado=\'Actualizada\')A',
                        		'*,(select Nombre from Clientes WHERE Cliente = a.Cliente and Local = '. $local .') AS CLIENTE','where (Saldo>0 AND A.LOCAL = '. $local .') order by Facturas');
                    break;
                case 'Nombre':
                    return $this->ObtenerDatos('(Select RUC,Cliente,Número,isnull(SaldoValor,SaldoFactura)as SaldoValor,isnull(FormaPago,0)AS Cuota,Fecha,
            					Facturas.serieFactura, NombreCliente From Facturas left join FormaPagoFactura_Detalle on FormaPagoFactura_Detalle.Factura=Facturas.Número and
            					FormaPagoFactura_Detalle.serie=Facturas.serieFactura AND FACTURAS.LOCAL = FORMAPAGOFACTURA_DETALLE.LOCAL where NombreCliente like \'%' . $parametro  . '%\' and SaldoFactura>0 and Facturas.Estado=\'Actualizada\' and Facturas.Local = '. $local .') A',
            					'top(5) (select Nombre from Clientes WHERE Cliente = a.Cliente and Clientes.Local = '. $local .') AS CLIENTE,sum(a.SaldoValor) as Saldo, Número as Facturas, a.Fecha',
            					'where (SaldoValor>0 )','group by a.Cliente, a.Número, a.NombreCliente, A.Fecha order by a.NombreCliente,a.fecha');
                    break;
                case 'Monitoreo':
                    return $this->ObtenerDatos('(Select distinct(Cliente_Servicios.Codigo_Monitoreo), RUC,facturas.Cliente,Número,isnull(SaldoValor,SaldoFactura)as SaldoValor,isnull(FormaPago,0)AS Cuota,Fecha,
            					Facturas.serieFactura From Facturas left join FormaPagoFactura_Detalle on FormaPagoFactura_Detalle.Factura=Facturas.Número and
            					FormaPagoFactura_Detalle.serie=Facturas.serieFactura AND FACTURAS.LOCAL = FORMAPAGOFACTURA_DETALLE.LOCAL left join Cliente_Servicios on Facturas.Cliente = Cliente_Servicios.Codigo_Cliente
            					where Cliente_Servicios.Codigo_Monitoreo = \'' . $parametro  . '\' and SaldoFactura>0 and Facturas.Estado=\'Actualizada\' and Facturas.Local = '. $local .') A',
            					'(select Nombre from Clientes WHERE Cliente = a.Cliente and Clientes.Local = '. $local .') AS CLIENTE,sum(a.SaldoValor) as Saldo, a.Número as Facturas, a.Fecha, a.Cliente',
            					'where (SaldoValor>0 )','group by a.Cliente, A.Número,a.Fecha  order by a.Fecha');
                    break;
            }
            
        }
        
        //FUNCION PUBLICA PARA BUSCAR CLIENTES QUE QUIEREN SUSPENDER EL SERVICIO
        public function ConsultaSalidaClientes($tipo, $parametro,$local){
            switch ($tipo) {
                case 'RUC':
                    return $this->ObtenerDatos('Cliente_Servicios A left join clientes B on a.Codigo_Cliente=b.Cliente left join ScannerCOP.dbo.Cliente_Monitoreo C
         						on a.Codigo_Monitoreo = c.Codigo','distinct(c.Nombre), A.Codigo_Monitoreo, A.Codigo_Cliente,b.Nombre as NombreFact,b.RUC','where b.RUC = \'' . $parametro  . '\' and c.[State] = \'A\' ','order by c.Nombre');
                    break;
                case 'Codigo':
                    return $this->ObtenerDatos('Cliente_Servicios A left join clientes B on a.Codigo_Cliente=b.Cliente left join ScannerCOP.dbo.Cliente_Monitoreo C
         						on a.Codigo_Monitoreo = c.Codigo','distinct(c.Nombre), A.Codigo_Monitoreo, A.Codigo_Cliente,b.Nombre as NombreFact,b.RUC','where b.Cliente = \'' . $parametro  . '\' and c.[State] = \'A\' ','order by c.Nombre');
                    break;
                case 'Nombre':
                    return $this->ObtenerDatos('Cliente_Servicios A left join clientes B on a.Codigo_Cliente=b.Cliente left join ScannerCOP.dbo.Cliente_Monitoreo C
         						on a.Codigo_Monitoreo = c.Codigo','top(5) * from (select distinct(c.Nombre), A.Codigo_Monitoreo, A.Codigo_Cliente,b.Nombre as NombreFact,b.RUC','where c.Nombre like \'%' . $parametro  . '%\' and c.[State] = \'A\') C','order by c.Nombre');
                    break;
                case 'Monitoreo':
                    return $this->ObtenerDatos('Cliente_Servicios A left join clientes B on a.Codigo_Cliente=b.Cliente left join ScannerCOP.dbo.Cliente_Monitoreo C
         						on a.Codigo_Monitoreo = c.Codigo','distinct(c.Nombre), A.Codigo_Monitoreo, A.Codigo_Cliente,b.Nombre as NombreFact,b.RUC','where a.Codigo_Monitoreo = \'' . $parametro  . '\' and c.[State] = \'A\' ','order by c.Nombre');
                    break;
            }
            
        }
        
        //FUNCION PUBLICA PARA OBTENER EL LISTADO DE RAZONES DE SALIDA DE CLIENTES
        public function razonSalida() {
            return $this->ObtenerDatos('[RazonesSalida]','*','','Order by Razon');
        }
        
        //FUNCION PUBLICA PARA VALIDAR SI SE QUIERE INGRESAR UN REGISTRO QUE YA EXISTE
        public function validaRazonSalida($cliente,$monitoreo) {
            return $this->ObtenerDatos('clientesCancelacionServicio','Count(cliente) as cliente','where cliente=\''  . $cliente . '\' and codMonitoreo=\''  . $monitoreo . '\'','');
        }
        
        //FUNCION PARA OBTENER PRODUCTOS
        public function BuscarProductos($opcion,$criterio,$local, $tipo)
        {
            $palabras = explode(" ", $criterio);
            $criterio = "";
            foreach ($palabras as $i => $value) {
                if($tipo == 'produccion'){
                    if($opcion=='CODIGO'){
                        $criterio = $criterio . " Número like '%" . trim($value) . "%' AND ";
                    }else{
                        $criterio = $criterio . " Promoción like '%" . trim($value) . "%' AND ";
                    }
                }else{
                    if($opcion=='CODIGO'){
                        $criterio = $criterio . " Id_Producto like '%" . trim($value) . "%' AND ";
                    }else{
                        $criterio = $criterio . " Nombre like '%" . trim($value) . "%' AND ";
                    }
                }
            }
            $criterio = substr($criterio,0,strlen($criterio)-4);
            switch ($tipo) {
                case 'consulta':
                    if($opcion=='CODIGO')
                    {
                        return $this->ObtenerDatos('Productos','Top ' . Config::$MaxProductos . ' Id_Producto, Nombre, (select local'. $local .' from ProductosExistencias where idProducto = Id_Producto) as Existencias, [Precio_Unidad A] as Precio_A, [Precio_Unidad B] as Precio_B, UnidadxEntera','where Activo = 1 and ' . $criterio,'order by Nombre Asc');
                    }else{
                        return $this->ObtenerDatos('Productos','Top ' . Config::$MaxProductos . ' Id_Producto, Nombre, (select local'. $local .' from ProductosExistencias where idProducto = Id_Producto) as Existencias, [Precio_Unidad A] as Precio_A, [Precio_Unidad B] as Precio_B, UnidadxEntera','where Activo = 1 and ' . $criterio,'order by Nombre Asc');
                    }
                    break;
                
                case 'servicio':
                    if($opcion=='CODIGO')
                    {
                        return $this->ObtenerDatos('Productos','Top ' . Config::$MaxProductos . ' Id_Producto, Nombre, (select local'. $local .' from ProductosExistencias where idProducto = Id_Producto) as Existencias, [Precio_Unidad A] as Precio_A, [Precio_Unidad B] as Precio_B, UnidadxEntera','where Activo = 1 and dbo.Productos.Id_Producto not in (select id_producto from  [orden de trabajo servicios]) and ' . $criterio,'order by Nombre Asc');
                    }else{
                        return $this->ObtenerDatos('Productos','Top ' . Config::$MaxProductos . ' Id_Producto, Nombre, (select local'. $local .' from ProductosExistencias where idProducto = Id_Producto) as Existencias, [Precio_Unidad A] as Precio_A, [Precio_Unidad B] as Precio_B, UnidadxEntera','where Activo = 1 and dbo.Productos.Id_Producto not in (select id_producto from  [orden de trabajo servicios]) and ' . $criterio,'order by Nombre Asc');
                    }
                    break;
                case 'produccion':
                    if($opcion=='CODIGO')
                    {
                        return $this->ObtenerDatos('[ficha_técnica]','Número as Id_Producto, Promoción as Nombre, BaseImp, unidadxentera as UnidadxEntera',' where ' . $criterio. ' AND Ini_Promoción <= GETDATE() AND Fin_Promoción >= GETDATE()','order by Nombre Asc');
                    }else{
                        return $this->ObtenerDatos('[ficha_técnica]','Número as Id_Producto, Promoción as Nombre, BaseImp, unidadxentera as UnidadxEntera',' where ' . $criterio. ' AND Ini_Promoción <= GETDATE() AND Fin_Promoción >= GETDATE()','order by Nombre Asc');
                    }
                    break;
            }
        }

        public function proforma($local, $numProforma){
            return $this->ObtenerDatos('(SELECT Proformas.*,FormaPagoFacturas.Descripción AS formapagoNombre,dbo.VendedorOrden.nom_vendedor AS NombreVendedor, 
            (SELECT  nom_usuario FROM         UsrUsuarios WHERE     (cod_usuario = id_usuario_c)) as Usuario FROM Proformas INNER JOIN 
            FormaPagoFacturas ON Proformas.Forma_Pago = FormaPagoFacturas.FormaPago INNER JOIN
            dbo.VendedorOrden ON  dbo.Proformas.Contacto = dbo.VendedorOrden.cod_vendedor 
            WHERE dbo.Proformas.Local = ' . $local . ' and Número = ' . $numProforma . ' ) X ',' X.* ',' ','');
        }

        public function proformaDetalle($tipo, $local, $numProforma){
            switch ($tipo){
                    case 'prodServ':
                    return $this->ObtenerDatos('[Proformas] INNER JOIN  [Proformas Detalle] ON  [Proformas].Número = [Proformas Detalle].Número AND [Proformas].Local = [Proformas Detalle].Local  INNER  JOIN
                    Bodega0_Productos ON [Proformas Detalle].producto = Bodega0_Productos.Id_Producto','[Proformas Detalle].*, (SELECT COUNT(*) AS num From [Proformas Detalle] WHERE Número = ' . $numProforma . ' AND [Proformas Detalle].Local=' . $local . ' AND [Proformas Detalle].ficha <> 1) AS num, 
                    Bodega0_Productos.Nombre AS Nombre_Producto, Bodega0_Productos.Costo_Promedio AS Costopromedio,  Bodega0_Productos.Existencias AS Existencias, [Proformas].id_usuario_C, Bodega0_Productos.[Precio_Unidad A] as [Precio_Minimo], Bodega0_Productos.[Precio_Unidad B] as PrecioB, ISNULL((SELECT PC.UTILIDAD FROM PRODUCTOS_CATEGORÍA PC WHERE PC.CATEGORÍA = BODEGA0_PRODUCTOS.CATEGORÍA),0) AS Categoria, 
                    ISNULL(Bodega0_Productos.[Precio_Unidad A],0) as [Precio_Minimo], CASE Bodega0_Productos.condicionado WHEN 1 THEN ROUND((CASE WHEN Bodega0_Productos.Costo_Promedio>Bodega0_Productos.Precio_Compra THEN Bodega0_Productos.Costo_Promedio ELSE Bodega0_Productos.Precio_Compra END) * (1 + Bodega0_Productos.porcCostoCondicionado / 100.00),2) ELSE -1 END AS [PrecioCondicionado]',
                    'WHERE [Proformas Detalle].Número=' . $numProforma . ' AND [Proformas].Local=' . $local . '', 'order by  [Proformas Detalle].ficha');
                    break;
                case 'produccion':
                    return $this->ObtenerDatos('[Proformas Ficha Detalle] inner join  [ficha_técnica] on [ficha_técnica].número=[Proformas Ficha Detalle].producto',
                    '[Proformas Ficha Detalle].*,[ficha_técnica].promoción Nombre_Producto,[ficha_técnica].promoción ,[Proformas Ficha Detalle].unidadxentera',
                    'where [Proformas Ficha Detalle].número='. $numProforma .' AND [Proformas Ficha Detalle].Local=' . $local . '','');
                    break;
                case 'ficha':
                    return $this->ObtenerDatos('[Proformas] INNER JOIN  [Proformas Detalle] ON  [Proformas].Número = [Proformas Detalle].Número AND [Proformas].Local = [Proformas Detalle].Local  INNER  JOIN
                    Productos ON [Proformas Detalle].producto = Productos.Id_Producto','[Proformas Detalle].*, (SELECT COUNT(*) AS num From [Proformas Detalle] 
                    WHERE Número = ' . $numProforma . ' AND [Proformas Detalle].Local=' . $local . ' AND [Proformas Detalle].ficha <> 1) AS num, 
                    Productos.Nombre AS Nombre_Producto,Productos.Costo_Promedio AS Costopromedio,  Productos.Existencias AS Existencias,
                    [Proformas].id_usuario_C, Productos.[Precio_Unidad A] as [Precio_Minimo]','WHERE [Proformas Detalle].Número=' . $numProforma . ' AND [Proformas].Local=' . $local . ' AND [Proformas Detalle].ficha = 1',
                    'order by  [Proformas Detalle].ficha');
                    break;
            }
        }
        
        public function fichaTecnica(){
            return $this->ObtenerDatos('Ficha_Técnica','Número as Numero, Total, totalIva, Promoción as Promocion,(select count(*) FROM Ficha_Técnica) as fichanum,  monitoreo,OBSERVACIÓN',
                ' WHERE Número>0 and Ini_Promoción<=GETDATE() and Fin_Promoción>=GETDATE()and tipo=1','order by número');
        }

        public function fichaTecDetalle($num){
            return $this->ObtenerDatos('[Ficha_Técnica DETALLE] INNER JOIN productos ON [Ficha_Técnica DETALLE].producto=[productos].id_producto',
            'producto,productos.nombre as nombre,(SELECT count(*) FROM [Ficha_Técnica DETALLE] WHERE número=' . $num . ') as cont','WHERE número=' . $num . '','');
        }

        public function crudFichas($nomSP, $parametros){
           $this->ObtenerDatosSP($nomSP,$parametros);
        }

        public function consultaTotalClientes($tipo, $parametro, $local){
            switch ($tipo) {
                case 'RUC':
                    return $this->ObtenerDatos('Clientes','*',' where RUC like \'%' . $parametro  . '%\' and local=' . $local . '','order by Nombre');
                    break;
                case 'Codigo':
                    return $this->ObtenerDatos('Clientes','*',' where Cliente = \'' . $parametro  . '\' and local=' . $local . '','order by Nombre');
                    break;
                case 'Nombre':
                    return $this->ObtenerDatos('Clientes','*',' where Nombre like \'%' . $parametro  . '%\' and local=' . $local . '','order by Nombre');
                    break;
                case 'Monitoreo':
                    return $this->ObtenerDatos('Cliente_Servicios A left join clientes B on a.Codigo_Cliente=b.Cliente left join ScannerCOP.dbo.Cliente_Monitoreo C
                                on a.Codigo_Monitoreo = c.Codigo','distinct(c.Nombre), A.Codigo_Monitoreo, A.Codigo_Cliente as Cliente,b.Nombre as NombreFact,b.RUC','where a.Codigo_Monitoreo = \'' . $parametro  . '\'  and b.Local = ' . $local .'','order by c.Nombre');
                    break;
            }
        }

        public function InsertaNuevaProforma($tipo, $valores){
            switch ($tipo) {
                case 'cabecera':
                    return $this->IngresarDatos('Proformas','Empresa, Local, Número, Fecha, Hora, Cliente, Cliente_act, Ficha, Nombre, Teléfono, Contacto, Forma_Pago, Plazo_Días, Descuento, TotalIva,
                        CanalCom, Monitoreo, TotalBruto, FechaEntrega, FechaCaduca, Creacion, ID_Usuario_C, RUC, Dirección, Actualizado, Empleado_Asesor, ivaficha, Email, estado',''. $valores .'');
                    break;
                
                case 'servicio':
                    return $this->IngresarDatos('[Proformas Detalle]','Empresa, Local, Número, Producto, Enteras, Unidades, Precio_Unidad, Dscto, DescripAdicional, IVA, Precio_Entera, 
                        precio_real, ficha, Detalle, orden, Unidadxentera',''. $valores .'');

                    break;

                case 'produccion':
                    return $this->IngresarDatos('[Proformas Ficha Detalle]','Empresa, Local, Número, Producto, Enteras, Unidades, Precio_Unidad, Dscto, DescripAdicional, IVA, Precio_Entera, 
                        precio_real, ficha, Detalle, orden, Unidadxentera',''. $valores .'');

                    break;
            }
        }

        public function calcularNuevoRegistro($numProforma, $local, $valor, $accion){
            switch ($accion) {
                case 'ingreso':
                    $iva = $valor * 0.12;
                    return $this->ActualizarDatos('Proformas','TotalBruto = TotalBruto + ' . $valor . ', TotalIva= TotalIva + ' . $iva . '',' where Número = ' . $numProforma .' and local=' . $local  . '');                    # code...
                    break;

                case 'elimina':
                    $iva = $valor * 0.12;
                    return $this->ActualizarDatos('Proformas','TotalBruto = TotalBruto - ' . $valor . ', TotalIva= TotalIva - ' . $iva . '',' where Número = ' . $numProforma .' and local=' . $local  . '');                    # code...
                    break;
            }
        }

        public function editarDatosProforma($tipo, $valores, $local, $numProforma){
            switch ($tipo) {
                case 'cabecera':
                    return $this->ActualizarDatos(' Proformas',' ' . $valores . ' ',' where Número=' . $numProforma . ' and local = ' . $local . '','');
                    break;
                
                case 'cliente':
                    return $this->ActualizarDatos(' Proformas',' ' . $valores . ' ',' where Número=' . $numProforma . ' and local = ' . $local . '','');
                    break;

                case 'servicio':
                    return $this->IngresarDatos('[Proformas Detalle]','Empresa, Local, Número, Producto, Enteras, Unidades, Precio_Unidad, Dscto, DescripAdicional, IVA, Precio_Enteras, precio_real, ficha, Detalle, orden,
                        Unidadxentera',''. $valores .'');
                    break;

                case 'produccion':
                    # code...
                    break;
            }
            
        }
        public function editarDetalleProforma($tipo, $valores, $condicion){
            switch ($tipo) {
                case 'servicioDetalle':
                    return $this->ActualizarDatos('[Proformas Detalle]',' ' . $valores . ' ',' ' . $condicion . ' ','');
                    break;

                case 'produccionDetalle':
                    return $this->ActualizarDatos('[Proformas Ficha Detalle]',' ' . $valores . ' ',' ' . $condicion . ' ','');
                    break;
            }
        }

        public function editarTotalesProforma($valores, $local, $num){
            return $this->ActualizarDatos('Proformas',' ' . $valores . ' ',' where Número=' . $num . ' and local = ' . $local . '','');
        }

        public function duplicaProforma($numProforma, $usuario, $local){
            return $this->ObtenerDatosSP('SP_DUPLICARPROFORMA', ' '. $numProforma .',' . $usuario . ', ' . $local . ',1');
        }

        public function eliminaItemProforma($tabla, $condicion){
            return $this->EliminarDatos($tabla, $condicion);
        }

        public function vendedores($local){
            return $this->ObtenerDatos('VendedorOrden','cod_vendedor, nom_vendedor','',' order by nom_vendedor');
        }

        public function formaPagoFacturas(){
            return $this->ObtenerDatos('FormaPagoFacturas','*','',' order by FormaPago');
        }

        public function canalComercial($local){
            return $this->ObtenerDatos('Clientes_Tipo','Descripción as Descripcion, Denominativo,Tipo_Cliente','WHERE local=' . $local .' ','Order by Descripcion desc');
        }

        public function consultaTotalProformas($tipo, $parametro, $local){
            switch ($tipo) {
                case 'RUC':
                    return $this->ObtenerDatos('Proformas','DISTINCT Cliente, Cliente_act, Nombre, RUC',' where RUC like \'%' . $parametro  . '%\' and local=' . $local . '','order by Nombre');
                    break;
                case 'Codigo':
                    return $this->ObtenerDatos('Proformas','DISTINCT Cliente, Cliente_act, Nombre, RUC',' where Cliente_act = \'' . $parametro  . '\' and local=' . $local . '','order by Nombre');
                    break;
                case 'Nombre':
                    return $this->ObtenerDatos('Proformas','DISTINCT Cliente, Cliente_act, Nombre, RUC',' where (Nombre like \'%' . $parametro  . '%\' OR Cliente like \'%' . $parametro  . '%\') and local=' . $local . '',' order by Nombre');
                    break;
            }
        }

        public function proformasxCliente($nomCli, $rucCli, $codCli, $contacto, $usuario){
            $condicion = "";
            $where ="";
            if($nomCli != "" || $nomCli != null){
                $condicion = ' Nombre like \'%' . $nomCli . '%\' OR ';
            }
            if($contacto != "" || $contacto != null){
                $condicion .= ' Cliente like \'%' . $contacto . '%\' OR ';
            }
            if($codCli != "" || $codCli != null){
                $condicion .= ' Cliente_act = \'' . $codCli . '\' OR ';
            }
            if($rucCli != "" || $rucCli != null){
                $condicion .= ' RUC = \'' . $rucCli . '\' OR ';
            }
            $condicion = substr($condicion,0,strlen($condicion)-3);
            if($condicion != ""){
                $where = 'where ' . $condicion;
            }
            return $this->ObtenerDatos('Proformas',' top(10) Número as Numero, Fecha, Nombre, Cliente_act, RUC, Ficha, TotalBruto, TotalIva, Total, Teléfono',
                ' '. $where . '',' order by fecha desc');
        }
        
        # End Region SCAIIN
        #Region ARTEMISA
        //FUNCION PARA VERIFICAR SI EL USUARIO ES ASESOR O NO
        public function usuarioArtemisa($idempleado){
            return $this->ObtenerDatos('DCEUSU_USUARI','*','where DCEUSU_IDSCAN=\'' . $idempleado . '\' AND DCEUSU_ESTADO = 1 AND DCEUSU_TIPOUS = 2','');
        }
        
        public function citasAsignadas($idUsuario, $desde, $hasta){
            return $this->ObtenerDatos('MDECIT_CITASP',' *, isnull((select TOP(1) * from (select cliente from ScaiinExpress..V_CLIENTESERVICIOS where [ID CLIENTE] = MDECLI_IDCLIE
                    union all select nombre from ScannerCOP..Cliente_Monitoreo where Codigo collate SQL_Latin1_General_CP1_CI_AS = MDECLI_IDCLIE) A ),\'SIN NOMBRE\') AS CLIENTE','where MDECIT_ASIGNA=\'' . $idUsuario . '\' AND MDECIT_ESTADO = 1 and MDECIT_FECHAV between \'' . $desde . '\' and \'' . $hasta . '\'','order by MDECIT_FECHAV asc, MDECIT_HORAIN asc');
        }
        
        public function citasdetalle($idCita){
            return $this->ObtenerDatos('MDEGDC_GESCIT','*','where MDECIT_IDCITA= \'' . $idCita . '\'',' ORDER BY MDEGDC_FECHAG DESC');
        }
        
        public function datosCita($idCita){
            return $this->ObtenerDatos('MDECIT_CITASP','*, isnull((select TOP(1) cliente from ScaiinExpress..V_CLIENTESERVICIOS where [ID CLIENTE] = MDECLI_IDCLIE),\'SIN NOMBRE\') AS CLIENTE','where MDECIT_IDCITA= \'' . $idCita . '\'','');
        }
        
        public function registrosCitas($ID, $GESTION, $BAJA, $TIPO){
            //Funcion para evitar el ingreso de comillas simples en la BDD
            $GESTION = str_replace("'","",$GESTION);
            $usrArtemisa = $this->usuarioArtemisa($_SESSION['log_IDEMPLEADO'] );
            $usrArtemisa = $usrArtemisa[0]['DCEUSU_ACCESO'];
            
            $this->IngresarDatos('MDEGDC_GESCIT','MDECIT_IDCITA,MDEGDC_FECHAG,MDEGDC_GESTIO,MDEGDC_GENPOR','\'' . $ID . '\',GETDATE(),\'' . $GESTION . '\',\'' . $usrArtemisa . '\'');
            //VERIFICAR SI HAY QUE DAR DE BAJA
            $this->GuardaLOGS($usrArtemisa,"ARTEMISA CITAS|INGRESO GESTION ARTEMISA CITAS","ID[" . $ID . "].");
            if($DARBAJA=='true'){
                //CAMBIO DE ESTADO DE LA CITA
                $this->ActualizarDatos(' MDECIT_CITASP','MDECIT_ESTADO=2','where MDECIT_IDCITA=\'' . $ID . '\'');
                //GESTION DE EJECUCION POR EL SISTEMA
                $this->IngresarDatos('MDEGDC_GESCIT','MDECIT_IDCITA,MDEGDC_FECHAG,MDEGDC_GESTIO,MDEGDC_GENPOR','\'' . $ID . '\',GETDATE(), \'-- EJECUTADO POR: ' . $usrArtemisa . ' DESDE LA APP MOVIL --\',\'' . $usrArtemisa . '\'');
                //ELIMINARLO EN CUANTO DEJE DE SER NECESARIO
                $this->GuardaLOGS($usrArtemisa,"ARTEMISA CITAS|BAJA","ID[" . $ID . "].");
                //FIN CODIGO
                // $this->ActualizarDatos('ControlOrdenes','Estado=2, IdEjecutor=\'' . $idUsuario .'\', UsuarioEjecucion=\'' . $usuario . '\', FechaEjecucion=GETDATE()','where IdOrden=\'' . $ID . '\'');
            }
        }
        
        public function posponerCitas($ID, $FECHA, $HINI, $HFIN){
            $usrArtemisa = $this->usuarioArtemisa($_SESSION['log_IDEMPLEADO'] );
            $usrArtemisa = $usrArtemisa[0]['DCEUSU_ACCESO'];
            $this->ActualizarDatos('MDECIT_CITASP','MDECIT_FECHAV=\''. $FECHA .'\', MDECIT_HORAIN=\''. $HINI .'\', MDECIT_HORAFI=\''. $HFIN .'\'','where MDECIT_IDCITA=\'' . $ID . '\'');
            $this->IngresarDatos('MDEGDC_GESCIT','MDECIT_IDCITA,MDEGDC_FECHAG,MDEGDC_GESTIO,MDEGDC_GENPOR','\'' . $ID . '\',GETDATE(),\'-- SE POSPONE LA CITA HASTA EL <'. $FECHA .'> POR: ' . $usrArtemisa . ' DESDE LA APP MOVIL --\',\'' . $usrArtemisa . '\'');
            $this->GuardaLOGS($usrArtemisa,"ARTEMISA CITAS|POSPUESTA  ","ID[" . $ID . "].");
        }

        public function creaClienteArtemisa($procedimiento, $parametros){
            $result = $this->ObtenerDatosSP($procedimiento, $parametros);
             return $result[0]['COD'];
         }
 
         public function creaCitaArtemisa($procedimiento, $parametros){
             $result = $this->ObtenerDatosSP($procedimiento, $parametros);
             return $result[0]['CODIGO'];
          }
 
         public function retornaAgencia($id){
             return $this->ObtenerDatos('Clientes_Tipo','Descripción as Descripcion',' where Tipo_Cliente=' . $id . '','');
         }
 
         public function clientesArtemisa($nombre, $apellido, $rucCli){
             return $this->ObtenerDatos('(select * from MDECLI_CLIENT where MDECLI_NOMAMO like \'% ' . $nombre . '\' OR MDECLI_NOMAMO LIKE \'% ' . $apellido . '%\' OR MDECLI_CEDRUC LIKE \'%' . $rucCli . '%\')A',
                     'DISTINCT A.MDECLI_IDCLIE, A.MDECLI_NOMAMO, A.MDECLI_NOMBRE, A.MDECLI_APELLI','','');
         }
        #End Region ARTEMISA
    }

    class conxPostgreSQL {
        private static $conexion;
        public function __construct($nameServer, $bddName , $user, $pass){
            if(!isset(self::$conexion)){
                try{
                    self::$conexion = new PDO('pgsql:host=' . $nameServer . '; dbname=' . $bddName, $user, $pass);
                    self::$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    self::$conexion->exec("SET NAMES 'UTF8'");
                }catch(PDOException $ex){
                    print "ERROR: " . $ex->getMessage() . "<br>";
                }
            }
        }

        public static function cambioEstadoRMA($num, $estado){
            $SQL = "SELECT update_state_rma('$num','$estado')";
            $query = self::$conexion->prepare($SQL);
            $query->execute();
        }
    }

    class Login{
        //FUNCION PUBLICA PARA VERIFICAR ACCESO/PRIVILEGIOS DE USUARIO
        public function VerificarSCAIIN($pagina,$local){
            session_start();
            if(!isset($_SESSION['log_USUARIO'])){
                //No ha iniciado sesion
                header('location:./login/index.php?url=' . $pagina . '&local=' . $local);
            }
        }
        
        public function Verificar($pagina){
            session_start();
            if(!isset($_SESSION['log_USUARIO'])){
                //No ha iniciado sesion
                header('location:./login/index.php?url=' . $pagina);
            }
        }
    }

	//CONEXION GLOBAL
	ini_set('mssql.charset', 'UTF-8');
	$SD = new Model(Config::$mvc_server['SANTODOMINGO'], Config::$mvc_database['SCAIINEXPRESS'], Config::$mvc_user, Config::$mvc_pass);
	$x = new Model(Config::$mvc_server['SANTODOMINGO'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);
	$cone = new Model(Config::$mvc_server['SANTODOMINGO'], Config::$mvc_database['SCAIINEXPRESS'], Config::$mvc_user, Config::$mvc_pass);
    $art = new Model(Config::$mvc_server['SANTODOMINGO'], Config::$mvc_database['ARTEMISA'], Config::$mvc_user, Config::$mvc_pass);
    $pgSQL = new conxPostgreSQL(Config::$nameServer, Config::$bddName, Config::$user, Config::$pass);
?>
