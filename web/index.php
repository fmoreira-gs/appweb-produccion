<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');

?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?php echo(Config::$mvc_titulo); ?></title>
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
<meta content='initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjZhArEJGrMLLfujWfelLUPyBnkvS5gQw"></script>
<script type="text/javascript" src="js/script.js"></script>
</head>
<body>
<?php
	//CARGA DE SESIONES
	session_start();
	
	//CARGA DE MENU PRINCIPAL 
	require 'menu.php';
	//--FIN MENU
	
	echo('<div id="contenido">');
	
	//CONTENIDO
	echo('<div id="AjaxContent">');
	echo('&nbsp;&nbsp;Bienvenido, ');
	echo('<br />&nbsp;&nbsp;<b>' . $_SESSION['log_EMPLEADO'] . '</b>');
	echo('<br />Inicio sesion en: ' . $_SESSION['log_TIEMPOSESION']);
	echo('<br />Rol de Usuario: ' . $_SESSION['log_nomRolUsr']);
	if($_SESSION['log_IDTECNICO']=="-1"){
		echo('<br /><hr width="50%" />Al momento las opciones se encuentran habilitadas &uacute;nicamente para T&eacute;cnicos. Gracias por su comprensi&oacute;n.');
	}
	//--FIN CONTENIDO
	//echo('<br />');
	//echo('<a href="#" onClick="obtenerPosicion(\'PosicionGPS\')">Cargar</a>&nbsp;<a id="PosicionGPS" style="display: none" href="#">XXXXXXXXXXXXXXXXx</a>');
	echo('</div>');
	echo('</div>');
	//VENTANA EMERGENTE
	echo('<div id="Gestion" class="ventana" title="Gestiones" style="display: none"></div>');
	echo('<div id="Block" style="display: none"></div>');
?>	
</body>
</html>