
//AJAX PARA CARGAR CONTENIDO
var xmlhttp;
var miPosicion;
var destinoAJAX;

//FUNCION EXCLUSIVA PARA POSICIONAMIENTO
var x = document.getElementById("aviso");
var posLongitud = document.getElementById("longitud");
var posLatitud = document.getElementById("latitud");

function obtenerPosicion() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(mostrarPosicion, mostrarError);
    } else {
        x.innerHTML = "La geolocalizacion no esta soportada por este dispositivo.";
    }
}

function mostrarPosicion(posicion){
    var lat= posicion.coords.latitude;
    var lon= posicion.coords.longitude;
    document.getElementById("latitud").value = lat;
    document.getElementById("longitud").value = lon;

    var latlon = new google.maps.LatLng(lat, lon)
    var mapholder = document.getElementById('mapholder')
    mapholder.style.height = '250px';
    mapholder.style.width = '500px';
    var myOptions = {
        center:latlon,zoom:14,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false,
        navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }
    var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
    var marker = new google.maps.Marker({position:latlon,map:map,title:"Su ubicacion es esta!"});
}

function mostrarError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            alert('El usuario denego la solicitud de geolocalizacion');
            x.innerHTML = "El usuario denego la solicitud de geolocalizacion."
        break;
        case error.POSITION_UNAVAILABLE:
            alert('Informacion de posicion GPS no esta disponible');
            x.innerHTML = "Informacion de posicion GPS no esta disponible."
        break;
        case error.TIMEOUT:
            alert('La solicitud tomo mas tiempo del esperado');
            x.innerHTML = "La solicitud tomo mas tiempo del esperado."
        break;
        case error.UNKNOWN_ERROR:
            alert('Error desconocido');
            x.innerHTML = "Error desconocido."
        break;
    }
}

function loadXMLDoc() {	  
    if (window.XMLHttpRequest){
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }else{
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById(destinoAJAX).innerHTML=xmlhttp.responseText;
        }
    }
}

//FUNCION PARA CARGAR MANTENIMIENTOS
function Mantenimientos(opt){
    //CERRAR VENTANAS EMERGENTES
    CerrarGestiones();
    if(opt==1){
        //SIN FILTRO
        $("ul.submenu").toggle();
        destinoAJAX = "AjaxContent";
        $("#AjaxContent").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
        loadXMLDoc();
        xmlhttp.open("GET","mantenimientos.php?NoCache=" + Math.random(),true);
        xmlhttp.send();
    }else{
        //CON FILTRO
        var zona;
        zona = document.getElementById("zona").value;
        destinoAJAX = "AjaxContent";
        $("#AjaxContent").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
        loadXMLDoc();
        xmlhttp.open("GET","mantenimientos.php?zona=" + zona + "&NoCache=" + Math.random(),true);
        xmlhttp.send();
    }
}

//FUNCIONES PARA EL INGRESO MANTENIMIENTOS
function parametroIngreso(){
    var body = document.body;
    html = document.documentElement;
    var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    $("#Block").height(BodyHeight);
    $("#Block").show();
    destinoAJAX = "Gestion";
    $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
    loadXMLDoc();	  	  
    xmlhttp.open("GET","preIngresoRequerimiento.php",true);
    xmlhttp.send();		
    $("#Gestion").show();
    $("ul.submenu").toggle();
}

function IngresoMantenimientos(opt) {
    //$("ul.submenu").toggle();
    if (opt== 0){
        var codigo;
        $("#nuevoCliente").empty();
        BuscarPor('Codigo');
        codigo = prompt('Ingrese el Codigo de Monitoreo!',codigo);
        if(codigo.trim() != '' ){
            var body = document.body;
            html = document.documentElement;
            var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
            $("#Block").height(BodyHeight);
            $("#Block").show();
            destinoAJAX = "Gestion";
            $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
            loadXMLDoc();	  	  
            xmlhttp.open("GET","IngresoMantenimientos.php?codigo=" + codigo + "&monitoreo=0&NoCache=" + Math.random(),true);
            xmlhttp.send();		
            $("#Gestion").show();
        }
    }else{
            var body = document.body;
            html = document.documentElement;
            var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
            $("#Block").height(BodyHeight);
            $("#Block").show();
            destinoAJAX = "Gestion";
            $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
            loadXMLDoc();	  	  
            xmlhttp.open("GET","IngresoMantenimientos.php?monitoreo=1",true);
            xmlhttp.send();		
            $("#Gestion").show();
    }
}

//FUNCION PARA CARGAR SUB SERVICIOS DISPONIBLES
function CargarSubServicios(){
    var servicio = document.getElementById('servicios');
    var servicioseleccionado = document.getElementById('servicioSeleccionado');
    CargarSubCategoria(servicioseleccionado.value);
    destinoAJAX = "_subservicio";
    $("#_subservicio").html('<center>Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","alarmas.php?opt=subservicio&var=" + servicio.value + "&seleccionado=" + servicioseleccionado.value + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
}

//FUNCION PARA CARGAR SUB SERVICIOS DISPONIBLES
function CargarSubCategoria(id){
    var servicio = document.getElementById('servicios');
    var subservicio = document.getElementById('subservicios');
    var servicioseleccionado = document.getElementById('servicioSeleccionado');
    destinoAJAX = "_subcategoria";
    $("#_subcategoria").html('<center></center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","alarmas.php?opt=subcategoria&var=" + subservicio.value + "&servicio=" + servicio.value + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
}

function compruebaExistente(opt){
    var json_data = {
        nombre: $('#txtNomCli').val(),
        apellido : $('#txtApeCli').val(),
        direccion : $('#txtDirCli').val(),
        telefono : $('#txtTelCli').val(),
        agencia : $('#cmbAgenciaCli').val(),
        ruc : $('#txtRucCli').val(),
        requerimiento : $('#txtRequerimiento').val(),
        fechaCita : $('#txtFechCita').val()
    };

    if(json_data.nombre == null || json_data.nombre == ''){
        alert('El Campo de NOMBRE debe llenarlo obligatoriamente.. Favor Verifique!!');
        return;
    }
    if(json_data.apellido == null || json_data.apellido == ''){
        alert('El Campo de  APELLIDO debe llenarlo obligatoriamente.. Favor Verifique!!');
        return;
    }
    if(json_data.direccion == null || json_data.direccion == ''){
        alert('El Campo de  DIRECCION debe llenarlo obligatoriamente.. Favor Verifique!!');
        return;
    }
    if(json_data.telefono == null || json_data.telefono == ''){
        alert('El Campo de  DIRECCION debe llenarlo obligatoriamente.. Favor Verifique!!');
        return;
    }
    if(json_data.requerimiento == null || json_data.requerimiento == ''){
        alert('El Campo de  REQUERIMIENTO debe llenarlo obligatoriamente.. Favor Verifique!!');
        return;
    }

    if(json_data.fechaCita == null || json_data.fechaCita == ''){
        alert('El Campo de  FECHA VISITA debe llenarlo obligatoriamente.. Favor Verifique!!');
        return;
    }
    
    $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
    loadXMLDoc();	  	  
    xmlhttp.open("POST","clienteExistente.php?",true);
    xmlhttp.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
    xmlhttp.send("json=" + JSON.stringify(json_data));
}

function grabarCitaArtemisa(opt, cont){
    var json_data = {
        idCliente : $('#txtIdCliente' + cont).val(),
        fechaCita : $('#txtFechCita').val(),
        requerimiento : $('#txtRequerimiento').val(),
        usuario : $('#txtUsuario').val(),
        direccion : $('#txtDireccion').val(),
        nombre : $('#txtNomCliente').val(),
        apellido : $('#txtApeCliente').val(),
        telefono : $('#txtTelefono').val(),
        agencia : $('#txtAgencia').val(),
        opcion : opt
    }
    var r = confirm("\u00BFEsta Seguro de Realizar esta accion? : CREAR NUEVA CITA ");
    if(r==true){
        $.ajax({
            type: "POST",
            url: "creaCitaArtemisa.php",
            dataType : "json",
            data: json_data,
            success:function(id){
                //codigo = id;
                alert('Se creó exitosamente la Cita # ' + id);
            }
        });
        CerrarGestiones();
    }
}

function grabaRequerimiento(){
    //var servicioseleccionado = document.getElementById('servicioSeleccionado');
    var sub = document.getElementById('subservicios');
    var subsrv = sub.options[sub.selectedIndex].value;
    var serv = document.getElementById('servicios');
    var srv = serv.options[serv.selectedIndex].value;
    //var pri = document.getElementById('prioridad');
    //var prioridad = pri.options[pri.selectedIndex].value;
    var categoria = document.getElementById('subcategorias');
    if (categoria != null){
        var subcat = categoria.options[categoria.selectedIndex].value;
    }else{
        var subcat = ' ';
    }
    
    var codigo = document.getElementById('txtCodCli').value;
    var solicitud = document.getElementById('txtRequerimiento').value;
    var contacto = document.getElementById('txtContacto').value;

    if(contacto.trim() == ''){
        alert('El Campo de CONTACTO debe llenarlo obligatoriamente.. Favor Verifique!!');
    }else{
        var r = confirm("\u00BFEsta Seguro de Realizar esta accion? : GRABAR REQUERIMIENTO ");
        if(r==true){
            destinoAJAX = "Gestion";
            $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
             loadXMLDoc();
             xmlhttp.open("GET","GrabarRequerimiento.php?codigo=" + codigo + "&requerimiento=" + solicitud + "&contacto=" + contacto + "&subservicio=" + subsrv + "&servicio=" + srv + "&prioridad=2&subcategoria=" + subcat + "&NoCache=" + Math.random(),true);
             xmlhttp.send();
             sleep(500);
             alert('El Requerimiento fue guardado con Exito');
             CerrarGestiones();
        }
    }
} 

//FUNCION REALIZADA CUANDO SE QUIZO INGRESAR CITAS COMO POSIBLE ORDEN 16-10-2019(FABIAN MOREIRA)
/*function grabaRequerimiento(opt){
    //var servicioseleccionado = document.getElementById('servicioSeleccionado');
    var sub = document.getElementById('subservicios');
    var subsrv = sub.options[sub.selectedIndex].value;
    var serv = document.getElementById('servicios');
    var srv = serv.options[serv.selectedIndex].value;
    //var pri = document.getElementById('prioridad');
    //var prioridad = pri.options[pri.selectedIndex].value;
    var categoria = document.getElementById('subcategorias');
    if (categoria != null){
        var subcat = categoria.options[categoria.selectedIndex].value;
    }else{
        var subcat = ' ';
    }
    
    var solicitud = document.getElementById('txtRequerimiento').value;
    var codigo = document.getElementById('txtCodCli').value;
    var contacto = document.getElementById('txtContacto').value;

    if(opt==0){
        if(contacto.trim() == ''){
            alert('El Campo de CONTACTO debe llenarlo obligatoriamente.. Favor Verifique!!');
        return;
        }
    }else{
       var json_data = {
            nombre: $('#txtNomCli').val(),
            apellido : $('#txtApeCli').val(),
            direccion : $('#txtDirCli').val(),
            telefono : $('#txtTelCli').val(),
            agencia : $('#cmbAgenciaCli').val(),
            solicitud : solicitud,
            contacto : $('#txtNomCli').val() + ' ' + $('#txtApeCli').val()
        };
        
        if(json_data.nombre == null || json_data.nombre == ''){
            alert('El Campo de NOMBRE debe llenarlo obligatoriamente.. Favor Verifique!!');
            return;
        }
        if(json_data.apellido == null || json_data.apellido == ''){
            alert('El Campo de  APELLIDO debe llenarlo obligatoriamente.. Favor Verifique!!');
            return;
        }
        if(json_data.direccion == null || json_data.direccion == ''){
            alert('El Campo de  DIRECCION debe llenarlo obligatoriamente.. Favor Verifique!!');
            return;
        }
        if(json_data.telefono == null || json_data.telefono == ''){
            alert('El Campo de  DIRECCION debe llenarlo obligatoriamente.. Favor Verifique!!');
            return;
        }
        if(json_data.solicitud == null || json_data.solicitud == ''){
            alert('El Campo de  REQUERIMIENTO debe llenarlo obligatoriamente.. Favor Verifique!!');
            return;
        }

        $.ajax({
            type: "POST",
            url: "retornaCodigo.php",
            dataType : "json",
            data: json_data,
            success:function(cod){
                codigo = cod;
                contacto = json_data.contacto;
                alert(cod);
                var r = confirm("\u00BFEsta Seguro de Realizar esta accion? : GRABAR REQUERIMIENTO ");
                if(r==true){
                    destinoAJAX = "Gestion";
                    $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
                    loadXMLDoc();
                    xmlhttp.open("GET","GrabarRequerimiento.php?codigo=" + codigo + "&requerimiento=" + solicitud + "&contacto=" + contacto + "&subservicio=" + subsrv + "&servicio=" + srv + "&prioridad=2&subcategoria=" + subcat + "&NoCache=" + Math.random(),true);
                    xmlhttp.send();
                    sleep(500);
                    alert('El Requerimiento fue guardado con Exito');
                    CerrarGestiones();
                }
            }
        });
    }
    if(opt==0){
        var r = confirm("\u00BFEsta Seguro de Realizar esta accion? : GRABAR REQUERIMIENTO ");
        if(r==true){
            destinoAJAX = "Gestion";
            $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
            loadXMLDoc();
            xmlhttp.open("GET","GrabarRequerimiento.php?codigo=" + codigo + "&requerimiento=" + solicitud + "&contacto=" + contacto + "&subservicio=" + subsrv + "&servicio=" + srv + "&prioridad=2&subcategoria=" + subcat + "&NoCache=" + Math.random(),true);
            xmlhttp.send();
            sleep(500);
            alert('El Requerimiento fue guardado con Exito');
            CerrarGestiones();
        }
    }
} */

function pasaPosibleOrden(id,tipo,idReq){
    var r = confirm("\u00BFEst\u00e1 Seguro que desea pasar el Requerimiento a POSIBLE ORDEN.?");
    if(r==true){
        var serv = document.getElementById('servicios');
        var srv = serv.options[serv.selectedIndex].value;
        var sub = document.getElementById('subservicios');
        var subsrv = sub.options[sub.selectedIndex].value;
        destinoAJAX = "Gestion";
        $("#Gestion").html('<center>Cargando...</center>');	  
        loadXMLDoc();
        xmlhttp.open("GET","pasoPosibleOrden.php?servicio=" + srv + "&subservicio=" + subsrv + "&idmant=" + id + "&idReq=" + idReq + "&NoCache=" + Math.random(),true);
        xmlhttp.send();
        CerrarGestiones();
        sleep(500);
        Mantenimientos(0);
    }
}

//FUNCION MARCAR COMO EJECUTADO
var DarBaja = 0;
function MarcarEjecutado(){
    if($("#Marca").attr("src")=="images/ejecutado_0.png"){
        $("#Marca").attr("src","images/ejecutado_1.png");
        $("#MarcaTexto").text("MARCADO COMO EJECUTADO");
        DarBaja = 1;	  
    }else{
        $("#Marca").attr("src","images/ejecutado_0.png");
        $("#MarcaTexto").text("MARCAR COMO EJECUTADO");
        DarBaja = 0;
    }	  
}

//FUNCION REGISTRAR GESTION MANTENIMIENTOS
function RegistrarGestion(id,tipo,idReq){
    idRma = $('#idRma').val();
    tipoDoc =  $('#txtTipoDoc').val();

    var txtGestion = document.getElementById("txtGestion").value; 
    if(txtGestion.trim()==''){
        alert('Debe ingresar gesti\u00f3n antes de continuar.');
    }else{
        //CONTINUAR
        //VERIFICAR SI DESEA DAR DE BAJA EL MANTENIMIENTO
        destinoAJAX = "Gestion";
        //$("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
        loadXMLDoc();
        if(tipo=='M'){
            xmlhttp.open("POST","IngresarGestionMantenimiento.php",true);
            xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        }else{ 
            xmlhttp.open("POST","IngresoGestionAsistencias.php",true);
            xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        }
        if(DarBaja==1){
            //DAR DE BAJA
            xmlhttp.send("txtidmantenimiento=" + encodeURIComponent(id) + "&txtGestion=" + encodeURIComponent(txtGestion) + "&opt=1&idReq=" + encodeURIComponent(idReq) + "&idRMA=" + encodeURIComponent(idRma) + "&tipoDoc=" + encodeURIComponent(tipoDoc) + "&NoCache=" + Math.random());
            //ACTUALIZAR LISTA DE MANTENIMIENTOS
            CerrarGestiones();
        if(tipo=='A'){
            Asistencias(0);
        }else{
            Mantenimientos(0);
            alert('El Mantenimiento se dio de baja EXITOSAMENTE !!');
        }
        }else{
            //NO DAR DE BAJA
            xmlhttp.send("txtidmantenimiento=" + encodeURIComponent(id) + "&txtGestion=" + encodeURIComponent(txtGestion) + "&idReq=" + encodeURIComponent(idReq) + "&NoCache=" + Math.random());
            //ACTUALIZAR LISTA DE GESTIONES
            Gestiones(id,tipo,idReq);	  				 
        }
    }
}

function Gestiones(Id,tipo,idReq) {
    sleep (100);
    //FUNCION PARA CARGAR LOS MANTENIMIENTOS
    var body = document.body,
    html = document.documentElement;
    var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    $("#Block").height(BodyHeight);
    $("#Block").show();
    destinoAJAX = "Gestion";
    $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
    loadXMLDoc();	  	  
    if(tipo=='A'){		   
        xmlhttp.open("GET","EjecucionAsistencias.php?numot=" + Id + "&NoCache=" + Math.random(),true);
        xmlhttp.send();						
    }else{
        xmlhttp.open("GET","GestionEjecucion.php?idmant=" + Id + "&idReq="+ idReq + "&NoCache=" + Math.random(),true);
        xmlhttp.send();
    }
    $("#Gestion").fadeIn(300,"swing");
}

function tipoGestion(Id,tipo,idReq) {
    sleep (100);
    //FUNCION PARA CARGAR LOS MANTENIMIENTOS
    var body = document.body,
    html = document.documentElement;
    var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    $("#Block").height(BodyHeight);
    $("#Block").show();
    destinoAJAX = "Gestion";
    $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
    loadXMLDoc();	  
    if(tipo=='A'){		   
        xmlhttp.open("GET","EjecucionAsistencias.php?numot=" + Id + "&NoCache=" + Math.random(),true);
        xmlhttp.send();						
    }else{
        xmlhttp.open("GET","seleccionGestion.php?idmant=" + Id + "&idReq="+ idReq + "&NoCache=" + Math.random(),true);
        xmlhttp.send();
    }
    $("#Gestion").fadeIn(300,"swing");
}

//FUNCION CLIENTES: DATOS CLIENTES - GUARDAR CAMBIOS
function Clientes_GuardarDatos(){
  //VARIABLES
    var txtCodCli = document.getElementById("txtCodCli").value;
    var marca = document.getElementById("marca").value;
    var modelo = document.getElementById("modelo").value;
    var version = document.getElementById("version").value;
    var operadora = document.getElementById("operadora").value;
    var txtNomCli = document.getElementById("txtNomCli").value;
    var txtDirCli = document.getElementById("txtDirCli").value;
    var txtTelCli = document.getElementById("txtTelCli").value;
    var txtRefCli = document.getElementById("txtRefCli").value;
    var txtMovilSms = document.getElementById("txtMovilSms").value;
    var txtLineaConv = document.getElementById("txtLineaConv").value;
    var txtTipoLinea = document.getElementById("tipoLinea").value;
    var chklinea = document.getElementById("linea").checked;
    var chkAveriada = document.getElementById("Consigna").checked;
    var txtEstadoClave = document.getElementById("txtEstadoClave").value;
    if(txtEstadoClave == 1){
        var estClave = true;
    }else{
        var estClave = false;
    }        
    if(document.getElementById("claveCambiada")){
        var chkCambioClave = document.getElementById("claveCambiada").checked;
    }else{
        var chkCambioClave = estClave;
    }
    
    if(isNaN(txtMovilSms) ) {
        alert("Solo se admiten numeros, Ingrese Datos Correctos.");;
        txtMovilSms = "";
    }
    if(txtNomCli.trim()==''||txtTelCli.trim()==''||txtRefCli.trim()==''||txtMovilSms.trim()==''){
        alert("Existen campos en blanco, por favor revise.");
    }else{
        //ALMACENAR CAMBIOS DE CLIENTE
        destinoAJAX = "W_Datos";
        $("#W_Datos").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
        loadXMLDoc();
        xmlhttp.open("POST","EditarDatosClientes.php",true);
        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        xmlhttp.send("txtCodCli="+encodeURIComponent(txtCodCli)+"&marca="+encodeURIComponent(marca)+"&modelo="+encodeURIComponent(modelo)
              +"&version="+encodeURIComponent(version)+"&operadora="+encodeURIComponent(operadora)+"&txtNomCli="+encodeURIComponent(txtNomCli)
              +"&txtDirCli="+encodeURIComponent(txtDirCli)+"&txtTelCli="+encodeURIComponent(txtTelCli)+"&txtRefCli="+encodeURIComponent(txtRefCli)
              +"&txtMovilSms="+encodeURIComponent(txtMovilSms) + "&txtLineaConv=" + encodeURIComponent(txtLineaConv) + "&txtTipoLinea=" + encodeURIComponent(txtTipoLinea) 
              + "&sinLinea=" + encodeURIComponent(chklinea) + "&lineaAveriada="  + encodeURIComponent(chkAveriada) + "&cambioClave=" + encodeURIComponent(chkCambioClave) 
              + "&txtEstadoclave=" + encodeURIComponent(estClave) + "&NoCache=" + Math.random());
        DatosClientes(txtCodCli);
    }
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

function Editar(){
    if(NumOrden!=-1){
        var tab = $(tabActivo).attr("id");
        switch($(tabActivo).attr("id")){
        case 'W_Autorizado':
            var txtCodCli = document.getElementById("txtCodCli").value;
            var txtnombre = document.getElementById(PreID + "txtnombre").value;
            var txtnota = document.getElementById(PreID + "txtnota").value;
            var txtorden = document.getElementById(PreID + "txtorden").value;
            destinoAJAX = "W_Autorizado";
            $("#W_Autorizado").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
            loadXMLDoc();
            xmlhttp.open("POST","EditarPersonalAutorizado.php",true);
            xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            xmlhttp.send("txtcodigo="+encodeURIComponent(txtCodCli)+"&txtnombre="+encodeURIComponent(txtnombre)+"&txtnota="+encodeURIComponent(txtnota)+"&txtorden="+encodeURIComponent(txtorden)+"&txtanterior="+encodeURIComponent(NumOrden) + "&NoCache=" + Math.random());
            sleep(500);
            TabsClientes(tab,tab.substring(2,tab.length));
        break;
        case 'W_Emergente':
            var txtCodCli = document.getElementById("txtCodCli").value;
            var txtnombre = document.getElementById(PreID + "txtnombre").value;
            var txtnumero = document.getElementById(PreID + "txtnumero").value;
            // var txtnota = document.getElementById(PreID + "txtnota").value;
            var txtorden = document.getElementById(PreID + "txtorden").value;
            var txtprioridad = document.getElementById(PreID + "prioridad").value;
            var txtcargo = document.getElementById(PreID + "cargo").value;
            destinoAJAX = "W_Emergente";
            $("#W_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
            loadXMLDoc();
            xmlhttp.open("POST","EditarPersonalEmergente.php",true);
            xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            xmlhttp.send("txtcodigo="+encodeURIComponent(txtCodCli)+"&txtnombre="+encodeURIComponent(txtnombre)+"&txtnumero="+encodeURIComponent(txtnumero)+"&txtorden="+encodeURIComponent(txtorden)+"&txtanterior="+encodeURIComponent(NumOrden)+"&prioridad="+encodeURIComponent(txtprioridad)+"&cargo="+encodeURIComponent(txtcargo) + "&NoCache=" + Math.random());
            sleep(500);
            TabsClientes(tab,tab.substring(2,tab.length));
        break;
        case 'W_Zonificacion':
            var txtCodCli = document.getElementById("txtCodCli").value;
            var txtZona = document.getElementById(PreID + "txtZona").value;
            var txtZonasCli = document.getElementById(PreID + "txtZonasCli").value;
            destinoAJAX = "W_Zonificacion";
            $("#W_Zonificacion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
            loadXMLDoc();
            xmlhttp.open("POST","EditarZonificacion.php",true);
            xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            xmlhttp.send("txtcodigo="+encodeURIComponent(txtCodCli)+"&txtZona="+encodeURIComponent(txtZona)+"&txtZonasCli="+encodeURIComponent(txtZonasCli)+"&txtanterior="+encodeURIComponent(NumOrden) + "&NoCache=" + Math.random());
            sleep(500);
            TabsClientes(tab,tab.substring(2,tab.length));
        break;
        case 'W_Emails':
            var txtCodCli = document.getElementById("txtCodCli").value;
            var txtNotaE = document.getElementById(PreID + "notaEmail").value;
            var txtEmail = document.getElementById(PreID + "Email").value;
            var txtNombre = document.getElementById(PreID + "nomEmail").value;
            var chkestado = document.getElementById(PreID + "check").checked;
            destinoAJAX = "W_Emails";
            $("#W_Emails").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
            loadXMLDoc();
            xmlhttp.open("POST","EditarEmail.php",true);
            xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            xmlhttp.send("txtcodigo="+encodeURIComponent(txtCodCli)+"&txtNotaE="+encodeURIComponent(txtNotaE)+"&txtEmail="+encodeURIComponent(txtEmail)+"&txtNombre="+encodeURIComponent(txtNombre)+"&chkestado="+encodeURIComponent(chkestado)+"&txtanterior="+encodeURIComponent(NumOrden) + "&NoCache=" + Math.random());
            sleep(500);
            TabsClientes(tab,tab.substring(2,tab.length));
        break;
        }
    }
}

function Eliminar(){
    if(NumOrden!=-1){
        var tab = $(tabActivo).attr("id");
        switch($(tabActivo).attr("id")){
        case 'W_Autorizado':
            var txtnombre = document.getElementById(PreID + "txtnombre").value;
            var r = confirm("\u00BFDesea eliminar el usuario (" + NumOrden + ") " + txtnombre + "?");
            if(r==true){
                var txtCodCli = document.getElementById("txtCodCli").value;
                //ELIMINAR
                destinoAJAX = tab;
                $(tab).html('<br /><center><img src="images/icon_cargando.gif"/><br />Eliminando...</center>');	  
                loadXMLDoc();
                xmlhttp.open("POST","EliminarPersonalAutorizado.php",true);
                xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                xmlhttp.send("codigo="+encodeURIComponent(txtCodCli)+"&orden="+encodeURIComponent(NumOrden) + "&NoCache=" + Math.random());
                //CARGAR VENTANA
                sleep(500);
                TabsClientes(tab,tab.substring(2,tab.length));
            }  
        break;
        case 'W_Emergente':
            var txtnombre = document.getElementById(PreID + "txtnombre").value;
            var r = confirm("\u00BFDesea eliminar el usuario (" + NumOrden + ") " + txtnombre + "?");
            if(r==true){
                 var txtCodCli = document.getElementById("txtCodCli").value;
                //ELIMINAR
                destinoAJAX = tab;
                  $(tab).html('<br /><center><img src="images/icon_cargando.gif"/><br />Eliminando...</center>');	  
                  loadXMLDoc();
                  xmlhttp.open("POST","EliminarPersonalEmergente.php",true);
                xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                xmlhttp.send("codigo="+encodeURIComponent(txtCodCli)+"&orden="+encodeURIComponent(NumOrden) + "&NoCache=" + Math.random());
                //CARGAR VENTANA
                sleep(500);
                TabsClientes(tab,tab.substring(2,tab.length));
            }
        break;
        case 'W_Zonificacion':
            var txtzona = document.getElementById(PreID + "txtZonasCli").value;
            var r = confirm("\u00BFDesea eliminar la zona (" + NumOrden + ") " + txtzona + "?");
            if(r==true){
                var txtCodCli = document.getElementById("txtCodCli").value;
                //ELIMINAR
                destinoAJAX = tab;
                $(tab).html('<br /><center><img src="images/icon_cargando.gif"/><br />Eliminando...</center>');	  
                loadXMLDoc();
                xmlhttp.open("POST","EliminarZonificacion.php",true);
                xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                xmlhttp.send("codigo="+encodeURIComponent(txtCodCli)+"&orden="+encodeURIComponent(NumOrden) + "&NoCache=" + Math.random());
                //CARGAR VENTANA
                sleep(500);
                TabsClientes(tab,tab.substring(2,tab.length));
            }
        break;
        case 'W_Emails':
            var txtEmail = document.getElementById(PreID + "Email").value;
            var r = confirm("\u00BFDesea eliminar el Correo (" + NumOrden + ") " + txtEmail + "?");
            if(r==true){
                 var txtCodCli = document.getElementById("txtcodigo").value;
                 //ELIMINAR
                 destinoAJAX = tab;
                   $(tab).html('<br /><center><img src="images/icon_cargando.gif"/><br />Eliminando...</center>');	  
                   loadXMLDoc();
                   xmlhttp.open("POST","EliminarEmail.php",true);
                 xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                 xmlhttp.send("codigo="+encodeURIComponent(txtCodCli)+"&orden="+encodeURIComponent(NumOrden) + "&NoCache=" + Math.random());
                 //CARGAR VENTANA
                 sleep(500);
                 TabsClientes(tab,tab.substring(2,tab.length));
           }
        break;
        }
    }
}

//FUNCION CLIENTES: PERSONAL AUTORIZADO - AGREGAR
function Clientes_AgregarAutorizado(){
    destinoAJAX = "V_Emergente";
    var Codigo = document.getElementById("txtCodCli").value;
    $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","InsertarPersonalAutorizado.php?codigo="+Codigo + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
    $("#V_Emergente").show();
}

//FUNCION CLIENTES: PERSONAL AUTORIZADO - GUARDAR CAMBIOS
function Clientes_GuardarAutorizado(){
  //VARIABLES
    var orden = document.getElementById("txtNusuario").value;
    var nombre = document.getElementById("txtnombre").value;
    var nota = document.getElementById("txtnota").value;
    if(nombre.trim()!=''){
        destinoAJAX = "V_Emergente";
        var Codigo = document.getElementById("txtCodCli").value;
        $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');
        loadXMLDoc();	    
        xmlhttp.open("POST","InsertarPersonalAutorizado.php?codigo="+Codigo,true);
        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");	    
        xmlhttp.send("txtnombre="+encodeURIComponent(nombre)+"&txtNusuario="+encodeURIComponent(orden)+"&txtnota="+encodeURIComponent(nota) + "&NoCache=" + Math.random());
    }else{
        alert('Existen campos en blanco.');
    }  
}


//FUNCION CLIENTES: PERSONAL EMERGENTE - AGREGAR
function Clientes_AgregarEmergente(){
    destinoAJAX = "V_Emergente";
    var Codigo = document.getElementById("txtCodCli").value;
    $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","InsertarPersonalEmergente.php?codigo="+ Codigo + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
    $("#V_Emergente").show();
}

//FUNCION CLIENTES: PERSONAL EMERGENTE - GUARDAR CAMBIOS
function Clientes_GuardarEmergente(){
    //VARIABLES
    var orden = document.getElementById("txtorden").value;
    var nombre = document.getElementById("txtnombre").value;
    var numero = document.getElementById("txtnumero").value;
    var pri = document.getElementById('prioridad');
    var prioridad = pri.options[pri.selectedIndex].value;
    var car = document.getElementById('cargo');
    var cargo = car.options[car.selectedIndex].value;
    if(nombre.trim()!=''&&numero.trim()!=''){
        destinoAJAX = "V_Emergente";
        var Codigo = document.getElementById("txtCodCli").value;
        $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');
        loadXMLDoc();	    
        xmlhttp.open("POST","InsertarPersonalEmergente.php?codigo="+Codigo,true);
        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");	    
        xmlhttp.send("txtnombre="+encodeURIComponent(nombre)+"&txtnumero="+encodeURIComponent(numero)+"&txtorden="+encodeURIComponent(orden)+"&prioridad="+encodeURIComponent(prioridad)+"&cargo="+encodeURIComponent(cargo)+"&NoCache=" + Math.random());
    }else{
        alert('Existen campos en blanco.');
    }
}

//FUNCION CLIENTES: ZONIFICACION - AGREGAR
function Clientes_AgregarZonificacion(){
    destinoAJAX = "V_Emergente";
    var Codigo = document.getElementById("txtCodCli").value;
    $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","InsertarZonificacion.php?codigo="+Codigo + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
    $("#V_Emergente").show();
}

//FUNCION PRA ACTIVAR O DESATIVAR EL ENVIO DE MAILS A CLIENTES
function editaEnvioMail(chk){
    var tab = $(tabActivo).attr("id");
    var txtCodCli = document.getElementById("txtcodigo").value;
    destinoAJAX = "W_Emails";
    $("#W_Emails").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","ConfirmacionEmail.php?txtcodigo=" + txtCodCli + "&activo=" + chk + "&NoCache="+ Math.random(),true);
    xmlhttp.send();
    sleep(500);
    TabsClientes(tab,tab.substring(2,tab.length));
}

//FUNCION CLIENTES: EMAILS - AGREGAR
function Clientes_AgregarEmail(){
    destinoAJAX = "V_Emergente";
    var Codigo = document.getElementById("txtCodCli").value;
    $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","InsertarEmail.php?codigo="+Codigo + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
    $("#V_Emergente").show();
}

//FUNCION CLIENTES: PERSONAL EMERGENTE - GUARDAR CAMBIOS
function Clientes_GuardarEmail(){
    //VARIABLES
    //var orden = document.getElementById("txtorden").value;
    var nombre = document.getElementById("txtnombre").value;
    var email = document.getElementById("txtEmail").value;
    var nota = document.getElementById("txtnota").value;
    if(nombre.trim()!='' && email.trim()!=''){
        destinoAJAX = "V_Emergente";
        var Codigo = document.getElementById("txtCodCli").value;
        $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');
        loadXMLDoc();	    
        xmlhttp.open("POST","InsertarEmail.php?codigo="+Codigo,true);
        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");	    
        xmlhttp.send("txtnombre="+encodeURIComponent(nombre)+"&txtEmail="+encodeURIComponent(email)+"&txtnota="+encodeURIComponent(nota) + "&NoCache=" + Math.random());
    }else{
        alert('Existen campos en blanco.');
    }
}

//FUNCION CLIENTES: HORARIOS - GUARDAR CAMBIOS
function Horarios_GuardarDatos(){
    var codigo = document.getElementById("txtcodigo").value;

    var lunes = document.getElementById("horarioLunes").value;
    var martes = document.getElementById("horarioMartes").value;
    var miercoles = document.getElementById("horarioMiercoles").value;
    var jueves = document.getElementById("horarioJueves").value;
    var viernes = document.getElementById("horarioViernes").value;
    var sabado = document.getElementById("horarioSabado").value;
    var domingo = document.getElementById("horarioDomingo").value;
    
    var chklunes = document.getElementById("checkLunes").checked;
    var chkmartes = document.getElementById("checkMartes").checked;
    var chkmiercoles = document.getElementById("checkMiercoles").checked;
    var chkjueves = document.getElementById("checkJueves").checked;
    var chkviernes = document.getElementById("checkViernes").checked;
    var chksabado = document.getElementById("checkSabado").checked;
    var chkdomingo = document.getElementById("checkDomingo").checked;
    
    var notahorario = document.getElementById("notahorario").value;
    var test = document.getElementById("test").value;
    var armado = document.getElementById("armado").value;
    var chkcontrol = document.getElementById("control").checked;
    
    destinoAJAX = "W_Horarios";
    $("#W_Horarios").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
    
    loadXMLDoc();
    xmlhttp.open("POST","GrabarHorariosClientes.php",true);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    
    xmlhttp.send("txtcodigo="+encodeURIComponent(codigo)+"&horarioLunes="+encodeURIComponent(lunes)+"&horarioMartes="+encodeURIComponent(martes)+"&horarioMiercoles="+encodeURIComponent(miercoles)+"&horarioJueves="+encodeURIComponent(jueves)+"&horarioViernes="+encodeURIComponent(viernes)+"&horarioSabado="+encodeURIComponent(sabado)+"&horarioDomingo="+encodeURIComponent(domingo)+"&checkLunes="+encodeURIComponent(chklunes)+"&checkMartes="+encodeURIComponent(chkmartes)+"&checkMiercoles="+encodeURIComponent(chkmiercoles)+"&checkJueves="+encodeURIComponent(chkjueves)+"&checkViernes="+encodeURIComponent(chkviernes)+"&checkSabado="+encodeURIComponent(chksabado)+"&checkDomingo="+encodeURIComponent(chkdomingo)+"&notahorario="+encodeURIComponent(notahorario)+"&test="+encodeURIComponent(test)+"&armado="+encodeURIComponent(armado)+"&control="+encodeURIComponent(chkcontrol)+"&NoCache=" + Math.random());
    sleep(500);
    TabsClientes("W_Horarios","Horarios");
}

//FUNCION CLIENTES: ZONIFICACION - GUARDAR CAMBIOS
function Clientes_GuardarZonificacion(){
    //VARIABLES
    var zona = document.getElementById("txtZona").value;
    var descripcion = document.getElementById("txtDescripcion").value;
    if(descripcion.trim()!=''){
        destinoAJAX = "V_Emergente";
        var Codigo = document.getElementById("txtCodCli").value;
        $("#V_Emergente").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');
        loadXMLDoc();	    
        xmlhttp.open("POST","InsertarZonificacion.php?codigo="+Codigo,true);
        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");	    
        xmlhttp.send("txtZona="+encodeURIComponent(zona)+"&txtDescripcion="+encodeURIComponent(descripcion) + "&NoCache=" + Math.random());
    }else{
        alert('Existen campos en blanco.');
    }
}	  

function VerMas(id){
    //COPIAR INFORMACION DE CAMPO EN DIV
    if(id!='none'){
        var Campo = document.getElementById(id).value;
    }else{
        var Campo = ''; 
    }
    $("#MasInfo").text(Campo);
}

//FUNCION PARA MARCAR FILA
var UltimoRadio;
var NumOrden;
var PreID;
function MarcarFila(id, num){
    var miRadio = document.getElementById(id);
    var miNumero = document.getElementById(num).value;
    //QUITAR
    $(".Radio").removeClass("Radio_OFF");
    $(".Radio").removeClass("Radio_ON");
    //COLOCAR
    $(".Radio").addClass("Radio_OFF");	  
    if(miRadio==UltimoRadio){
        NumOrden = -1;
        UltimoRadio = null;
        PreID = null;
    }else{
        $(miRadio).addClass("Radio_ON");
        NumOrden = miNumero;
        UltimoRadio = miRadio;
        PreID = id.substring(0,2) + id.replace(/\D/g,'') + '_';
    }	  
}

//FUNCION CHECKBOXES
function CheckHorario(CheckBOX,Link){
    //alert(CheckBOX +'-'+ Link);
    var chk = document.getElementById(CheckBOX);
    var lnk = document.getElementById(Link);
    if(chk.checked==true){
        chk.checked = false;
        $(lnk).removeClass("Radio_ON");
        $(lnk).addClass("Radio_OFF");
    }else{
        chk.checked = true;
        $(lnk).addClass("Radio_ON");
        $(lnk).removeClass("Radio_OFF");
    }
    if (CheckBOX == 'EM_chkMail'){
        editaEnvioMail(chk.checked);
    }
}

var BusquedaPor = "RUC";
function BuscarPor(param){
    $(".Radio").removeClass("Radio_OFF");
    $(".Radio").removeClass("Radio_ON");
    $(".Radio").addClass("Radio_OFF");
    $("#txtBuscarCliente").val("");
    $("#txtBuscarCliente").focus();
    if(param=="Nombre"){
        $("#optNombre").addClass("Radio_ON");		  
    }else if(param=="Codigo"){
        $("#optCodigo").addClass("Radio_ON");
    }else if(param=="RUC"){
        $("#optRUC").addClass("Radio_ON");
    }
    BusquedaPor = param;
}

function t_BuscarCliente(event, origen){
    if(event.keyCode==13){
        BuscarCliente(origen);
    }
}

function BuscarCliente(origen){
    destinoAJAX = "listaClientes";
    var criterio = document.getElementById('txtBuscarCliente').value;
    switch(BusquedaPor){
    case "Nombre":
        //$("#listaClientes").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
        loadXMLDoc();
        xmlhttp.open("GET","Buscar.php?code=" + Math.random() + "&opt=CL&buscarPor=nombre&criterio=" + criterio + "&origen=" + origen,true);
        xmlhttp.send();
        break;
    case "Codigo":
        //$("#listaClientes").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
        loadXMLDoc();
        xmlhttp.open("GET","Buscar.php?code=" + Math.random() + "&opt=CL&buscarPor=codigo&criterio=" + criterio + "&origen=" + origen,true);
        xmlhttp.send();
        break;
    case "RUC":
        //$("#listaClientes").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
        loadXMLDoc();
        xmlhttp.open("GET","Buscar.php?code=" + Math.random() + "&opt=CL&buscarPor=RUC&criterio=" + criterio + "&origen=" + origen,true);
        xmlhttp.send();
        break;
    }
}

//FUNCION PARA CARGAR ASISTENCIAS TECNICAS
function Asistencias(opt){
  //CERRAR VENTANAS EMERGENTES
  CerrarGestiones();
  if(opt==1){
      //SIN FILTRO
      $("ul.submenu").toggle();
      destinoAJAX = "AjaxContent";
      loadXMLDoc();
      xmlhttp.open("POST","AsistenciasTecnicas.php",true); 
      xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      xmlhttp.send();
  }else{
      //CON FILTRO
      var tecnico;
        tecnico = document.getElementById("tecnicos").value;
        destinoAJAX = "AjaxContent";
        $("#AjaxContent").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');
      loadXMLDoc();
      xmlhttp.open("POST","AsistenciasTecnicas.php",true);
      xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");	    
      xmlhttp.send("tecnicos="+encodeURIComponent(tecnico) + "&NoCache=" + Math.random());
      xmlhttp.send();
  }
}

//FUNCION PARA CARGAR CLIENTES MONITOREO
function Clientes(opt){
  //CERRAR VENTANAS EMERGENTES
  CerrarGestiones();
  //if(opt==1){
      //SIN FILTRO
      $("ul.submenu").toggle();
      destinoAJAX = "AjaxContent";
      loadXMLDoc();
      xmlhttp.open("GET","clientes.php?origen=" + opt + "&NoCache=" + Math.random(),true);
      xmlhttp.send();
  //}else{
  //CON FILTRO
}

//FUNCION PARA CARGAR CONFIGURACION
function Configuracion(opt){
  //CERRAR VENTANAS EMERGENTES
  CerrarGestiones();
  if(opt==1){
      //SIN FILTRO
      $("ul.submenu").toggle();
      destinoAJAX = "AjaxContent";
      loadXMLDoc();
      xmlhttp.open("GET","configuracion.php?NoCache=" + Math.random(),true);
      xmlhttp.send();
  }else{
      //CON FILTRO
  }
}	  

//MAS INFORMACION DE MANTENIMIENTO
var divs; 
function MasInfo(id) {
    //FUNCTION PARA VISUALIZAR MAS INFORMACION
    $(".MasInfo_detalle").slideUp('fast');
    divs = document.getElementById(id);
    fila = document.getElementById('tr_' + id);
    if($(divs).is(':hidden')){ 
        $(divs).slideDown('fast');
        $("#Cuenta").text(id);
        $("#Cuenta").show();		  
    } else {
        $(divs).slideUp('fast');
        $("#Cuenta").hide();
    }	  
}

function DatosClientes(Codigo) {
    //FUNCION PARA CARGAR LOS DATOS DE CLIENTES
    var body = document.body,
    html = document.documentElement;
    var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    $("#Block").height(BodyHeight);
    $("#Block").show();
    destinoAJAX = "Gestion";
    $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","Datos_Clientes.php?codigo=" + Codigo + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
    $("#Gestion").fadeIn(300,"swing");
}

function CargarModelos(){
    //FUNCION PARA CARGAR MODELOS DE CENTRALES
    var marca = document.getElementById('marca');
    destinoAJAX = "_Modelo";
    $("#_Modelo").html('<center>Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","alarmas.php?opt=marca&var=" + marca.value + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
}

function CargarVersiones(){
    //FUNCION PARA CARGAR VERSIONES DE CENTRALES
    var modelo = document.getElementById('modelo');
    destinoAJAX = "_Version";
    $("#_Version").html('<center>Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","alarmas.php?opt=modelo&var=" + modelo.value + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
}

var tabActivo;
function TabsClientes(id,mnu,rol){
    //QUITAR RADIO
    $(".Radio").removeClass("Radio_OFF");
    $(".Radio").removeClass("Radio_ON");
    //COLOCAR
    $(".Radio").addClass("Radio_OFF");
    NumOrden = -1;
    UltimoRadio = null;
    //FUNCION PARA CARGAR DATOS DE TABS (CLIENTES)
    var tab = document.getElementById(id);
    var menu = document.getElementById(mnu);
    //MOSTRAR/OCULTAR ICONOS
    if(id=="W_Datos"||id=="W_Horarios" || id=="W_Posicion" || id=="W_Emails"){
        $("#Botonera_2").hide();
    }else{
        if(rol != 0){
            $("#Botonera_2").show();
        }else{
            $("#Botonera_2").hide();
        }
    }
    //ACTIVAR DISE�O
    $(".TabMenu").removeClass("actual");
    $(menu).addClass("actual");
    tabActivo = tab;
    $(".Wrapper").hide();
    var Codigo = document.getElementById("txtCodCli").value;
    destinoAJAX = id;
    loadXMLDoc();
    xmlhttp.open("GET",id + ".php?codigo=" + Codigo + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
    $(tab).show();
}

function Historial(Cuenta) {
  //FUNCION PARA CARGAR LOS HISTORIALES
    var body = document.body,
    html = document.documentElement;
    var BodyHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    $("#Block").height(BodyHeight);
    $("#Block").show();
    destinoAJAX = "Gestion";
    $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("GET","Historial.php?cuenta=" + Cuenta + "&NoCache=" + Math.random(),true);
    xmlhttp.send();
    $("#Gestion").fadeIn(300,"swing");
}

function CerrarGestiones(){
  $("#Gestion").fadeOut(300,"swing");
  $("#Block").hide();
  CerrarEmergente()
  DarBaja = 0;
}

function CerrarEmergente(tab){
  var tab = tab || "TODOS";
  $("#V_Emergente").hide();
  if(tab!="TODOS"){
      TabsClientes(tab,tab.substring(2,tab.length));
  }
}

function posicionamiento(cod, origen){
    var lat = document.getElementById('latitud').value;
    var lon = document.getElementById('longitud').value;
    if(lat == "" || lon == "" || lat == null || lon == null){
        alert('No puede haber campos Vacios!. Debe Obtener las Coordenadas');		
    }else{
          loadXMLDoc();
          xmlhttp.open("POST","actualizaPosicion.php",true);
          xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");	    
          xmlhttp.send("codigo=" + encodeURIComponent(cod) + "&latitud=" + encodeURIComponent(lat) + "&longitud=" + encodeURIComponent(lon) + "&NoCache=" + Math.random());
          if(origen ==  1){
              TabsClientes('W_Datos','Datos');
              DatosClientes(cod);
          }else if (origen == 0){
            $("ul.submenu").toggle();
            CerrarGestiones();
            Clientes(0);
          }
    }
}

function w_posicion(Cuenta,origen){
     $("#Block").show();
     destinoAJAX = "Gestion";
     $("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Cargando...</center>');	  
     loadXMLDoc();
     xmlhttp.open("GET","W_Posicion.php?codigo=" + Cuenta + "&origen=" + origen + "&NoCache=" + Math.random(),true);
     xmlhttp.send();
     $("#Gestion").fadeIn(300,"swing");
}

function verClavePGM(codigo, idUsuario){
    //FUNCION PARA PODER VER LA CLAVE DE PROGRAMACION DEL SISTEMA DE ALARMA
    //alert(codigo);
    var r = confirm("IMPORTANTE: \nUsted va ha revisar informaci\xf3n CONFIDENCIAL, su usuario quedar\xe1 registrado para an\xe1lisis futuros.\nDesea Continuar.!!");
    if (r == true){
        if(idUsuario == null || idUsuario == ""){
            alert("No Hay informaci\xf3n de Usuario ACTIVO, FAVOR Cierre sesi\xf3n y Vuelva a intentar!!");
        }else{
            destinoAJAX = "_claveProgramador";
            $("#_claveProgramador").html('<center>Cargando...</center>');	  
            loadXMLDoc();
            xmlhttp.open("GET","verClave.php?var=" + codigo + "&NoCache=" + Math.random(),true);
            xmlhttp.send();		
        }
    }
    /*else{
        alert('No se realizo ninguna accion');
    }*/
}

function obtenerPosicion1() {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(succes, mostrarError);
          return true;
      } else {
          x.innerHTML = "La geolocalizacion no esta soportada por este dispositivo.";
          return false;
      }
}

function succes(posicion){
  var lat= posicion.coords.latitude;
  var lon= posicion.coords.longitude;
  document.getElementById("latitud").value = lat;
  document.getElementById("longitud").value = lon;
}

function insertaRegistroHoras(idOrden, idUsuario){
    var gestion;
    var lat = document.getElementById('latitud').value;
    var lon = document.getElementById('longitud').value;
    var tipo = document.getElementById('cmbTipoRegistro');
    var tipoReg = tipo.options[tipo.selectedIndex].value;
    if(tipoReg == '---'){
        alert('Escoga una opcion para continuar!!')
    }else{
        switch(tipoReg){
        case 'I-TS':
            gestion = "INICIO TRASLADO";
            break;
        case 'F-TS':
            gestion = "FIN TRASLADO";
            break;
        case 'A-TB':
            gestion = "APERTURA DE TRABAJO";
            break;
        case 'I-TB':
            gestion = "INICIO PARCIAL DE TRABAJO";
            break;
        case 'F-TB':
            gestion = "FIN PARCIAL DE TRABAJO";
            break;
        case 'C-TB':
            gestion = "CIERRE TOTAL DE TRABAJO";
            break;
        }
        
        if(lat == null || lat == "" || lon == null || lon=="")
        {
            alert("No existen datos de Posisionamineto, Favor Corrija y Vuelva a intentar!!");
        }else{
            if(idUsuario == null || idUsuario ==""){
                alert("No Hay informaci\xf3n de Usuario ACTIVO, FAVOR Cierre sesi\xf3n y Vuelva a intentar!!");
            }else{
                destinoAJAX = "Gestion";
                loadXMLDoc();
                xmlhttp.open("POST","ingresoRegistroHoras.php",true);
                xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");	    
                xmlhttp.send("idOrden=" + encodeURIComponent(idOrden) + "&latitud=" + encodeURIComponent(lat) 
                          + "&longitud=" + encodeURIComponent(lon) + "&gestion=" + encodeURIComponent(gestion)
                          + "&tipoDetalle=" + encodeURIComponent(tipoReg));
                Gestiones(idOrden,'A',);
            }
        }
    }
}

function pasaValores(clave, usuario,md5,sistema,local){
    $("#Block").show();
    destinoAJAX = "cambioContrasenia";
    //$("#Gestion").html('<br /><center><img src="images/icon_cargando.gif"/><br />Guardando...</center>');	  
    loadXMLDoc();
    xmlhttp.open("POST","../../cambioContrasenia.php",true);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xmlhttp.send("usuario=" + usuario + "&clave=" + clave + "&sistema=" + sistema + "&md5=" + md5 + "&local=" + local + "&tipo=1");
    $("#cambioContrasenia").show();
}

function cambioContrasenia(tipo){
	sistema = $('#txtSistema').val();
	if(sistema =='scaiin' || sistema =='SCAIIN'){
		opcion = 1;
	}else{
		opcion = 2;
	}
	switch (tipo) {
		case 2:
			var json_data = {
				nomUsuario : $('#txtUsuario').val(),
				claveAnterior : $('#txtClave').val(),
				claveNueva : $('#pwdNueva').val(),
                optValidacion : 2,
                tipo : tipo
			};
			if(json_data.claveAnterior != $('#pwdAnterior').val()){
				alert('La Clave Actual ingresada no es la Correcta..!!');
				return;
			}else if($('#pwdNueva').val() != $('#pwdConfirma').val()){
				alert('La confirmación de contraseña no coincide..!!');
				return;
			}else if($('#pwdNueva').val() == $('#pwdAnterior').val()){
				alert('La Nueva contraseña NO debe ser igual a la Anterior..!!');
				return;
			}
			pwd = $('#pwdNueva').val();
			if(validarContrasenia(pwd) == false){
				return;
			}else{
            	$.ajax({
					type: "POST",
					url: "../../actualizaDatosUsr.php",
					dataType : "text",
					data: json_data,
					success:function(r){
                        console.log(r);
                        if(r=='Usuario o contraseña no existen.'){
                            alert(r);
                        }else{
                            alert(r);
                            $("#mensaje").text("");
                            $("#cambioContrasenia").hide();
                            $("#Block").hide();
                            $("#cajaUsuario").val(json_data.nomUsuario);
                            $("#cajaPwd").focus();
                        }
					}
				});
			}
		break;
	}
}

function validarContrasenia(pwd){
    var sizePwd = pwd.length;
    mayus = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ';
    hayNum = 0;
    
    if(sizePwd < 8){
        alert('La Contraseña debe contener (8) caracteres mínimo.');
        return false;
    }
    for(i=0; i<sizePwd; i++ ){
        if(!isNaN(pwd[i]))
            hayNum++;
    }
    if(hayNum == 0){
        alert('La Contraseña debe contener al menos un número.');
        return false;
    }

    hayMayus=0;
    for(j=0; j<sizePwd; j++){
        for(h=0; h<mayus.length; h++){
            if(pwd[j] == mayus[h]){	
                hayMayus++;
            }
        }
    }
    if(hayMayus==0){
        alert('La Contraseña debe contener al menos una Letra Mayúscula.');
        return false;
    }
    
    hayArroba=0;
    for(x=0; x<sizePwd; x++){
        if(pwd[x] == '@'){	
            hayArroba++;
        }
    }
    if(hayArroba == 0){
        alert('La Contraseña debe contener al menos una Arroba (@)..');
        return false;
    }
}


//FUNCIONES JQUERY
$(document).ready(function(){  
    $("ul.submenu").hide();
    $("a.desplegable").toggle(
        function() {
            $(this).parent().find("ul.submenu").slideDown('fast');
        },
        function() {
            $(this).parent().find("ul.submenu").slideUp('fast');
        }
    ); 
});