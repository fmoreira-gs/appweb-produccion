<?php
	//ARCHIVOS DE CONFIGURACION GLOBAL
	session_start();
	$usuario = $_SESSION['log_USUARIO'];
	
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');

	
	//RECOPILACION DE VARIABLES DE PASO
	$idMant = $_GET['idmant'];
	$idreq = $_GET['idReq'];	
	$servicio =  $_GET['servicio'];
	$subservicio =  $_GET['subservicio'];
	
	$insert = $x ->idInsertAgencia();
	$insMovil = $x->InsertsMovil();
	$datos = $x -> datosRequerimientoDetalle($idMant,$idreq);
	$codigo = $datos[0][codCliente];
	$requerimiento = $datos[0][requerimiento];
	$requerimiento = $requerimiento;
	$prioridad = $datos[0][prioridad];
		
	$datoMant = $x-> mantenimientoObservacion($idMant);
	$idPosibleOrden = $datoMant[0][observacion];
	$contacto = $datoMant[0][Contacto];
	$contacto = $contacto;
	
	$repetido = $x->posibleOrdenRepetida($idPosibleOrden);
	if($repetido==0){
		$nuevoid = $idPosibleOrden;
	}else{
		$maxnum = $x->mantenimientoMax();
		$nuevoid = $insert.$maxnum;
		$x->nuevoRequerimiento('Requerimientos','IdRequerimiento,TipoCliente,Codigo,Requerimiento,Contacto,FechaCreacion,UsuarioCreacion,Modificado,Estado,prioridad,servicio,subServicio,subCategoria',
					'\'' . $nuevoid . '\',0,\'' . $codigo . '\',\'' . $requerimiento . '\',\'' . $contacto . '\',GETDATE(),\'' . $usuario . '\',0,2,\'' . $prioridad . '\',\'' . $servicio . '\',\'' . $subservicio . '\',NULL');
	}

	$x->nuevoRequerimiento('RequerimientosOrden','IdRequerimiento, Requerimiento, FechaCreacion, UsuarioCreacion, Estado, Orden, Prioridad, Cita, Servicio, SubServicio, origen',
				'\'' . $nuevoid . '\',\'' . $requerimiento . ' - Pasado desde PSNC(' . $idMant . ') -\',GETDATE(),\'' . $usuario . '\',1,NULL,\'' . $prioridad . '\',GETDATE(),\'' . $servicio . '\',\'' . $subservicio . '\',1');

	$x->ObtenerDatosSP('SP_RequerimientosDetalle','1, \'' . $idMant. '\', \'OT\', \'' .$nuevoid . '\', \'' . date('Y-m-d H:i:s') . '\',\'' . trim($usuario) . '\',1');

	$x->actualizaMantenimiento('Mantenimientos_Clientes_Detalle','Estado=3,requerimiento=\'' . $requerimiento . ' - Pasa a P.ORDEN(' . $nuevoid . ') -\'','Where idMantenimiento=\'' . $idMant . '\' and idReq=\'' . $idreq . '\'');
	$numNotasM = $x->cuentaRegistros(1,$idMant,$idreq);
	$x->nuevoRequerimiento('Notas_Mantenimientos_Clientes','Id_Mantenimiento,Fecha,Nota,Usuario,Id_Nota,idReq',
			'\'' . $idMant . '\',GETDATE(),\'Mantenimiento Pasado a - POSIBLE ORDEN(' . $nuevoid . ')\',\'' . $usuario . '\',\'' . $insMovil . $idMant . $numNotasM . '\',\'' . $idreq . '\'');
	
	$baja = $x->comprobarEstadoBaja($idMant);
	if($baja == 0){
		$x->actualizaMantenimiento('Mantenimientos_Clientes','Estado=2, Id_Ejecutor=\'' . $idUsuario .'\', Auditor=\'' . $usuario . '\', FechaAuditor=GETDATE(),Verificado=\'' . $usuario . '\', FechaVerificado=GETDATE()','where Id_Mantenimiento=\'' . $idMant . '\'');
	}

	$notasMant = $x->notasMantenimientos($idMant,$idreq);
	foreach($notasMant as $notas){
		$fechaNota = $notas[Fecha];
		$nota = $notas[Nota];
		$nota = $nota;
		$usuarioNota = $notas[Usuario];
		$numNotasP = $x->cuentaRegistros(0,$nuevoid,$idreq);
		
		$x->nuevoRequerimiento('NotasPOrdenesTrabajo','idOrden,Fecha,Nota,usuario,IdNota',
				'\'' . $nuevoid . '\',\'' . $fechaNota . '\',\'' . $nota . '\',\'' . $usuarioNota . '\',\'' . $insMovil . $nuevoid . $numNotasP . '\'');
	}
	
?>