<?php
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
    $local = $_SESSION['log_LOCAL'];
    $idUsuario = $_SESSION['log_IDUSUARIO'];
    //VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
    $ver->VerificarSCAIIN('index.php',$local);
	
	if(!isset($_SESSION['log_USUARIO'])){
	    header('location:/final/index.php');
    }
    
    $nomCli = $_GET['nombre'];
    $rucCli = $_GET['ruc'];
    $codCli = $_GET['id'];
    $contactoCli = $_GET['contacto'];
	
?>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
		<!--  ESTILOS  -->
		<link rel="stylesheet" type="text/css" href="css/estilos.css" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/select2.css">
		<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
	<body>
        <?php
            $proformas = $cone -> proformasxCliente($nomCli,$rucCli,$codCli, $contactoCli, $idUsuario);
            if(count($proformas) == 0){
				//echo '<h1>No existen Coincidencias para: '  . $parametro .    '</h1>';
				echo '<p class="mensaje"><b>No existen Coincidencias para: </p>';
				echo '<center class="respuesta"> '  . $nomCli .    '</center>';
			}else{
                echo ('
                <div id="cheque">
                                <br><div id="datosCliente" class="table-responsive">
                                <form id="frmNuevaProforma" method="POST">
                                <table class="table table-sm table-striped" border=1>
                                    <thead class="thead-dark">
                                        <th>Número</th>
                                        <th>Fecha</th>
                                        <th>RUC/CC</th>
                                        <th>Cod. F</th>
                                        <th>Tot. Proforma</th>
                                    </thead>
                                    <tbody>
                                    ');
                                    $cont=1;
                                    foreach ($proformas as $proforma) {
                                        $totalProforma = $proforma['TotalBruto'] + $proforma['TotalIva'];
                                        echo ('<tr>
                                            <td width="12%"><input type="text" class="textobserv" id="txtNumero' . $cont . '" value="' . $proforma['Numero'] .'" readonly></td>
                                            <td width="5%"><input type="text" class="textobserv" id="txtFecha' . $cont . '" value="' . date('m-Y',strtotime($proforma['Fecha'])) . '" maxlength="10" readonly ></td>
                                            <td width="5%"><input type="text" class="textobserv" id="txtRuc' . $cont . '" value="' . $proforma['RUC'] . '" readonly ></td>
                                            <td width="5%"><input type="text" class="textobserv" id="txtCodCliente' . $cont . '" value="' . $proforma['Cliente_act'] . '" maxlength="10" readonly ></td>
                                            <td width="5%"><input type="text" class="textobserv" id="txtTotal' . $cont . '" value="' . $totalProforma . '" maxlength="10" readonly ></td>
                                            <td width="2%"><center><button type="button" class="btn btn-sm btn-success" onClick=proformas(' . $proforma['Numero'] . ')><i class="fa fa-plus"> Detalle</i></button></center></td>
                                            <div id="listaProf'. $cont .'"></div>
                                        ');
                                            $cont++;
                                    }
                                    echo ('</tr>
                                </table>
                                </form>
                                </div>
                                </div>
                            <br>');
            }
        ?>
        <script type="text/javascript" src="js/script.js"></script> 
        <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="../js/select2.js"></script>
</body>
</html>