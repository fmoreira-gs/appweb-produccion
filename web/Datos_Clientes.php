<?php
	//Archivos 
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
	$rolUsr = $_SESSION['log_rolUsr'];
	$codigocli = $_GET['codigo'];
	$datoscli = $x-> ObtenerDatos('Cuentas_Monitoreo_Adt','*','Where Codigo=\'' . $codigocli . '\'','');
	$marca = $x-> ObtenerDatos('AlarmasMarca','*','','');	
	
?>
<html>
	<head>
    	<title>Posicionamiento</title>
    	<link rel="stylesheet" type="text/css" href="css/estilo.css" />
    	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
    	<script type="text/javascript" src="js/scripts.js"></script>
	</head>
	<body>
<?php
	
	echo '<div class="ventana_titulo">DATOS CLIENTES';
	echo '<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones()"><img src="images/cerrar_ventana.png" style="width:100%"  /></a></div>';
	echo '</div>';
	echo '<div class="TabWrapper">';
	echo '<ul>';
	echo '<li class="TabMenu icon_Menu_Clientes" id="Datos" onClick="TabsClientes(\'W_Datos\',\'Datos\','. $rolUsr .')"></li>';
	echo '<li class="TabMenu icon_Menu_Autorizado" id="Autorizado" onClick="TabsClientes(\'W_Autorizado\',\'Autorizado\','. $rolUsr .')"></li>';
	echo '<li class="TabMenu icon_Menu_Emergente" id="Emergente" onClick="TabsClientes(\'W_Emergente\',\'Emergente\','. $rolUsr .')"></li>';
	echo '<li class="TabMenu icon_Menu_Zonificacion" id="Zonificacion" onClick="TabsClientes(\'W_Zonificacion\',\'Zonificacion\','. $rolUsr .')"></li>';
	echo '<li class="TabMenu icon_Menu_Horarios" id="Horarios" onClick="TabsClientes(\'W_Horarios\',\'Horarios\','. $rolUsr .')"></li>';
	echo '<li class="TabMenu icon_Menu_Posicion" id="Posicion" onClick="TabsClientes(\'W_Posicion\',\'Posicion\','. $rolUsr .')"></li>';
	echo '<li class="TabMenu icon_Menu_Emails" id="Emails" onClick="TabsClientes(\'W_Emails\',\'Emails\','. $rolUsr .')"></li>';
	//echo '<li class="TabMenu icon_Menu_Reservado"></li>';
	echo '</ul>';	
	echo '</div>';
	
	
	//DATOS CLIENTES
	echo '<div class="TopWrapper" id="TopWrapper"><span id="MasInfo"></span><div class="Botonera_2" id="Botonera_2" style="display: none"><a href="#" onClick="Editar()" id="Editar" class="Boton_Editar icon_editar"></a>&nbsp;<a href="#" id="Eliminar" onClick="Eliminar()" class="Boton_Eliminar icon_eliminar"></a></div></div>';
	echo '<div class="Wrapper" id="W_Datos">';
	echo '<table class="VistaDatos" width="100%">';
	foreach ($datoscli as $datos){
	    echo '<input type="hidden" id="txtEstadoClave" value =\'' . $datos['nuevaClavePGM'] . '\' >';
		echo '<tr><td>C&oacute;digo:</td><td><input onFocus="VerMas(\'txtCodCli\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="txtCodCli" name="txtCodCli" value="' . $datos['Codigo'] . '" readonly="readonly"></td></tr>';
		echo '<tr><td>Clave PGM:</td><td><div id="_claveProgramador"><input type="button" value="Ver..." onClick="verClavePGM(\'' . $datos['Codigo'] . '\',\'' . $_SESSION['log_USUARIO'] . '\')" /></div></td></tr>';
		//---------------------
		if($datos['nuevaClavePGM'] == 0){
		    echo('<tr>');
		    echo('<td>Validar Cambio Clave:</td>');
		    echo('<td colspan="2">');
		    echo('<a class="Radio Radio_OFF" onClick="CheckHorario(\'claveCambiada\',\'CC\')" id="CC"></a>');
		    echo('<input type="checkbox" style="display: none" name="claveCambiada[]" id="claveCambiada" value=\'' . $datos['nuevaClavePGM'] . '\' />');
		    echo('</td></tr>');
		}
		//---------------------
		echo '<tr><td>Fecha Ingreso:</td><td><input onFocus="VerMas(\'txtFechain\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="txtFechain" name="txtFechain" value="' . date('d-m-Y H:i', strtotime($datos['DateIn'])) . '" readonly="readonly"></td></tr>';
		echo '<tr><td>Nombre:</td><td><input onFocus="VerMas(\'txtNomCli\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="txtNomCli" name="txtNomCli" value="' . $datos['Nombre'] . '"></td></tr>';
		echo '<tr><td>Direcci&oacute;n:</td><td><input onFocus="VerMas(\'txtDirCli\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="txtDirCli" name="txtDirCli" value="' . $datos['Direccion'] . '"></td></tr>';	
		echo '<tr><td>Tel&eacute;fonos:</td><td><input onFocus="VerMas(\'txtTelCli\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="txtTelCli" name="txtTelCli" value="' . $datos['Telefonos'] . '"></td></tr>';
		echo '<tr><td>Referencia:</td><td><input onFocus="VerMas(\'txtRefCli\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="txtRefCli" name="txtRefCli" value="' . $datos['Referencia'] . '"></td></tr>';
		$MarcaSeleccionada;
		echo '<tr><td>Central :</td><td>'; 
				echo('<div id="_Marca">');
				echo '<select class="Combo Extendido" name="marca" id="marca" onchange="CargarModelos()">';
					foreach($marca as $central){
						echo '<option value="'. $central['IdMarca'] . '"';
							if(isset($central['Marca']) && ($central['Marca']==$datos['Marca']) ){
								echo (' selected');
								$MarcaSeleccionada = $central['IdMarca'];
							}
							echo '>';
							echo($central['Marca']);
						echo '</option>';
					}
				echo '</select>';
				echo('</div>');
		echo '</td></tr>';
		$ModeloSeleccionado;
		echo '<tr><td>Modelo :</td><td>';
				echo('<div id="_Modelo">');
				$modelo = $x->ObtenerDatos('AlarmasModelo','*','where idMarca = \'' . $MarcaSeleccionada . '\'','order by Modelo Asc');
				echo '<select class="Combo Extendido" name="modelo" id="modelo" onchange="CargarVersiones()">';
					foreach($modelo as $model){
						echo '<option value="'. $model['IdModelo'] . '"';
						if(isset($model['Modelo']) && ($model['Modelo']==$datos['Modelo']) ){
							echo (' selected');
							$ModeloSeleccionado = $model['IdModelo'];
						}
						echo '>';
						echo $model['Modelo'];
						echo '</option>';
					}
				echo '</select>';
				echo('</div>');
		echo '</td></tr>';
			echo '<tr><td>Versi&oacute;n :</td><td>';
					echo('<div id="_Version">');
					$version = $x-> ObtenerDatos('AlarmasVersion','*','where idModelo = \'' . $ModeloSeleccionado . '\'','order by Version Asc');
					echo '<select class="Combo Extendido" name="version" id="version">';
						foreach($version as $vers){
							echo '<option value="'. $vers['IdVersion'] . '"';
							if(isset($vers['Version']) && ($vers['Version']==$datos['Version'])){
								echo (' selected');
							}
							echo '>';
							echo $vers['Version'];
							echo '</option>';
						}
					echo '</select>';
					echo('</div>');
			echo '</td></tr>';
			echo '<tr><td>M&oacute;vil SMS: </td><td><input onFocus="VerMas(\'txtMovilSms\')" onBlur="VerMas(\'none\')" class="Edicion" type="tel" id="txtMovilSms" name="txtMovilSms" value="' . $datos['TelefonoEnvioSMS'] . '" maxlength="10"></td></tr>';
			echo '<tr><td>Operadora: </td><td>';
			switch ($datos['ProveedorLineaConvencional']) {
				case Porta:
				$operadora=2;
				break;
				case Movistar:
					$operadora=3;
					break;
				case Allegro:
					$operadora=4;
					break;
			}	
				echo '<select class="Combo Extendido" id="operadora" name="operadora" style="width:30%; height:100%">';
					//CLARO (2)
					echo '<option value=2';
					if($operadora==2){
						echo(' selected');
					}
					echo '>Porta</option>';
					//MOVISTAR (3)
					echo '<option value=3';
					if($operadora==3){
						echo(' selected');
					}
					echo '>Movistar</option>';
					//CNT ALEGRO (4)
					echo '<option value=4';
					if($operadora==4){
						echo(' selected');
					}
					echo '>Allegro - CNT</option>';
				echo '</select>';
			echo '</td></tr>';
			echo '<tr><td>Linea Conectada: </td><td><input onFocus="VerMas(\'txtLineaConv\')" onBlur="VerMas(\'none\')" class="Edicion" type="tel" id="txtLineaConv" name="txtLineaConv" value="' . $datos['NumeroTelefonoConvencional'] . '" maxlength="10"></td></tr>';
			echo '<tr><td>Tipo Linea: </td><td>';
			switch ($datos['TipoLineaConvencional']) {
				case Ninguno:
					$tipoLinea=0;
					break;
				case Fija:
					$tipoLinea=1;
					break;
				case Celular:
					$tipoLinea=2;
					break;
			}
			echo '<select class="Combo Extendido" id="tipoLinea" name="tipoLinea" style="width:30%; height:100%">';
			//NINGUNO (0)
			echo '<option value=0';
			if($tipoLinea==0){
				echo(' selected');
			}
			echo '>Ninguno</option>';
			//FIJA (1)
			echo '<option value=1';
			if($tipoLinea==1){
				echo(' selected');
			}
			echo '>Fija</option>';
			//CELULARO (2)
			echo '<option value=2';
			if($tipoLinea==2){
				echo(' selected');
			}
			echo '>Celular</option>';
			echo '</select>';
			
			if($datos['Linea']==1){
				echo('<tr>');
				echo('<td>Sin L&iacute;nea:</td>');
				echo('<td colspan="2">');
				echo('<a class="Radio Radio_ON" onClick="CheckHorario(\'linea\',\'LN\')" id="LN"></a>');
				echo('<input type="checkbox" style="display: none" checked="checked" name="linea[]" id="linea" value=\'' . $datos['Linea'] . '\' />');
				echo('</td>');
			}else{
				echo('<tr>');
				echo('<td>Sin L&iacute;nea:</td>');
				echo('<td colspan="2">');
				echo('<a class="Radio Radio_OFF" onClick="CheckHorario(\'linea\',\'LN\')" id="LN"></a>');
				echo('<input type="checkbox" style="display: none" name="linea[]" id="linea" value=\'' . $datos['Linea'] . '\' />');
				echo('</td><br>');
			}
			
			if($datos['Consigna']==1){
				echo('<tr>');
				echo('<td>L&iacute;nea Averiada:</td>');
				echo('<td colspan="2">');
				echo('<a class="Radio Radio_ON" onClick="CheckHorario(\'Consigna\',\'CN\')" id="CN"></a>');
				echo('<input type="checkbox" style="display: none" checked="checked" name="Consigna[]" id="Consigna" value=\'' . $datos['Consigna'] . '\' />');
				echo('</td></tr>');
			}else{
				echo('<tr>');
				echo('<td>L&iacute;nea Averiada:</td>');
				echo('<td colspan="2">');
				echo('<a class="Radio Radio_OFF" onClick="CheckHorario(\'Consigna\',\'CN\')" id="CN"></a>');
				echo('<input type="checkbox" style="display: none" name="Consigna[]" id="Consigna" value=\'' . $datos['Consigna'] . '\' />');
				echo('</td></tr><br>');
			}
			
			echo '<tr><td>Coordenadas:</td>
                   <td colspan="2">Lat: <input onFocus="VerMas(\'txtLatitud\')" onBlur="VerMas(\'none\')" class="Coordenada" type="text" id="txtLatitud" name="txtLatitud" value=\'' . $datos['latitud'] . '\' readonly>
                    Lon: <input onFocus="VerMas(\'txtLongitud\')" onBlur="VerMas(\'none\')" class="Coordenada" type="text" id="txtLongitud" name="longitud" value=\'' . $datos['longitud'] . '\' readonly></td>';
			//<input type="button" value="GPS" onclick="posicionMostrar(\'' .$codigocli . '\',\'' . $datos[latitud] . '\',\'' . $datos[longitud] . '\')">';
			echo '</tr></table>';
			echo('<br/><center>');
			if($_SESSION['log_rolUsr'] != 0){
				echo('<a class="Boton_Guardar icon_guardar" onClick="Clientes_GuardarDatos()">&nbsp;Guardar cambios..</a>');
			}
			echo('</center>');

		}
	echo('<br />');
	echo('</div>');
	
	
	//PERSONAL AUTORIZADO	
	echo '<div class="Wrapper" id="W_Autorizado" style="display: none">';
	///////////AQUI EL CODIGO DE W_AUTORIZADO.PHP
	echo '</div>';
	
	
	//PERSONAL EMERGENTE
	echo('<div class="Wrapper" id="W_Emergente" style="display: none">');
	///////////AQUI EL CODIGO DE W_EMERGENTE.PHP
	echo('</div>');
	
	
	//ZONIFICACION
	echo('<div class="Wrapper" id="W_Zonificacion" style="display: none">');
	////////////AQUI EL CODIGO DE W_ZONIFICACION.PHP
	echo('</div>');
	
	//HORARIOS
	echo('<div class="Wrapper" id="W_Horarios" style="display: none">');
	/////////AQUI EL CODIGO DE LA PAGINA HORARIOS.PHP
	echo('</div>');

	//EMAILS
	echo('<div class="Wrapper" id="W_Emails" style="display: none">');
	/////////AQUI EL CODIGO DE LA PAGINA EMAILS.PHP
	echo('</div>');
	
	//POSICIONOAMIENTO
	echo('<div class="Wrapper" id="W_Posicion" style="display: none">');
	/////////AQUI EL CODIGO DE LA PAGINA W_POSICION.PHP
	echo('</div>');
	
	//CUADRANTE QUE PRESENTA LAS VENTANAS  DE EMERGENTE Y AUTORIZADO
	echo('<div class="ventana" id="V_Emergente" style="display: none">');
?>
</body>
</html>